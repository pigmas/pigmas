#ifndef COMPAREWIDGET_H
#define COMPAREWIDGET_H

#include <QWidget>
#include "mainwindow.h"
#include "spreadsheet.h"
#include "surfacegraph.h"
#include "bargraph.h"
#include "ql/math/matrix.hpp"

#include<QVBoxLayout>
#include<QHBoxLayout>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChartView>

#include<QtDataVisualization/Q3DSurface>
#include<QtDataVisualization/QSurface3DSeries>
#include<QtDataVisualization/Q3DBars>

class MainWindow;
class Spreadsheet;
class QtCharts::QChartView;


using namespace QuantLib;
using namespace QtDataVisualization;

class CompareWidget : public QWidget
{
    Q_OBJECT

public:
    CompareWidget(QWidget *parent = 0);
    CompareWidget(MainWindow* mainPtr = 0);

    Q3DBars* absoluteGraph = new Q3DBars();
    BarGraph *absoluteModifier = new BarGraph(absoluteGraph);
    QWidget* absoluteContainer = QWidget::createWindowContainer(absoluteGraph);

    Q3DBars *relativeGraph = new Q3DBars();
    BarGraph *relativeModifier = new BarGraph(relativeGraph);
    QWidget *relativeContainer = QWidget::createWindowContainer(relativeGraph);

    QVBoxLayout* mainLayout = new QVBoxLayout;
    QVBoxLayout* absoluteContainerLayout = new QVBoxLayout;
    QLabel* absoluteLabel = new QLabel(tr("Absolute Swaption Price Errors"));
    QVBoxLayout* relativeContainerLayout = new QVBoxLayout;
    QLabel* relativeLabel = new QLabel(tr("Relative Swaption Price Errors"));
    QHBoxLayout* graphLayout = new QHBoxLayout;
    QHBoxLayout* tableLayout = new QHBoxLayout;

    Spreadsheet* absoluteTable;
    Spreadsheet* relativeTable;

    void init();
    void layoutSetting();
    void signalSetting();
    void generate();
    void barGraphUpdate();

private:
    MainWindow* mainPtr_;
    vector<Matrix> tempRnVector_;

private slots:


signals:
    void updated();
};

#endif // CALIBRATIONWIDGET_H
