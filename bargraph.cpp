#include "bargraph.h"

BarGraph::BarGraph(Q3DBars *bargraph)
    : m_graph(bargraph),
      m_primarySeries(new QBar3DSeries)
{
    m_optionMaturity<<"1"<<"2"<<"3"<<"5"<<"7"<<"10";
    m_swapMaturity<<"1"<<"2"<<"3"<<"5"<<"7"<<"10";


    m_optionAxis->setTitle("Option Maturity");
    m_optionAxis->setTitleVisible(true);

    m_swapAxis->setTitle("Swap Maturity");
    m_swapAxis->setTitleVisible(true);

    m_graph->setValueAxis(m_valueAxis);
    m_graph->setRowAxis(m_optionAxis);
    m_graph->setColumnAxis(m_swapAxis);

    m_graph->addSeries(m_primarySeries);
    m_graph->activeTheme()->setBackgroundEnabled(false);

    m_graph->activeTheme()->setFont(QFont("Times New Roman", 30));
    m_graph->scene()->activeCamera()->setCameraPosition(45,30);
    m_graph->scene()->activeCamera()->setZoomLevel(90);// default 100

    m_valueAxis->setLabelAutoRotation(30.0f);
    m_optionAxis->setLabelAutoRotation(30.0f);
    m_swapAxis->setLabelAutoRotation(30.0f);


}

BarGraph::~BarGraph()
{
    delete m_graph;
}

void BarGraph::setData(Matrix* m){

    QBarDataArray *dataSet = new QBarDataArray;
    QBarDataRow *dataRow;

    dataSet->reserve(m->size1());

    for (int i = 0; i < m->size1() ; i++) {

        dataRow = new QBarDataRow(m->size2());

        for (int j = 0; j < m->size2(); j++) {
            (*dataRow)[j].setValue((*m)[i][j]);
        }

        dataSet->append(dataRow);

    }

    m_primarySeries->dataProxy()->resetArray(dataSet, m_optionMaturity, m_swapMaturity);

}

void BarGraph::setRelativeGraph(){
    m_valueAxis->setLabelFormat(QString(QStringLiteral("%.1f ") + "%"));
    m_graph->setTitle("Relative Swaption Price Errors");
}
