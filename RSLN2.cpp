#include "RSLN2.h"
#include <QTime>

RSLN2::RSLN2(){

    modelIndex = 3;
    params_={0.01,-0.02,0.04,0.08,0.04,0.4};
    paramsName_={"Mu1", "Mu2", "Sigma1", "Sigma2", "p12", "p21" };

}

void rslnCalibrationHelper(const real_1d_array &x, double &func, void* ptr){
    RSLN2* modelPtr = static_cast<RSLN2*>(ptr);
    std::vector<double> params = { x[0], x[1], x[2], x[3], x[4], x[5] };
    modelPtr->setParams(params);
    func = - modelPtr->MLE();
}

std::vector<double> RSLN2::calibrate(QuantLib::Matrix indexData){
    qDebug()<<" RSLN2::calibrate Called";

    // indexData input : d0=today d1=past
    // Transform
    int sizeIndex = indexData.rows();
    QuantLib::Matrix temp(sizeIndex,1);
    for(int i=0;i<sizeIndex;i++){
        temp[i][0]=indexData[sizeIndex-1-i][0];
    }
    indexData = temp;
    // calibrate() logic indexData : d0=past d1=today

    this->setData(indexData);

    real_1d_array paramsArray;
    paramsArray.setcontent(params_.size(),&(params_[0]));

    real_1d_array bndl = "[-1000,-1000,0.001,0.001,0.001,0.001]";
    real_1d_array bndu = "[1000,1000,1000,1000,1,1]";
    minbcstate state;
    minbcreport rep;

    double epsg = 1.0e-16;
    double epsf = 0;
    double epsx = 0;
    ae_int_t maxits = 0;
    double diffstep = 1.0e-6;

    minbccreatef(paramsArray, diffstep, state);
    minbcsetbc(state, bndl, bndu);
    minbcsetcond(state, epsg, epsf, epsx, maxits);

    alglib::minbcoptimize(state, rslnCalibrationHelper, 0, this);
    minbcresults(state, paramsArray, rep);

    return params_;

}

double RSLN2::MLE(QuantLib::Matrix indexData){
    // Reference : SOA RSLN Excel 2003
	//std::cout << "Function MLE Called" << std::endl;

    // Transform
    int sizeIndex = indexData.rows();

	std::vector<double> logReturn;

	for (int i = 1; i < sizeIndex; i++){
		logReturn.push_back(std::log(indexData[i][0] / indexData[i - 1][0]));
		//std::cout << "i : " << i << " logReturn : " << logReturn.back() << std::endl;

	}
	
	double mu1 = params_[0];
	double mu2 = params_[1];
	double sigma1 = params_[2];
	double sigma2 = params_[3];
	double pr12 = params_[4];
	double pr21 = params_[5];
	double phi1 = pr21 / (pr12 + pr21);
	double phi2 = 1 - phi1;
	double pr11 = 1 - pr12;
	double pr22 = 1 - pr21;
	int size = logReturn.size();
	
	boost::math::normal nd;

	std::vector<double> B(size);
	std::vector<double> C(size);
	std::vector<double> D(size);
	std::vector<double> E(size);
	std::vector<double> F(size);
	std::vector<double> G(size);
	std::vector<double> H(size);
	std::vector<double> I(size);
	std::vector<double> J(size);

	for (int i = 0; i < size; i++){

		double z1 = (logReturn[i] - mu1) / sigma1;
		double z2 = (logReturn[i] - mu2) / sigma2;
		
		B[i]=boost::math::pdf(nd, z1) / sigma1;
		C[i]=boost::math::pdf(nd, z2) / sigma2;
		
	}

	D[0] = (phi1*pr11 + phi2*pr21)*B[0];
	E[0] = 0;
	F[0] = (phi1*pr12 + phi2*pr22)*C[0];
	G[0] = 0;
	H[0] = D[0] + F[0];
	I[0] = D[0] / H[0];
	J[0] = F[0] / H[0];

	double sumLik = std::log(H[0]);

	for (int i = 1; i < size; i++){

		D[i]=I[i - 1] * pr11*B[i];
		E[i]=J[i - 1] * pr21*B[i];
		F[i]=I[i - 1] * pr12*C[i];
		G[i]=J[i - 1] * pr22*C[i];
		H[i]=D[i] + E[i] + F[i] + G[i];
		I[i]=(D[i] + E[i]) / H[i];
		J[i]=(F[i] + G[i]) / H[i];

		sumLik = sumLik + std::log(H[i]);
	}
	//std::cout << std::setprecision(30)<<"sumlik = " << sumLik << std::endl;

    return sumLik;
}

QuantLib::Matrix RSLN2::scenario(QuantLib::Matrix& rnd){
    qDebug()<<" RSLN2::scenario Called";
    // params : mu1, mu2, sigma1, sigma2, p12, p21

    std::vector<double> mu = { params_[0], params_[1] };
    std::vector<double> sigma = { params_[2], params_[3] };
    std::vector<double> prob = { params_[4], params_[5] };
    double phi1 = prob[1]/(prob[0]+prob[1]);
    double dt = 1.0/12.0;

    int numScen = rnd.size1();
    int numTerm = rnd.size2();
    QuantLib::Matrix scen(numScen,numTerm);
    QuantLib::Matrix regime(numScen,numTerm);

    QTime time = QTime::currentTime();


    QuantLib::MersenneTwisterUniformRng uGenerator(time.second());

    for(int i=0;i<numScen;i++){
        double u = uGenerator.next().value;
        if(u<phi1){
            regime[i][0] = 0;
        }else{
            regime[i][0] = 1;
        }
        scen[i][0]=exp(mu[regime[i][0]]*dt-0.5*dt*sigma[regime[i][0]]*sigma[regime[i][0]]+sigma[regime[i][0]]*sqrt(dt)*rnd[i][0])-1.0;
    }

    for(int i=0;i<numScen;i++){
        for(int j=1;j<numTerm;j++){
            double v = uGenerator.next().value;
            if(v<prob[regime[i][j-1]]){
                regime[i][j] = 1-regime[i][j-1];
            }else{
                regime[i][j] = regime[i][j-1];
            }
            scen[i][j]=exp(mu[regime[i][j]]*dt-0.5*dt*sigma[regime[i][j]]*sigma[regime[i][j]]+sigma[regime[i][j]]*sqrt(dt)*rnd[i][j])-1.0;
        }
    }
    return scen;

}
