#ifndef REALRNWIDGET_H
#define REALRNWIDGET_H

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <ql/math/matrix.hpp>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QGridLayout>
#include <QComboBox>
#include <QPushButton>
#include <QLineEdit>
#include <QListWidget>
#include <QSizePolicy>
#include <QInputDialog>


#include "realmainwindow.h"
#include "qcustomplot.h"
#include "dataclass.h"

using namespace QuantLib;
using namespace std;
class RealMainWindow;

class RealRnWidget : public QWidget
{
    Q_OBJECT

public:
    RealRnWidget(QWidget *parent = 0);
    ~RealRnWidget();

    QWidget* optionContainer = new QWidget;
    QLabel *methodLabel = new QLabel(tr("Method:"));
    QComboBox* methodComboBox = new QComboBox;
    QLabel* seedLabel = new QLabel(tr("Seed Number:"));
    QLineEdit *seedEdit = new QLineEdit;
    QLabel *maturityLabel = new QLabel(tr("Month:"));
    QLineEdit *maturityEdit = new QLineEdit;
    QLabel *nLabel = new QLabel(tr("N:"));
    QLineEdit *nEdit = new QLineEdit;
    QLabel *tempListLabel = new QLabel(tr("Normal Random Number:"));
    QListWidget *tempList = new QListWidget;

    QLabel *maturityHistLabel = new QLabel(tr("Month Slice Histogram:"));
    QComboBox *maturityHistComboBox = new QComboBox;

    QLabel *nHistLabel = new QLabel(tr("N Slice Histogram:"));
    QComboBox *nHistComboBox = new QComboBox;

    QCustomPlot* customPlot1 = new QCustomPlot(this);
    QCustomPlot* customPlot2 = new QCustomPlot(this);
    QPushButton* generatePush = new QPushButton(tr("Generate"));
    QHBoxLayout* mainLayout = new QHBoxLayout;
    QVBoxLayout* optionLayout = new QVBoxLayout;
    QVBoxLayout* histLayout = new QVBoxLayout;
    QHBoxLayout* histOptionLayout = new QHBoxLayout;
    QHBoxLayout* chartLayout = new QHBoxLayout;

    void init();
    void layoutSetting();
    void signalSetting();
    void generate();

    void updateList();
    void updateHistCombo();
    void updateChart1();
    void updateChart2();
    void updateAll();


    Matrix hist(Matrix dat, double first, double last, int n);

    void addTemp(QString varName,Matrix rnd);
    void delTemp();
    void setMainPtr(){ mainPtr_ = qobject_cast<RealMainWindow*>(this->parent()); }

private:
    vector<boost::shared_ptr<DataClass>> tempRnVector_;
    RealMainWindow* mainPtr_;

private slots:
    void on_generatePush_clicked();
    void on_tempList_Changed();
    void on_Maturity_Changed();
    void on_N_Changed();

signals:
    void updated();
};

#endif // REALRNWIDGET_H
