/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Data Visualization module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "surfacegraph.h"

#include <QtDataVisualization/QValue3DAxis>
#include <QtDataVisualization/Q3DTheme>
#include <QtGui/QImage>
#include <QtCore/qmath.h>


using namespace QtDataVisualization;

const int sampleCountX = 50;
const int sampleCountZ = 50;
const int heightMapGridStepX = 6;
const int heightMapGridStepZ = 6;
const float sampleMin = -8.0f;
const float sampleMax = 8.0f;

SurfaceGraph::SurfaceGraph(Q3DSurface *surface)
    : m_graph(surface)
{
//    m_graph->setAxisX(new QValue3DAxis);
//    m_graph->setAxisY(new QValue3DAxis);
//    m_graph->setAxisZ(new QValue3DAxis);


    m_valueAxis->setTitle("Swaption Price");
    m_valueAxis->setTitleVisible(true);

    m_optionAxis->setTitle("Option Maturity");
    m_optionAxis->setTitleVisible(true);

    m_swapAxis->setTitle("Swap Maturity");
    m_swapAxis->setTitleVisible(true);

    m_graph->setAxisX(m_optionAxis);
    m_graph->setAxisY(m_valueAxis);
    m_graph->setAxisZ(m_swapAxis);

    m_valueAxis->setLabelAutoRotation(30.0f);
    m_optionAxis->setLabelAutoRotation(30.0f);
    m_swapAxis->setLabelAutoRotation(30.0f);

    m_graph->activeTheme()->setFont(QFont("Times New Roman", 30));
    m_graph->scene()->activeCamera()->setCameraPosition(45,30);
    m_graph->scene()->activeCamera()->setZoomLevel(90);// default 100
    //! [0]
    m_sqrtSinProxy = new QSurfaceDataProxy();
    m_sqrtSinSeries = new QSurface3DSeries(m_sqrtSinProxy);
    //! [0]
    fillSqrtSinProxy();
}

SurfaceGraph::~SurfaceGraph()
{
    delete m_graph;
}

//! [1]
void SurfaceGraph::fillSqrtSinProxy()
{
    float stepX = (sampleMax - sampleMin) / float(sampleCountX - 1);
    float stepZ = (sampleMax - sampleMin) / float(sampleCountZ - 1);

    QSurfaceDataArray *dataArray = new QSurfaceDataArray;
    dataArray->reserve(sampleCountZ);
    for (int i = 0 ; i < sampleCountZ ; i++) {
        QSurfaceDataRow *newRow = new QSurfaceDataRow(sampleCountX);
        // Keep values within range bounds, since just adding step can cause minor drift due
        // to the rounding errors.
        float z = qMin(sampleMax, (i * stepZ + sampleMin));
        int index = 0;
        for (int j = 0; j < sampleCountX; j++) {
            float x = qMin(sampleMax, (j * stepX + sampleMin));
            float R = qSqrt(z * z + x * x) + 0.01f;
            float y = (qSin(R) / R + 0.24f) * 1.61f;
            (*newRow)[index++].setPosition(QVector3D(x, y, z));
        }
        *dataArray << newRow;
    }

    m_sqrtSinProxy->resetArray(dataArray);
}
//! [1]

void SurfaceGraph::enableSqrtSinModel(bool enable)
{
    if (enable) {
        //! [3]
        m_sqrtSinSeries->setDrawMode(QSurface3DSeries::DrawSurfaceAndWireframe);
        m_sqrtSinSeries->setFlatShadingEnabled(true);

        m_graph->axisX()->setLabelFormat("%.2f");
        m_graph->axisZ()->setLabelFormat("%.2f");
        m_graph->axisX()->setRange(0,10);
        m_graph->axisY()->setRange(0.0f, 1.0f);
        m_graph->axisZ()->setRange(0,10);
        m_graph->axisX()->setLabelAutoRotation(30);
        m_graph->axisY()->setLabelAutoRotation(90);
        m_graph->axisZ()->setLabelAutoRotation(30);

        m_graph->addSeries(m_sqrtSinSeries);
        //! [3]

        //! [8]
        // Reset range sliders for Sqrt&Sin
        m_rangeMinX = sampleMin;
        m_rangeMinZ = sampleMin;
        m_stepX = (sampleMax - sampleMin) / float(sampleCountX - 1);
        m_stepZ = (sampleMax - sampleMin) / float(sampleCountZ - 1);
        m_axisMinSliderX->setMaximum(sampleCountX - 2);
        m_axisMinSliderX->setValue(0);
        m_axisMaxSliderX->setMaximum(sampleCountX - 1);
        m_axisMaxSliderX->setValue(sampleCountX - 1);
        m_axisMinSliderZ->setMaximum(sampleCountZ - 2);
        m_axisMinSliderZ->setValue(0);
        m_axisMaxSliderZ->setMaximum(sampleCountZ - 1);
        m_axisMaxSliderZ->setValue(sampleCountZ - 1);
        //! [8]
    }
}

void SurfaceGraph::adjustXMin(int min)
{
    float minX = m_stepX * float(min) + m_rangeMinX;

    int max = m_axisMaxSliderX->value();
    if (min >= max) {
        max = min + 1;
        m_axisMaxSliderX->setValue(max);
    }
    float maxX = m_stepX * max + m_rangeMinX;

    setAxisXRange(minX, maxX);
}

void SurfaceGraph::adjustXMax(int max)
{
    float maxX = m_stepX * float(max) + m_rangeMinX;

    int min = m_axisMinSliderX->value();
    if (max <= min) {
        min = max - 1;
        m_axisMinSliderX->setValue(min);
    }
    float minX = m_stepX * min + m_rangeMinX;

    setAxisXRange(minX, maxX);
}

void SurfaceGraph::adjustZMin(int min)
{
    float minZ = m_stepZ * float(min) + m_rangeMinZ;

    int max = m_axisMaxSliderZ->value();
    if (min >= max) {
        max = min + 1;
        m_axisMaxSliderZ->setValue(max);
    }
    float maxZ = m_stepZ * max + m_rangeMinZ;

    setAxisZRange(minZ, maxZ);
}

void SurfaceGraph::adjustZMax(int max)
{
    float maxX = m_stepZ * float(max) + m_rangeMinZ;

    int min = m_axisMinSliderZ->value();
    if (max <= min) {
        min = max - 1;
        m_axisMinSliderZ->setValue(min);
    }
    float minX = m_stepZ * min + m_rangeMinZ;

    setAxisZRange(minX, maxX);
}

//! [5]
void SurfaceGraph::setAxisXRange(float min, float max)
{
    m_graph->axisX()->setRange(min, max);
}

void SurfaceGraph::setAxisZRange(float min, float max)
{
    m_graph->axisZ()->setRange(min, max);
}
//! [5]

//! [6]
void SurfaceGraph::changeTheme(int theme)
{
    m_graph->activeTheme()->setType(Q3DTheme::Theme(theme));
}
//! [6]

void SurfaceGraph::setBlackToYellowGradient()
{
    //! [7]
    QLinearGradient gr;
    gr.setColorAt(0.0, Qt::black);
    gr.setColorAt(0.33, Qt::blue);
    gr.setColorAt(0.67, Qt::red);
    gr.setColorAt(1.0, Qt::yellow);

    m_graph->seriesList().at(0)->setBaseGradient(gr);
    m_graph->seriesList().at(0)->setColorStyle(Q3DTheme::ColorStyleRangeGradient);
    //! [7]
}

void SurfaceGraph::setGreenToRedGradient()
{
    QLinearGradient gr;
    gr.setColorAt(0.0, Qt::darkGreen);
    gr.setColorAt(0.5, Qt::yellow);
    gr.setColorAt(0.8, Qt::red);
    gr.setColorAt(1.0, Qt::darkRed);

    m_graph->seriesList().at(0)->setBaseGradient(gr);
    m_graph->seriesList().at(0)->setColorStyle(Q3DTheme::ColorStyleRangeGradient);
}

void SurfaceGraph::setVolatilityGradient()
{
    //! [7]
    QLinearGradient gr;
    gr.setColorAt(0.2, Qt::blue);
    gr.setColorAt(0.27, Qt::green);
    gr.setColorAt(0.35, Qt::red);
    gr.setColorAt(0.40, Qt::yellow);

    m_graph->seriesList().at(0)->setBaseGradient(gr);
    m_graph->seriesList().at(0)->setColorStyle(Q3DTheme::ColorStyleRangeGradient);
    //! [7]
}

void SurfaceGraph::setVolatilitySurface(Matrix* mPtr){

//    Matrix surf(6,6);
    Matrix optionMaturity(1,6);
    Matrix bondMaturity(1,6);

//    surf[0][0]=0.363; surf[0][1]=0.348; surf[0][2]=0.335; surf[0][3]=0.315; surf[0][4]=0.310; surf[0][5]=0.303;
//    surf[1][0]=0.335; surf[1][1]=0.302; surf[1][2]=0.292; surf[1][3]=0.273; surf[1][4]=0.266; surf[1][5]=0.264;
//    surf[2][0]=0.295; surf[2][1]=0.271; surf[2][2]=0.265; surf[2][3]=0.250; surf[2][4]=0.241; surf[2][5]=0.242;
//    surf[3][0]=0.257; surf[3][1]=0.248; surf[3][2]=0.239; surf[3][3]=0.229; surf[3][4]=0.225; surf[3][5]=0.228;
//    surf[4][0]=0.244; surf[4][1]=0.240; surf[4][2]=0.235; surf[4][3]=0.230; surf[4][4]=0.231; surf[4][5]=0.242;
//    surf[5][0]=0.225; surf[5][1]=0.227; surf[5][2]=0.228; surf[5][3]=0.230; surf[5][4]=0.241; surf[5][5]=0.263;


    optionMaturity[0][0]=1; optionMaturity[0][1]=2; optionMaturity[0][2]=3; optionMaturity[0][3]=5; optionMaturity[0][4]=7; optionMaturity[0][5]=10;
    bondMaturity[0][0]=1;   bondMaturity[0][1]=2;   bondMaturity[0][2]=3;   bondMaturity[0][3]=5;   bondMaturity[0][4]=7;   bondMaturity[0][5]=10;

    QSurfaceDataArray *dataArray = new QSurfaceDataArray;
    dataArray->reserve(6);

    double maxY=0.0;
    double minY=10.0;
    double margin;

    for (int i = 0 ; i < 6 ; i++) {
        QSurfaceDataRow *newRow = new QSurfaceDataRow(6);
        for (int j = 0; j < 6; j++) {
            (*newRow)[j].setPosition(QVector3D(optionMaturity[0][i], (*mPtr)[i][j], bondMaturity[0][j]));
            if (maxY<(*mPtr)[i][j]){
                maxY=(*mPtr)[i][j];
            }
            if (minY>(*mPtr)[i][j]){
                minY=(*mPtr)[i][j];
            }
        }
        *dataArray << newRow;
    }

    qDebug()<<minY<<"minY";
    qDebug()<<maxY<<"maxY";
    margin=(maxY-minY)/10.0;


    m_graph->removeSeries(m_sqrtSinSeries);

    m_sqrtSinProxy = new QSurfaceDataProxy();
    m_sqrtSinProxy->resetArray(dataArray);

    m_sqrtSinSeries = new QSurface3DSeries(m_sqrtSinProxy);



    m_sqrtSinSeries->setDrawMode(QSurface3DSeries::DrawSurfaceAndWireframe);
    m_sqrtSinSeries->setFlatShadingEnabled(true);

    m_graph->axisX()->setLabelFormat("%.2f");
    m_graph->axisZ()->setLabelFormat("%.2f");
    m_graph->axisX()->setRange(0,10);
    m_graph->axisY()->setRange(minY-margin,maxY+margin);
    m_graph->axisZ()->setRange(0,10);
    m_graph->axisX()->setLabelAutoRotation(30);
    m_graph->axisY()->setLabelAutoRotation(90);
    m_graph->axisZ()->setLabelAutoRotation(30);


    //! [3]

    //! [8]
    // Reset range sliders for Sqrt&Sin
    m_rangeMinX = sampleMin;
    m_rangeMinZ = sampleMin;
    m_stepX = (sampleMax - sampleMin) / float(sampleCountX - 1);
    m_stepZ = (sampleMax - sampleMin) / float(sampleCountZ - 1);
    m_axisMinSliderX->setMaximum(sampleCountX - 2);
    m_axisMinSliderX->setValue(0);
    m_axisMaxSliderX->setMaximum(sampleCountX - 1);
    m_axisMaxSliderX->setValue(sampleCountX - 1);
    m_axisMinSliderZ->setMaximum(sampleCountZ - 2);
    m_axisMinSliderZ->setValue(0);
    m_axisMaxSliderZ->setMaximum(sampleCountZ - 1);
    m_axisMaxSliderZ->setValue(sampleCountZ - 1);
    //! [8]
    m_graph->addSeries(m_sqrtSinSeries);

    QLinearGradient gr;
    gr.setColorAt(0, Qt::blue);
    gr.setColorAt(0.33, Qt::green);
    gr.setColorAt(0.66, Qt::red);
    gr.setColorAt(1, Qt::yellow);

    m_graph->seriesList().at(0)->setBaseGradient(gr);
    m_graph->seriesList().at(0)->setColorStyle(Q3DTheme::ColorStyleRangeGradient);

}
