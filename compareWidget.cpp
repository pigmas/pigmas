
#include "comparewidget.h"

#include "ql/math/randomnumbers/randomsequencegenerator.hpp"
#include "ql/math/randomnumbers/mt19937uniformrng.hpp"
#include "ql/math/distributions/normaldistribution.hpp"


#include "delegate.h"
#include <QSizePolicy>
#include <QInputDialog>
#include <QBarSeries>
#include <QBarSet>
#include <QChart>
#include <QSplitter>


using namespace QuantLib;
class GeneralHullWhite;


CompareWidget::CompareWidget(QWidget *parent)
    : QWidget(parent)
{
    init();
    signalSetting();
}

CompareWidget::CompareWidget(MainWindow* mainPtr)
{
    mainPtr_=mainPtr;
    init();
    layoutSetting();
    signalSetting();

    absoluteTable = new Spreadsheet(mainPtr_);
    relativeTable = new Spreadsheet(mainPtr_);

    absoluteTable->setVolTable();
    relativeTable->setVolTable();

    //mainLayout->addLayout(graphLayout);
    //mainLayout->addLayout(tableLayout);
    QWidget* graphLayoutContainer = new QWidget;
    QWidget* tableLayoutContainer = new QWidget;
    graphLayoutContainer->setLayout(graphLayout);
    tableLayoutContainer->setLayout(tableLayout);
    graphLayoutContainer->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    tableLayoutContainer->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    tableLayoutContainer->setFixedHeight(230);
    QSplitter* mainSplitter= new QSplitter;
    mainSplitter->addWidget(graphLayoutContainer);
    mainSplitter->addWidget(tableLayoutContainer);

    mainSplitter->setOrientation(Qt::Vertical);
    mainLayout->addWidget(mainSplitter);

    absoluteContainerLayout->addWidget(absoluteLabel);
    absoluteLabel->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
    absoluteContainer->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    absoluteContainerLayout->addWidget(absoluteContainer);
    relativeContainerLayout->addWidget(relativeLabel);
    relativeContainer->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    relativeLabel->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
    relativeContainerLayout->addWidget(relativeContainer);

    graphLayout->addLayout(absoluteContainerLayout);
    graphLayout->addLayout(relativeContainerLayout);

    tableLayout->addWidget(absoluteTable);
    tableLayout->addWidget(relativeTable);


    this->setLayout(mainLayout);

}

void CompareWidget::init(){

}

void CompareWidget::signalSetting(){

}


void CompareWidget::layoutSetting(){



}

void CompareWidget::barGraphUpdate(){

    Matrix* absolute = new Matrix(6,6);
    Matrix* relative = new Matrix(6,6);

    Matrix* marketPrice = mainPtr_->calibrationWidget->currentMarketPricePtr_;
    Matrix* modelPrice = mainPtr_->calibrationWidget->currentModelPricePtr_;

    for(int i=0;i<6;i++){
        for(int j=0;j<6;j++){
            (*absolute)[i][j]=(*marketPrice)[i][j]-(*modelPrice)[i][j];
        }
    }

    for(int i=0;i<6;i++){
        for(int j=0;j<6;j++){
            (*relative)[i][j]=((*marketPrice)[i][j]-(*modelPrice)[i][j])/(*marketPrice)[i][j]*100.0;
        }
    }

    absoluteTable->readMatrix(absolute);
    relativeTable->readMatrix(relative);

    absoluteModifier->setData(absolute);
    relativeModifier->setData(relative);
    relativeModifier->setRelativeGraph();

    absoluteGraph->setTitle(tr("Absolute Errors"));
    relativeGraph->setTitle(tr("Relative Errors"));


}
