#ifndef DATABASEWIDGET_H
#define DATABASEWIDGET_H

#include <QWidget>
#include <QTableWidget>
#include "mainwindow.h"
#include "surfacegraph.h"
#include "bargraph.h"
#include "ql/math/matrix.hpp"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QComboBox>
#include <QGridLayout>

#include <QtCharts/QLineSeries>
#include <QtCharts/QChartView>

#include<QtDataVisualization/Q3DSurface>
#include<QtDataVisualization/QSurface3DSeries>
#include<QtDataVisualization/Q3DBars>

class MainWindow;
class Spreadsheet;
class QtCharts::QChartView;


using namespace QuantLib;
using namespace QtDataVisualization;

class DatabaseWidget : public QWidget
{
    Q_OBJECT

public:
    DatabaseWidget(QWidget *parent = 0);
    DatabaseWidget(MainWindow* mainPtr = 0);

    QTableWidget* yieldTable;
    QTableWidget* volTable;
    QLabel* yieldLabel = new QLabel("KTB Spot Rate");
    QLabel* volLabel = new QLabel("KRW Swaption Black Vol");
    QVBoxLayout* yieldLayout = new QVBoxLayout;
    QVBoxLayout* volLayout = new QVBoxLayout;

    QHBoxLayout* mainLayout = new QHBoxLayout;


    void init();
    void layoutSetting();
    void signalSetting();
    void setYieldTable();
    void setVolTable();

private:
    MainWindow* mainPtr_;

private slots:

signals:
    void updated();
};

#endif
