#include "mytreeitem.h"

MyTreeItem::MyTreeItem()
{

}

void MyTreeItem::setDataMatrix(Matrix &m){
    dataMatrix=m;
}

void MyTreeItem::setYieldTermStructure(boost::shared_ptr<MY::YieldTermStructure>& ts){
    TermStructure=*ts;
}

Matrix* MyTreeItem::getDataMatrixPtr(){
    return &dataMatrix;
}

boost::shared_ptr<MY::YieldTermStructure> MyTreeItem::getTermStructurePtr(){
    return boost::shared_ptr<MY::YieldTermStructure>(&TermStructure);
}

MY::YieldTermStructure MyTreeItem::getTermStructure(){

    return TermStructure;

}
