#include "RealScenWidget.h"

using namespace QuantLib;

RealScenWidget::RealScenWidget(QWidget *parent)
    : QWidget(parent)
{   qDebug()<<"# RealScenWidget Constructor Called";
    setMainPtr();
    init();
}

void RealScenWidget::init(){

    myDelegate = new ItemDelegate(this);
    noDelegate = new NoEditDelegate(this);

    layoutSetting();
    signalSetting();

}

void RealScenWidget::signalSetting(){
    connect(modelComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(on_modelComboBox_changed()));
    connect(generatePush,SIGNAL(clicked(bool)),this,SLOT(on_generatePush_clicked()));
    connect(savePush,SIGNAL(clicked(bool)),this,SLOT(on_savePush_clicked()));
    connect(tempScenarioList,SIGNAL(currentRowChanged(int)),this,SLOT(on_tempScenarioList_Changed()));
}

void RealScenWidget::layoutSetting(){

    optionContainer->setLayout(optionLayout);
    optionLayout->addWidget(modelLabel);
    optionLayout->addWidget(modelComboBox);
    optionLayout->addWidget(paramsLabel);
    optionLayout->addWidget(paramsTable);
    optionLayout->addWidget(rnLabel);
    optionLayout->addWidget(rnComboBox);
    optionLayout->addWidget(generatePush);
    optionLayout->addWidget(savePush);
    optionLayout->addWidget(tempScenarioLabel);
    optionLayout->addWidget(tempScenarioList);
    chartContainer->setLayout(chartContainerLayout);
    chartContainerLayout->addWidget(chart1Container);
    chart1Container->setLayout(chart1Layout);
    chart1Layout->addWidget(customPlot);

    mainSplitter->addWidget(optionContainer);
    mainSplitter->addWidget(chartContainer);
    mainLayout->addWidget(mainSplitter);

    this->setLayout(mainLayout);

    // 3. Implementation
    optionContainer->setFixedWidth(300);
    paramsTable->setItemDelegateForColumn(0,noDelegate);
    paramsTable->setItemDelegateForColumn(1,myDelegate);
    paramsTable->setModel(paramsTableModel);
    myDelegate->setMainPtr(mainPtr_);

    // 5 .Size Policy
    chart1Container->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    customPlot->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    paramsTable->horizontalHeader()->hide();
    paramsTable->verticalHeader()->hide();
    paramsTable->horizontalHeader()->setStretchLastSection(1);
    paramsTable->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Expanding);
    paramsTable->setFixedHeight(2);
}

void RealScenWidget::on_modelComboBox_changed(){
    qDebug()<<" #RealScenWidget::on_modelComboBox_changed() Called";
    updateParamsTable();
}

void RealScenWidget::on_generatePush_clicked(){
    qDebug()<<" #RealScenWidget::on_generatePush_clicked() Called";
    generate();
}

void RealScenWidget::on_tempScenarioList_Changed(){
    updateChart();
}

void RealScenWidget::updateModelComboBox(){
    qDebug()<<"# RealScenWidget::updateModelComboBox() Called";
    modelComboBox->blockSignals(1);
    modelComboBox->clear();
    for(int i=0;i<mainPtr_->modelDB_.size();i++){
        modelComboBox->addItem(QString::fromStdString(mainPtr_->modelDB_.at(i)->modelName));
    }
    modelComboBox->blockSignals(0);
}

void RealScenWidget::updateParamsTable(){
    qDebug()<<"# RealScenWidget::updateParamsTable() Called";

    int modelIndex = modelComboBox->currentIndex();
    if(modelIndex!=-1){

    std::vector<double> params = mainPtr_->modelDB_.at(modelIndex)->getParams();
    std::vector<QString> paramsName = mainPtr_->modelDB_.at(modelIndex)->getParamsName();
    int numParams = params.size();

    paramsTableModel->clear();
    paramsTableModel->setColumnCount(2);
    paramsTableModel->setRowCount(numParams);


    for(int i=0;i<numParams;i++){

        QStandardItem* nameItem = new QStandardItem;
        nameItem->setBackground(QBrush(QColor(Qt::lightGray)));
        nameItem->setTextAlignment(Qt::AlignCenter);
        QStandardItem* paramItem = new QStandardItem;
        paramItem->setTextAlignment(Qt::AlignCenter);

        nameItem->setData(paramsName[i],Qt::DisplayRole);
        paramItem->setData(params[i],Qt::DisplayRole);

        paramsTableModel->setItem(i,0,nameItem);
        paramsTableModel->setItem(i,1,paramItem);
    }

    int rowHeight = paramsTable->rowHeight(0);
    int rows = paramsTableModel->rowCount();
    paramsTable->setFixedHeight(rowHeight*rows+2);
    }else{
    paramsTable->setFixedHeight(2);
    }

}
void RealScenWidget::updateRnComboBox(){
    qDebug()<<"# RealScenWidget::updateRnComboBox() Called";
    rnComboBox->blockSignals(1);
    rnComboBox->clear();
    for(int i=0;i<mainPtr_->randomNumberDB_.size();i++){
        rnComboBox->addItem(mainPtr_->randomNumberDB_.at(i)->name);
    }
    rnComboBox->blockSignals(0);
}
void RealScenWidget::updateScenList(){
    qDebug()<<"# RealScenWidget::updateScenList() Called";
    tempScenarioList->blockSignals(1);
    tempScenarioList->clear();
    int numRow=mainPtr_->scenarioDB_.size();
    for(int i=0;i<numRow;i++){
        tempScenarioList->addItem(mainPtr_->scenarioDB_.at(i)->name);
    }
    tempScenarioList->setCurrentRow(numRow-1);
    tempScenarioList->blockSignals(0);
}
void RealScenWidget::updateChart(){
    qDebug()<<"# RealScenWidget::updateChart() Called";
    customPlot->clearGraphs();
    //customPlot->clearPlottables();
    if(tempScenarioList->currentRow()!=-1){
        Matrix scen=mainPtr_->scenarioDB_.at(tempScenarioList->currentRow())->dataMat;
        int numRow=scen.size1();
        int numCol=scen.size2();

        double yMin = 0.0;
        double yMax = 0.0;
        QVector<double> x(numCol), y(numCol);
        float currentHue = 0.0;
        for(int i=0;i<numRow;i++){
            QCPGraph* graph = customPlot->addGraph();
            for(int j=0;j<numCol;j++){
                x[j]=j;
                y[j]=scen[i][j];
                if(y[j]<yMin){
                    yMin=y[j];
                }
                if(y[j]>yMax){
                    yMax=y[j];
                }
            }
            graph->setData(x,y);
            currentHue += 0.618033988749895f;
            currentHue = std::fmod(currentHue, 1.0f);
            graph->setPen(QPen(QColor::fromHslF(currentHue, 1.0, 0.5)));
        }
        customPlot->xAxis->setRange(x[0],x[numCol-1]);
        customPlot->yAxis->setRange(yMin-0.005,yMax+0.005);
    }
    customPlot->replot();
}

void RealScenWidget::updateAll(){
    qDebug()<<"# RealScenWidget::updateAll() Called";
    updateModelComboBox();
    updateParamsTable();
    updateRnComboBox();
    updateScenList();
    updateChart();
}

void RealScenWidget::generate(){
    qDebug()<<"# RealScenWidget::generate() Called";
    int modelIndex = modelComboBox->currentIndex();
    int rnIndex = rnComboBox->currentIndex();

    if(modelIndex!=-1 && rnIndex!=-1){
        bool ok;
        QString varName = QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                      tr("Scenario Name:"), QLineEdit::Normal,
                                      "", &ok);
        if(varName==""){
            varName="null";
        }

        if(ok==1){
            qDebug()<<" Scenario Generate";

            //QString fileName1="c:/rndtest.csv";
            //ifstream rndStream(fileName1.toStdString());
            //Matrix rnd = dm->csv_Matrix(rndStream);
            Matrix rnd = mainPtr_->randomNumberDB_.at(rnIndex)->dataMat;
            Matrix scen(rnd.size1(),rnd.size2());
            if(modelIndex!=-1){
                scen = mainPtr_->modelDB().at(modelIndex)->scenario(rnd);
                mainPtr_->scenarioDB_.push_back(boost::make_shared<DataClass>());
                mainPtr_->scenarioDB_.back()->dataMat = scen;
                mainPtr_->scenarioDB_.back()->name = varName;
                updateScenList();
                updateChart();
                mainPtr_->updateTree();
            }else{
                qDebug()<<"Model is Empty";
            }

        }else{
            qDebug()<<" Scenario Generate Cancled";
        }

    }else{
        qDebug()<<" Can't Generate Scenario Generate";
    }

}

void RealScenWidget::addTemp(QString varName,Matrix scen){
    qDebug()<<"# RealScenWidget::addTemp(QString varName,Matrix scen) Called";

    //tempScenarioList->addItem(varName);
    //tempScenVector_.push_back(boost::make_shared<DataClass>());


}

void RealScenWidget::delTemp(){
    qDebug()<<"# RealScenWidget::delTemp(QString varName,Matrix scen) Called";
}

void RealScenWidget::on_savePush_clicked(){
    qDebug()<<"# RealScenWidget::on_savePush_clicked() Called";
}

RealScenWidget::~RealScenWidget(){
    qDebug()<<"# RealScenWidget Destructor Called";
}
