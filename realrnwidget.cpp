#include "RealRnWidget.h"

#include "ql/math/randomnumbers/randomsequencegenerator.hpp"
#include "ql/math/randomnumbers/mt19937uniformrng.hpp"
#include "ql/math/distributions/normaldistribution.hpp"

using namespace QuantLib;

RealRnWidget::RealRnWidget(QWidget *parent)
    : QWidget(parent)
{
    qDebug()<<"# RealRnWidget Constructor Called";
    setMainPtr();
    init();
    layoutSetting();
    signalSetting();
}


void RealRnWidget::init(){

    seedEdit->setText("28749");
    maturityEdit->setText("360");
    nEdit->setText("200");

}

void RealRnWidget::layoutSetting(){

    optionContainer->setLayout(optionLayout);
    optionContainer->setFixedWidth(300);

    mainLayout->addWidget(optionContainer);
    mainLayout->addLayout(histLayout);
    histLayout->addLayout(histOptionLayout);
    histLayout->addLayout(chartLayout);

    optionLayout->addWidget(methodLabel);
    optionLayout->addWidget(methodComboBox);

    optionLayout->addWidget(seedLabel);
    optionLayout->addWidget(seedEdit);
    optionLayout->addWidget(maturityLabel);
    optionLayout->addWidget(maturityEdit);
    optionLayout->addWidget(nLabel);
    optionLayout->addWidget(nEdit);
    optionLayout->addWidget(generatePush);
    optionLayout->addWidget(tempListLabel);
    optionLayout->addWidget(tempList);

    histOptionLayout->addWidget(maturityHistLabel);
    histOptionLayout->addWidget(maturityHistComboBox);
    histOptionLayout->addWidget(nHistLabel);
    histOptionLayout->addWidget(nHistComboBox);
    chartLayout->addWidget(customPlot1);
    chartLayout->addWidget(customPlot2);

    methodComboBox->addItem("Mersenne Twister 19937");

    this->setLayout(mainLayout);
    // 4. Size Policy
    customPlot1->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    customPlot2->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    // 5. Test

}

void RealRnWidget::signalSetting(){
    connect(generatePush,SIGNAL(clicked(bool)),this,SLOT(on_generatePush_clicked()));
    connect(tempList,SIGNAL(currentRowChanged(int)),this,SLOT(on_tempList_Changed()));
    connect(maturityHistComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(on_Maturity_Changed()));
    connect(nHistComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(on_N_Changed()));
}
void RealRnWidget::on_tempList_Changed(){
    qDebug()<<"# RealRnWidget::on_tempList_Changed(int n) Called";
    updateHistCombo();
    updateChart1();
    updateChart2();
}

void RealRnWidget::on_Maturity_Changed(){
    qDebug()<<"# RealRnWidget::on_Maturity_Changed() Called";
    if(maturityHistComboBox->currentIndex()!=-1){
        updateChart1();
    }
}
void RealRnWidget::on_N_Changed(){
    qDebug()<<"# RealRnWidget::on_N_Changed() Called";
    if(nHistComboBox->currentIndex()!=-1){
        updateChart2();
    }
}

void RealRnWidget::on_generatePush_clicked(){
    qDebug()<<"# RealRnWidget::on_generatePush_clicked() Called";
    generate();
}

void RealRnWidget::generate(){
    qDebug()<<"# RealRnWidget::generate() Called";
    bool ok;
    QString varName = QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                  tr("Random Number Name:"), QLineEdit::Normal,
                                  "", &ok);
    if(varName==""){
        varName="null";
    }
    if(ok==1){
        QString seed = seedEdit->text();
        QString numRow = nEdit->text();
        QString numCol = maturityEdit->text();

        MersenneTwisterUniformRng uGenerator(seed.toInt());
        Matrix rnd(numRow.toInt(),numCol.toInt());
        InverseCumulativeNormal normInv;

        for (int i=0; i<numRow.toInt();i++){
            for (int j=0; j<numCol.toInt(); ++j){
              rnd[i][j] = normInv(uGenerator.next().value);
            }
        }
        addTemp(varName,rnd);
    }

}
void RealRnWidget::updateList(){
    qDebug()<<"# RealRnWidget::updateList() Called";
    tempList->blockSignals(1);
    tempList->clear();
    int numRow=mainPtr_->randomNumberDB_.size();
    for(int i=0;i<numRow;i++){
        tempList->addItem(mainPtr_->randomNumberDB_.at(i)->name);
    }
    tempList->setCurrentRow(numRow-1);
    tempList->blockSignals(0);
}

void RealRnWidget::updateChart1(){
    qDebug()<<"# RealRnWidget::updateChart1() Called";
    //customPlot1->clearGraphs();
    customPlot1->clearPlottables();
    if(tempList->currentRow()!=-1){
        Matrix rnd=mainPtr_->randomNumberDB_.at(tempList->currentRow())->dataMat;
        int numRow=rnd.size1();

        Matrix dat1=Matrix(1,numRow);
        for (int i=0;i<numRow;i++){
            dat1[0][i]=rnd[i][maturityHistComboBox->currentIndex()];
        }

        Matrix histMatrix1 = hist(dat1, -3, 3, 15);
        int numBar=histMatrix1.size2();
        QVector<double> x(numBar), y(numBar);
        QCPBars* bars = new QCPBars(customPlot1->xAxis,customPlot1->yAxis);
       for(int i=0;i<numBar;i++){
            x[i]=histMatrix1[0][i];
            y[i]=histMatrix1[1][i];
       }
        bars->setData(x,y);
        bars->setPen(QPen(QColor(Qt::blue).lighter()));
        bars->setBrush(QColor(Qt::blue));
        double xMin = *std::min_element(x.begin(),x.end());
        double xMax = *std::max_element(x.begin(),x.end());
        double yMax = *std::max_element(y.begin(),y.end());
        bars->setWidth(x[1]-x[0]);
        customPlot1->xAxis->setRange(xMin-0.3,xMax+0.3);
        customPlot1->yAxis->setRange(0,yMax+10);
        //set1->setLabel(maturityHistComboBox->currentText()+" Month Random Number Histogram");
    }
    customPlot1->replot();
}
void RealRnWidget::updateChart2(){
    qDebug()<<"# RealRnWidget::updateChart2() Called";
    customPlot2->clearPlottables();
    if(tempList->currentRow()!=-1){

        Matrix rnd=mainPtr_->randomNumberDB_.at(tempList->currentRow())->dataMat;
        int numCol=rnd.size2();

        Matrix dat2=Matrix(1,numCol);
        for (int j=0;j<numCol;j++){
            dat2[0][j]=rnd[nHistComboBox->currentIndex()][j];
        }

        Matrix histMatrix2 = hist(dat2, -3, 3, 15);
        int numBar=histMatrix2.size2();
        QVector<double> x(numBar), y(numBar);
        QCPBars* bars = new QCPBars(customPlot2->xAxis,customPlot2->yAxis);
        for(int i=0;i<numBar;i++){
            x[i]=histMatrix2[0][i];
            y[i]=histMatrix2[1][i];
        }
        bars->setData(x,y);
        bars->setPen(QPen(QColor(Qt::blue).lighter()));
        bars->setBrush(QColor(Qt::blue));
        double xMin = *std::min_element(x.begin(),x.end());
        double xMax = *std::max_element(x.begin(),x.end());
        double yMax = *std::max_element(y.begin(),y.end());
        bars->setWidth(x[1]-x[0]);
        customPlot2->xAxis->setRange(xMin-0.3,xMax+0.3);
        customPlot2->yAxis->setRange(0,yMax+10);
        //set2->setLabel(nHistComboBox->currentText()+" Scenario Random Number Histogram");
    }
    customPlot2->replot();
}
void RealRnWidget::updateHistCombo(){
    qDebug()<<"# RealRnWidget::updateHistCombo() Called";
    maturityHistComboBox->blockSignals(1);
    nHistComboBox->blockSignals(1);

    maturityHistComboBox->clear();
    nHistComboBox->clear();
    int listIndex = tempList->currentRow();
    if(listIndex!=-1){
        int numRow=mainPtr_->randomNumberDB_.at(listIndex)->dataMat.size1();
        int numCol=mainPtr_->randomNumberDB_.at(listIndex)->dataMat.size2();
        for(int j=0;j<numCol;j++){
            maturityHistComboBox->addItem(QString::number(j+1));
        }
        for(int i=0;i<numRow;i++){
            nHistComboBox->addItem(QString::number(i+1));
        }
    }
    maturityHistComboBox->blockSignals(0);
    nHistComboBox->blockSignals(0);
}
void RealRnWidget::updateAll(){
    updateList();
    updateHistCombo();
    updateChart1();
    updateChart2();
}

void RealRnWidget::addTemp(QString varName,Matrix rnd){
    qDebug()<<"# RealRnWidget::addTemp() Called";
    mainPtr_->randomNumberDB_.push_back(boost::make_shared<DataClass>());
    mainPtr_->randomNumberDB_.back()->dataMat = rnd;
    mainPtr_->randomNumberDB_.back()->name = varName;
    updateList();
    updateHistCombo();
    updateChart1();
    updateChart2();
    mainPtr_->updateTree();
    mainPtr_->scenWidget->updateRnComboBox();
}
void RealRnWidget::delTemp(){
    qDebug()<<"# RealRnWidget::delTemp() Called";
    if(tempList->currentIndex().row()!=-1){
        mainPtr_->randomNumberDB_.erase(mainPtr_->randomNumberDB_.begin()+tempList->currentIndex().row());
    }
    updateList();
}


Matrix RealRnWidget::hist(Matrix dat, double first, double last, int n){

    double step=(last-first)/(n-2.0);
    std::vector<double> hold;

    for(int i=0;i<n-1;i++){
        hold.push_back(first+i*step);
    }
    Matrix nMatrix = Matrix(1,n);

    for(int i=0;i<nMatrix.size2();i++){
        nMatrix[0][i]=0.0;
    }
    for(int i=0;i<dat.size2();i++){

        for(int j=0;j<hold.size();j++){
            if(dat[0][i]<hold[j]){
                nMatrix[0][j]=nMatrix[0][j]+1;
            }

        }
    }
    nMatrix[0][n-1]=dat.size2();
    for(int i=0;i<nMatrix.size2();i++){
        //qDebug()<<nMatrix[0][i];
    }

    Matrix histMatrix = Matrix(2,n);
    histMatrix[0][0]=first-step*0.5;
    histMatrix[1][0]=nMatrix[0][0];
    for(int i=1;i<nMatrix.size2();i++){
        histMatrix[0][i]=first-step*0.5+i*step;
        histMatrix[1][i]=nMatrix[0][i]-nMatrix[0][i-1];
    }

    return histMatrix;

}

RealRnWidget::~RealRnWidget(){
    qDebug()<<"# RealRnWidget Destructor Called";

}

