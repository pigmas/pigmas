#include <stdlib.h>
#include <crtdbg.h>

#include <gsl/gsl_errno.h>
#include <QApplication>
#include <QTranslator>

#include "loginwidget.h"
#include "mainwindow.h"
#include "realmainwindow.h"
#include "realcalibrationwidget.h"
#include "realrnwidget.h"
#include "realscenwidget.h"

int main(int argc, char *argv[])
{
    gsl_set_error_handler_off();
    _CrtCheckMemory();
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
    //_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
    //_Crt_DumpMemoryLeaks();
    QApplication app(argc, argv);
    QTranslator T;
    T.load(":/korean.qm");
    app.installTranslator(&T);

    LoginWidget loginWidget;
    loginWidget.show();
    //RealCalibrationWidget realCalib;
    //realCalib.showMaximized();
    //RealRnWidget realRn;
    //realRn.showMaximized();
    //RealScenWidget realScen;
    //realScen.showMaximized();

    //RealMainWindow realMain;
    //realMain.showMaximized();
    return app.exec();

}


