#ifndef SMITHWILSONENGINE_H
#define SMITHWILSONENGINE_H

#include "ql/math/matrix.hpp"
#include "datamanager.h"

using namespace QuantLib;


class SmithWilsonEngine
{
public:

    SmithWilsonEngine();

    Matrix generate();

    void setTenor(Matrix tenor){tenor_=tenor;}
    void setGridRate(Matrix gridRate){gridRate_=gridRate;}
    void setUFR(double UFR){UFR_=UFR;}
    void setAlpha(double alpha){alpha_=alpha;}
    void setRate(Matrix rateTable);

    Matrix getTenor(){return tenor_;}
    Matrix getGridRate(){return gridRate_;}
    double getUFR(){return UFR_;}
    double getAlpha(){return alpha_;}

private:
    DataManager dm_;
    Matrix tenor_;
    Matrix gridRate_;
    double UFR_;
    double alpha_;

};

#endif // SMITHWILSONENGINE_H
