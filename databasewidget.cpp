
#include "DatabaseWidget.h"

#include "ql/math/randomnumbers/randomsequencegenerator.hpp"
#include "ql/math/randomnumbers/mt19937uniformrng.hpp"
#include "ql/math/distributions/normaldistribution.hpp"


#include "delegate.h"
#include <QSizePolicy>
#include <QInputDialog>
#include <QBarSeries>
#include <QBarSet>
#include <QChart>
#include <QSplitter>


using namespace QuantLib;
class GeneralHullWhite;


DatabaseWidget::DatabaseWidget(QWidget *parent)
    : QWidget(parent)
{
    init();
    signalSetting();

}

DatabaseWidget::DatabaseWidget(MainWindow* mainPtr)
{
    mainPtr_=mainPtr;
    init();
    layoutSetting();
    signalSetting();


}

void DatabaseWidget::init(){

}

void DatabaseWidget::signalSetting(){
    //connect(this->optionCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(on_optionCombo_changed()));
    //connect(this->swapCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(on_swapCombo_changed()));
    //connect(this->generatePush,SIGNAL(clicked(bool)),this,SLOT(on_generatePush_clicked));
}


void DatabaseWidget::layoutSetting(){

    yieldTable = new QTableWidget;
    volTable = new QTableWidget;

    //mainLayout->addLayout(yieldLayout);
    //mainLayout->addLayout(volLayout);

    QWidget* yieldLayoutContainer = new QWidget;
    QWidget* volLayoutContainer = new QWidget;
    yieldLayoutContainer->setLayout(yieldLayout);
    volLayoutContainer->setLayout(volLayout);
    QSplitter* mainSplitter = new QSplitter;
    mainSplitter->setOrientation(Qt::Horizontal);
    mainSplitter->addWidget(yieldLayoutContainer);
    mainSplitter->addWidget(volLayoutContainer);
    mainSplitter->setSizes({400,600});
    mainLayout->addWidget(mainSplitter);


    yieldLayout->addWidget(yieldLabel);
    yieldLayout->addWidget(yieldTable);

    volLayout->addWidget(volLabel);
    volLayout->addWidget(volTable);



    this->setLayout(mainLayout);
}

void DatabaseWidget::setYieldTable(){

    vector<Matrix>& yieldDB = mainPtr_->dataManager->yieldDB;
    vector<Date>& dateVector = mainPtr_->dataManager->dateVector;
    Matrix& yieldDbTenor = mainPtr_->dataManager->yieldDbTenor;


    yieldTable->setRowCount(yieldDB.size()+1);
    yieldTable->setColumnCount(yieldDbTenor.size2()+1);

    QTableWidgetItem* c = new QTableWidgetItem;//cell
    yieldTable->setItem(0,0,c);//table
    c->setText("Date");
    for(int i=1;i<dateVector.size()+1;i++){
        QTableWidgetItem* c = new QTableWidgetItem;//cell
        yieldTable->setItem(i,0,c);//table

        std::ostringstream out;
        out<<QuantLib::io::iso_date(mainPtr_->dataManager->dateVector.at(i-1));
        //c->setText(QString::fromStdString(out.str()));
        c->setData(Qt::DisplayRole,QString::fromStdString(out.str()));
    }

    for (int j=1;j<yieldDbTenor.size2()+1;j++){
        QTableWidgetItem* c = new QTableWidgetItem;
        yieldTable->setItem(0,j,c);
        c->setData(Qt::DisplayRole,yieldDbTenor[0][j-1]);
    }

    for(int i=1;i<dateVector.size()+1;i++){
        for (int j=1;j<yieldDbTenor.size2()+1;j++){
            QTableWidgetItem* c = new QTableWidgetItem;
            yieldTable->setItem(i,j,c);
            c->setData(Qt::DisplayRole,yieldDB.at(i-1)[0][j-1]);
        }
    }
    yieldTable->setEditTriggers(QAbstractItemView::NoEditTriggers);


}

void DatabaseWidget::setVolTable(){
    vector<Date>& dateVector = mainPtr_->dataManager->dateVector;
    vector<Matrix>& volDB = mainPtr_->dataManager->volDB;
    volTable->setRowCount(dateVector.size()+1);
    volTable->setColumnCount(36+1);

    QStringList volIndex;
    volIndex<<"(1,1)"<<"(1,2)"<<"(1,3)"<<"(1,5)"<<"(1,7)"<<"(1,10)"
    <<"(2,1)"<<"(2,2)"<<"(2,3)"<<"(2,5)"<<"(2,7)"<<"(2,10)"
    <<"(3,1)"<<"(3,2)"<<"(3,3)"<<"(3,5)"<<"(3,7)"<<"(3,10)"
    <<"(5,1)"<<"(5,2)"<<"(5,3)"<<"(5,5)"<<"(5,7)"<<"(5,10)"
    <<"(7,1)"<<"(7,2)"<<"(7,3)"<<"(7,5)"<<"(7,7)"<<"(7,10)"
    <<"(10,1)"<<"(10,2)"<<"(10,3)"<<"(10,5)"<<"(10,7)"<<"(10,10)";

    QTableWidgetItem* c = new QTableWidgetItem;//cell
    volTable->setItem(0,0,c);//table
    c->setText("Date");

    for(int i=1;i<dateVector.size()+1;i++){
        QTableWidgetItem* c = new QTableWidgetItem;
        volTable->setItem(i,0,c);

        std::ostringstream out;
        out<<QuantLib::io::iso_date(mainPtr_->dataManager->dateVector.at(i-1));
        //c->setText(QString::fromStdString(out.str()));
        c->setData(Qt::DisplayRole,QString::fromStdString(out.str()));
    }


    for (int j=1;j<volIndex.size()+1;j++){
        QTableWidgetItem* c = new QTableWidgetItem;
        volTable->setItem(0,j,c);
        c->setData(Qt::DisplayRole,volIndex.at(j-1));
    }

    for (int i=1;i<volDB.size()+1;i++){
        for(int m=0;m<6;m++){
            for(int n=0;n<6;n++){
                QTableWidgetItem* c = new QTableWidgetItem;
                volTable->setItem(i,m*6+n+1,c);
                c->setData(Qt::DisplayRole,volDB.at(i-1)[m][n]);
            }
        }
    }
}
