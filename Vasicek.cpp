#include "Vasicek.h"
#include <gsl/gsl_sf_bessel.h>
#include <iostream>
#include <math.h>
#include <QDebug>

using namespace std;


MY::Vasicek::Vasicek(){

    modelIndex = 0;
    params_={0.04,0.05,0.06};
    paramsName_={"Alpha","Theta","Sigma"};

}

std::vector<double> MY::Vasicek::calibrate(QuantLib::Matrix rateData){
    qDebug()<<" Vasicek::calibrate Called";
	double dt = 1.0 / 12.0;

	int size = rateData.size1();
	QuantLib::Matrix x(size - 1, 2);
	QuantLib::Matrix y(size - 1, 1);

	for (int i = 0; i < size - 1; i++){
		x[i][0] = 1.0; x[i][1] = rateData[i][0];
		y[i][0] = rateData[i + 1][0];
	}

	QuantLib::Matrix ols = QuantLib::inverse(QuantLib::transpose(x)*x)*(QuantLib::transpose(x)*y);
	QuantLib::Matrix resid = y - x*ols;
	
	QuantLib::Matrix ones(1, size - 1);
	for (int j = 0; j < size - 1; j++){
		ones[0][j] = 1.0;
	}

	QuantLib::Matrix mean = (ones*resid / (size - 1));
	QuantLib::Matrix stdev = (QuantLib::transpose(resid)*resid - (size - 1)*mean*mean) / (size - 2);
	stdev[0][0] = sqrt(stdev[0][0]);

	//std::cout << "mean : " << mean << "stdev : " <<stdev <<std::endl;

	double c = ols[0][0];
	double b = ols[1][0];
	double delta = stdev[0][0];

	double alpha = -log(b) / dt;
	double theta = c / (1.0 - b);
	double sigma = delta / sqrt((pow(b, 2.0) - 1.0)*dt / (2.0*log(b)));

    params_= { alpha, theta, sigma };

	std::cout << "alpha : " << alpha << " theta : " << theta << " sigma : " << sigma << endl<<endl;

    return params_;

}

QuantLib::Matrix MY::Vasicek::scenario(QuantLib::Matrix& rnd){
    qDebug()<<" Vasicek::scenario Called";

    double dt = 1.0/12.0;
    double r0 = 0.03;
    double a = params_[0];
    double theta = params_[1];
    double sigma = params_[2];

    int numScen = rnd.size1();
    int numTerm = rnd.size2();
    QuantLib::Matrix scen(numScen,numTerm);

    for(int i=0;i<numScen;i++){
        scen[i][0]=r0;
    }

    for(int i=0;i<numScen;i++){
        for(int j=0;j<numTerm-1;j++){
            //scen[i][j+1]=scen[i][j]+a*(theta-scen[i][j])*dt+sigma*rnd[i][j]*sqrt(dt); //Approx
            scen[i][j+1]=scen[i][j]*exp(-a*dt) + theta*(1-exp(-a*dt))+(sigma*sqrt((1-exp(-2*a*dt))/(2*a))*rnd[i][j]); //Exact
        }
    }
    return scen;
}

