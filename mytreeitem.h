#ifndef MYTREEITEM_H
#define MYTREEITEM_H

#include <QObject>
#include <QStandardItem>
#include "ql/math/matrix.hpp"
#include "yieldtermstructure.h"

class YieldTermStructure;

using namespace QuantLib;

class MyTreeItem : public QStandardItem
{

public:
    MyTreeItem();
    MyTreeItem(const QString &text) : QStandardItem(text){}
    void setDataMatrix(Matrix &m);
    void setYieldTermStructure(boost::shared_ptr<MY::YieldTermStructure>& ts);
    Matrix* getDataMatrixPtr();
    boost::shared_ptr<MY::YieldTermStructure> getTermStructurePtr();
    MY::YieldTermStructure getTermStructure();

private:
    Matrix dataMatrix;
    MY::YieldTermStructure TermStructure;

};

#endif // MYTREEITEM_H
