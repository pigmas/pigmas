#ifndef DATACLASS_H
#define DATACLASS_H

#include "ql/math/matrix.hpp"
#include "ql/utilities/dataparsers.hpp"

#include <QString>
using namespace QuantLib;

class DataClass
{
public:
    DataClass();
    QString name;
    std::vector<QString> timeset;
    std::vector<double> dataset;
    Matrix dataMat;

};

#endif // DATACLASS_H
