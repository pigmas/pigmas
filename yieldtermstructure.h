#ifndef YIELDTERMSTRUCTURE_H
#define YIELDTERMSTRUCTURE_H

#include "ql/math/matrix.hpp"
#include "ql/termstructures/yieldtermstructure.hpp"
#include "ql/quote.hpp"
#include "ql/termstructure.hpp"
#include "ql/quotes/simplequote.hpp"
#include "ql/time/daycounters/simpledaycounter.hpp"


#include <QDebug>

using namespace QuantLib;
namespace MY{
class YieldTermStructure : public QuantLib::YieldTermStructure
{

public:
    YieldTermStructure(const Date& referenceDate=Date(29, December, 2017),const DayCounter& dayCounter=SimpleDayCounter());
    YieldTermStructure(Matrix dm_);
    void setDiscountMonth(Matrix dm_);
    Matrix getDiscountMonth(){return discount_Month;}
    //double discount(double t);
    Date maxDate() const { return Date::maxDate(); }
    Time maxTime() const { return discount_Month.size2(); }

private:
    Matrix discount_Month;
    DiscountFactor discountImpl(QuantLib::Time) const;


};

inline DiscountFactor YieldTermStructure::discountImpl(Time t) const {

    if (t==0){
        return 1;
    }else if (t==0.0001){
        double dt(1.0/12.0);
        double p(discount_Month[0][0]);
        qDebug()<<pow(p,0.0001/dt);
        return pow(p,0.0001/dt);
    };

    return discount_Month[0][int(std::round(t*12.0-1.0))];

}

//inline QuantLib::DiscountFactor YieldTermStructure::discountImpl(QuantLib::Time t) const {
    //calculate();
  //  return this->discount(t);
//}

}

#endif // YIELDTERMSTRUCTURE_H
