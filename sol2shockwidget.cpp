#include "Sol2ShockWidget.h"
#include "smithwilsonengine.h"
#include "datamanager.h"
#include "mainwindow.h"
#include "delegate.h"

#include <QHBoxLayout>
#include <QTableWidget>
#include <QListWidget>
#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChartView>
#include <QDebug>
#include <QInputDialog>

QT_CHARTS_USE_NAMESPACE

Sol2ShockWidget::Sol2ShockWidget(QWidget *parent)
    : QWidget(parent)
{
    init();
    signalSetting();
}

Sol2ShockWidget::Sol2ShockWidget(MainWindow* mainPtr)
{
    mainPtr_=mainPtr;
    init();
    signalSetting();
}

Sol2ShockWidget::~Sol2ShockWidget()
{

}


void Sol2ShockWidget::init(){

    sol2Table = new Spreadsheet(mainPtr_);
    tempRateTable = new Spreadsheet(mainPtr_);

    layoutSetting();

    chartView->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    tempRateTable->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Expanding);

    DataManager dm;
    QString fileName1="c:/sol2shocktable.csv";
    ifstream data1(fileName1.toStdString());

    Matrix shockMatrix = dm.csv_Matrix(data1);
    sol2Table->readMatrix(&shockMatrix);
    sol2Table->setColumnWidth(0,30);
    sol2Table->setColumnWidth(1,70);
    sol2Table->setColumnWidth(2,70);

    QStringList headerLabels;
    headerLabels.append("Year");
    headerLabels.append("Up(%)");
    headerLabels.append("Down(%)");
    sol2Table->setHorizontalHeaderLabels(headerLabels);
    sol2Table->verticalHeader()->hide();



}

void Sol2ShockWidget::layoutSetting(){


    tempRateTable->setItemDelegate(new ItemDelegate);

    curveOptionLayout->addWidget(sol2Label);
    curveOptionLayout->addWidget(sol2Table);

    curveOptionContainer->setLayout(curveOptionLayout);
    curveOptionContainer->setFixedWidth(220);

    typeComboBox->addItem("Forward");
    typeComboBox->addItem("Discount");
    typeComboBox->addItem("Spot Rate");
    //chartViewLayout->addLayout(typeLayout);
    //typeLayout->addSpacerItem(new QSpacerItem(20,20,QSizePolicy::Expanding));
    //typeLayout->addWidget(typeLabel);
    //typeLayout->addWidget(typeComboBox);
    chartViewLayout->addWidget(chartView);

    inputLabelLayout->addWidget(inputLabel);
    inputLabel->setAlignment(Qt::AlignRight);
    inputLabelLayout->addWidget(tempComboBox);


    curveLayout->addLayout(chartViewLayout);

    inputLayout->addLayout(inputLabelLayout);
    inputLayout->addWidget(tempRateTable);

    mainLayout->addWidget(curveOptionContainer);
    mainLayout->addLayout(inputLayout);
    mainLayout->addLayout(curveLayout);

    this->resize(1300,800);
    this->setLayout(mainLayout);
    update();
    voidSetting();

}

void Sol2ShockWidget::signalSetting(){

    connect(tempComboBox,SIGNAL(currentIndexChanged(const QString&)),this,SLOT(on_tempComboBox_changed()));

}


void Sol2ShockWidget::drawCurve(){
    qDebug()<<"Sol2ShockWidget drawCurve() called";

    DataManager dm;
    QString fileName1="c:/upshock.csv";
    ifstream data1(fileName1.toStdString());

    Matrix upShock = dm.csv_Matrix(data1);

    QString fileName2="c:/downshock.csv";
    ifstream data2(fileName2.toStdString());

    Matrix downShock = dm.csv_Matrix(data2);



    QChart *chart = new QChart();


            boost::shared_ptr<MY::YieldTermStructure> rth(new MY::YieldTermStructure);

            if(mainPtr_->smithWilsonWidget->temporaryCurveVector.size()!=0){

            int i=tempComboBox->currentIndex();

            rth=mainPtr_->smithWilsonWidget->temporaryCurveVector.at(i);

            QLineSeries* midSeries = new QLineSeries();
            for(int j=0;j<rth->maxTime();j++){
                double t=double(j)/12.0;
                    midSeries->append(j,(exp(rth->zeroRate(t,Continuous))-1.0)*100.0);

            }
            midSeries->setName("Base");
            chart->addSeries(midSeries);

            QLineSeries* upSeries = new QLineSeries();
            for(int j=0;j<rth->maxTime();j++){
                double t=double(j)/12.0;
                    upSeries->append(j,(exp(rth->zeroRate(t,Continuous))-1.0)*100.0*(1+upShock[j][1]/100.0));

            }
            upSeries->setName("Up");
            chart->addSeries(upSeries);

            QLineSeries* downSeries = new QLineSeries();
            for(int j=0;j<rth->maxTime();j++){
                double t=double(j)/12.0;
                    downSeries->append(j,(exp(rth->zeroRate(t,Continuous))-1.0)*100.0*(1+downShock[j][1]/100.0));

            }
            downSeries->setName("Down");
            chart->addSeries(downSeries);

            }




    chartView->setChart(chart);

    chart->createDefaultAxes();
    chart->setTitle("Solvency2 Interest rate Shock Scenario(Spot)");

    chartView->setRenderHint(QPainter::Antialiasing);


    Matrix m=Matrix(1200,4);

    for(int i=0;i<1200;i++){
        double t=double(i)/12.0;
        m[i][0]=i;
        m[i][1]=std::round((exp(rth->zeroRate(t,Continuous))-1.0)*100.0*100.0)/100.0;
        m[i][2]=std::round((exp(rth->zeroRate(t,Continuous))-1.0)*100.0*(1+upShock[i][1]/100.0)*100.0)/100.0;
        m[i][3]=std::round((exp(rth->zeroRate(t,Continuous))-1.0)*100.0*(1+downShock[i][1]/100.0)*100.0)/100.0;
    }

    tempRateTable->readMatrix(&m);
    tempRateTable->setEnabled(true);

    tempRateTable->setColumnWidth(0,40);
    tempRateTable->setColumnWidth(1,60);
    tempRateTable->setColumnWidth(2,60);
    tempRateTable->setColumnWidth(3,60);


    QStringList headerLabels;
    headerLabels.append("Month");
    headerLabels.append("Base(%)");
    headerLabels.append("Up(%)");
    headerLabels.append("Down(%)");
    tempRateTable->setHorizontalHeaderLabels(headerLabels);
    tempRateTable->verticalHeader()->hide();


}




void Sol2ShockWidget::on_tempComboBox_changed(){
    qDebug()<<"Sol2ShockWidget inputComboBox changed called";
    qDebug()<<tempComboBox->currentIndex();

    //qDebug()<<mainPtr_->getMyTree()->getItemVector().at(inputComboBox->currentIndex())->text();
    qDebug()<<mainPtr_->getMyTree()->getInputItemVector().size()<<"changedSize";
    if(tempComboBox->currentIndex()!=-1){
        drawCurve();
    }
    if(tempComboBox->currentIndex()==-1){
        voidSetting();

    }


}

void Sol2ShockWidget::update(){
    qDebug()<<"Sol2ShockWidget slot update called";

    tempComboBox->clear();


    for(int i=0;i<mainPtr_->smithWilsonWidget->temporaryCurveItemVector.size();i++){
        tempComboBox->addItem(mainPtr_->smithWilsonWidget->temporaryCurveItemVector.at(i)->text());
    }

    tempComboBox->setCurrentIndex(mainPtr_->smithWilsonWidget->temporaryCurveItemVector.size()-1);

}
void Sol2ShockWidget::voidSetting(){

        tempRateTable->setEnabled(false);

}
