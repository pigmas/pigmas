#include "rwmodel.h"
#include <optimization.h>
#include <gsl/gsl_sf_bessel.h>
#include <QDebug>

using namespace alglib;

class CIR : public RwModel{
public:

    CIR();
    virtual std::vector<double> calibrate(QuantLib::Matrix indexData);
    virtual std::vector<double> calibrate(){ return this->calibrate(this->data_); }
    virtual QuantLib::Matrix scenario(QuantLib::Matrix& rnd);
	double MLE(QuantLib::Matrix rateData);
    double MLE(){ return this->MLE(this->data_); }



private:




};

