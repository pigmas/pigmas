#include <QtGui>
#include <QFileDialog>
#include <QMessageBox>
#include <QMenuBar>
#include <QtWidgets>
#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QSizePolicy>
#include "spreadsheet.h"
#include "cell.h"


Spreadsheet::Spreadsheet(QWidget *parent)
    :QTableWidget(parent)
{
    autoRecalc = true;
    //setItemPrototype(new Cell);
    //setSelectionMode(QAbstractItemView::ContiguousSelection);

    //clear();
    signalSetting();


}
Spreadsheet::Spreadsheet(MainWindow* mainPtr)
{
    mainPtr_=mainPtr;
    autoRecalc = true;
    signalSetting();

}
void Spreadsheet::signalSetting(){
    connect(this,SIGNAL(itemChanged(QTableWidgetItem*)),this, SLOT(somethingChanged()));
    connect(this,SIGNAL(modified()),mainPtr_,SLOT(updateWidgets()));
}

void Spreadsheet::clear()
{
    //setRowCount(0);
    //setColumnCount(0);

    //setRowCount(RowCount);
    //setColumnCount(ColumnCount);
    for (int i = 0; i < ColumnCount; i++) {
        //QTableWidgetItem *item = new QTableWidgetItem;
        //item->setText(QString(QChar('A' + i)));
        //setHorizontalHeaderItem(i, item);
    }
    setCurrentCell(0, 0);
}

Cell *Spreadsheet::cell(int row, int column) const
{
    return static_cast<Cell *>(item(row, column));//type change item*->cell*
}

QString Spreadsheet::text(int row, int column) const
{
    Cell *c = cell(row, column);
    if (c) {
        return c->text();
    } else {
        return "";
    }
}

QString Spreadsheet::formula(int row, int column) const
{
    Cell *c = cell(row, column);
    if (c) {
        return c->formula();
    } else {
        return "";
    }
}

void Spreadsheet::setFormula(int row, int column, const QString &formula)
{
    Cell *c = cell(row, column);
    if (!c) {
        c = new Cell;
        setItem(row, column, c);
    }
    c->setFormula(formula);
}

QString Spreadsheet::currentLocation() const
{
    return QChar('A' + currentColumn()) +
            QString::number(currentRow() + 1);
}

QString Spreadsheet::currentFormula() const
{
    return formula(currentRow(), currentColumn());
}

void Spreadsheet::somethingChanged()
{
    qDebug()<<"Spreadsheet Something Changed called";
    if (autoRecalc) {
        recalculate();
    }
    emit modified();
}

bool Spreadsheet::writeFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::warning(this, tr("Spreadsheet"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(file.fileName())
                             .arg(file.errorString()));
        return false;
    }

    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_4_6);

    out << quint32(MagicNumber); // safe int size

    QApplication::setOverrideCursor(Qt::WaitCursor);

    for (int row = 0; row < RowCount; ++row) {
        for (int col = 0; col < ColumnCount; ++col) {
            QString str  = formula(row, col);
            if (!str.isEmpty()) {
                out << quint16(row) << quint16(col) << str;
            }
        }
    }

    QApplication::restoreOverrideCursor();
    return true;
}

bool Spreadsheet::readFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::warning(this, tr("Spreadsheet"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(file.fileName())
                             .arg(file.errorString()));
        return false;
    }

    QDataStream in(&file);
    in.setVersion(QDataStream::Qt_4_6);

    quint32 magic;
    in >> magic;
    if (magic != MagicNumber) {
        QMessageBox::warning(this, tr("Spreadsheet"),
                             tr("The file is not a spreadsheet file."));
        return false;
    }

    clear();
    quint16 row, col;
    QString str;

    while(!in.atEnd()) {
        in >> row >> col >> str;
    }
    setFormula(row, col, str);
    return true;
}

bool Spreadsheet::readMatrix(Matrix* m)
{

    setRowCount(m->size1());
    setColumnCount(m->size2());

    blockSignals(1);
    for(int i=0;i<m->size1();i++){
        for(int j=0;j<m->size2();j++){

            Cell* c = new Cell;
            setItem(i,j,c);

            c->setDataPtr(&(*m)[i][j]);

        }
    }
    blockSignals(0);

    return true;
}



bool Spreadsheet::updateMatrix(Matrix* m)
{

    blockSignals(1);
    for(int i=0;i<m->size1();i++){
        for(int j=0;j<m->size2();j++){

            Cell* c=(Cell*)(this->item(i,j));
            c->setDataPtr(&(*m)[i][j]);//"this->childAt(i,j)";
            //c->setDataPtr(&(*m)[i][j]);

        }
    }
    blockSignals(0);
    return true;
}

void Spreadsheet::cut()
{
    copy();
    del();
}

void Spreadsheet::copy()
{
    QTableWidgetSelectionRange range = selectedRange();
    QString str;
    for (int i = 0; i < range.rowCount(); ++i) {
        if (i > 0 ) {
            str += "\n";
        }
        for (int j = 0; j < range.columnCount(); ++j) {
            str += formula(range.topRow() + i, range.leftColumn() + j);
        }
    }
    QApplication::clipboard()->setText(str);
}

QTableWidgetSelectionRange Spreadsheet::selectedRange() const
{
    QList<QTableWidgetSelectionRange> ranges = this->selectedRanges();
    if (ranges.isEmpty()) {
        // ContinuousSelection automatically selects the current cell
        // if there is no current cell, a default range is selected;
        return QTableWidgetSelectionRange();
    } else {
        return ranges.first();
    }
}

void Spreadsheet::paste()
{
    QTableWidgetSelectionRange range = selectedRange();
    QString str = QApplication::clipboard()->text();
    QStringList rows = str.split('\n');
    int numRows = rows.count();
    int numColumn = rows.first().count('\t') + 1;

    if (range.rowCount() * range.columnCount() != 1
            && (range.rowCount() != numRows || range.columnCount() != numColumn)) {
        QMessageBox::information(this, tr("Spreadsheet"),
                             tr("The information cannot be pasted"
                                "because the copy and paste area are not the same size."));
        return;
    }

    for (int i = 0; i < numRows; ++i) {
        QStringList columns = rows[i].split('\t');
        for (int j = 0; j < numColumn; ++j) {
            int row = range.topRow() + i;
            int column = range.leftColumn() + j;
            setFormula(row, column, columns[j]);
        }
    }
    somethingChanged();
}

void Spreadsheet::del()
{
    QList<QTableWidgetItem*> items = selectedItems();
    if (!items.isEmpty()) {
        foreach (QTableWidgetItem *item, items) {
            delete item;
        }
        somethingChanged();
    }
}

void Spreadsheet::selectCurrentRow()
{
    selectRow(currentRow());
}

void Spreadsheet::selectCurrentColumn()
{
    selectColumn(currentColumn());
}

void Spreadsheet::findNext(const QString &str, Qt::CaseSensitivity cs)
{
    int row = currentRow();
    int column = currentColumn() + 1;
    while(row < RowCount) {
        while(column < ColumnCount) {
            if (text(row, column).contains(str, cs)) {
                clearSelection();
                setCurrentCell(row, column);
                activateWindow();
                return;
            }
            column++;
        }
        column = 0;
        row++;
    }

    QApplication::beep();
}

void Spreadsheet::findPrevious(const QString &str, Qt::CaseSensitivity cs)
{
    int row = currentRow();
    int column = currentColumn() -1;
    while(row >= 0) {
        while(column >= 0) {
            if (text(row, column).contains(str, cs)) {
                clearSelection();
                setCurrentCell(row, column);
                activateWindow();
                return;
            }
            column--;
        }
        column = ColumnCount - 1;
        row--;
    }

    QApplication::beep();
}


void Spreadsheet::recalculate()
{

    for (int i=0;i<this->rowCount();i++){
        for (int j=0;j<this->columnCount();j++) {
            this->cell(i,j)->setDirty();
            this->cell(i,j)->updatePtr();
        }
    }
    //qDebug()<<"recalculate";
    //viewport()->update();
}

void Spreadsheet::setAutoRecalculate(bool recalc)
{
    autoRecalc = recalc;
    if (autoRecalc) {
        recalculate();
    }
}

bool SpreadsheetCompare::operator()(const QStringList &row1, const QStringList &row2) const
{
    for (int i = 0; i < KeyCount; ++i) {
        int column = keys[i];
        if (column != -1) {
            if (row1[column] != row2[column]) {
                if (ascending[i]) {
                    return row1[column] < row2[column];
                } else {
                    return row1[column] > row2[column];
                }
            }
        }
    }
    return false;
}

void Spreadsheet::sort(const SpreadsheetCompare &compare)
{
    QList<QStringList> rows;
    QTableWidgetSelectionRange range = selectedRange();

    for (int i = 0; i < range.rowCount(); ++i) {
        QStringList row;
        for (int j = 0; j < range.columnCount(); ++j) {
            row.append(formula(range.topRow() + i, range.leftColumn() + j));
        }
        rows.append(row);
    }

    qStableSort(rows.begin(), rows.end(), compare);

    for (int i = 0; i < range.rowCount(); ++i) {
        for (int j = 0; j < range.columnCount(); ++j) {
            setFormula(range.topRow() + i, range.leftColumn() + j, rows[i][j]);
        }
    }
    clearSelection();
    somethingChanged();
}

void Spreadsheet::setRateTable(){
     //header->setSectionResizeMode(QHeaderView::Stretch);
     //header->setStretchLastSection(true);
     //disconnect(this,SIGNAL(modified()),mainPtr_,SLOT(updateWidgets()));
     this->setColumnWidth(0,90);
     this->horizontalHeader()->setStretchLastSection(true);
     QStringList headerLabels;
     headerLabels.append("Maturity(Y)");
     headerLabels.append("Rate(%)");
     this->setHorizontalHeaderLabels(headerLabels);
}

void Spreadsheet::setParamsTable(){
    this->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->verticalHeader()->hide();
    this->setFixedHeight(55);
    //this->setFixedWidth(280);
    this->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
}

void Spreadsheet::setScenParamsTable(){
    this->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    this->verticalHeader()->hide();
    disconnect(this,SIGNAL(modified()),mainPtr_,SLOT(updateWidgets()));
    connect(this,SIGNAL(modified()),mainPtr_,SLOT(updateScenParam()));
    this->setFixedHeight(55);
    //this->setFixedWidth(280);
    this->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
}

void Spreadsheet::setVolTable(){
    this->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    //this->verticalHeader()->hide();
    //disconnect(this,SIGNAL(modified()),mainPtr_,SLOT(updateWidgets()));
    //connect(this,SIGNAL(modified()),mainPtr_,SLOT(updateScenParam()));
    //this->setFixedHeight(50);
    //this->setFixedWidth(280);
    //this->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
    this->setFixedHeight(205);
    setRowCount(6);
    setColumnCount(6);
}

void Spreadsheet::createYieldTable(vector<Matrix>& yieldVector, vector<Date>& dateVector, Matrix& yieldDbTenor){
    setRowCount(yieldVector.size());
    setColumnCount(yieldDbTenor.size2());

    for(int i=1;i<dateVector.size();i++){
        Cell* c = new Cell;
        setItem(i,0,c);
        std::ostringstream out;
        out<<QuantLib::io::iso_date(mainPtr_->dataManager->dateVector.at(i-1));

        c->setText(QString::fromStdString(out.str()));
    }

}
void Spreadsheet::createVolTable(vector<Matrix> volVector, vector<Date> dateVector){

}




















