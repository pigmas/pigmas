#ifndef GENERALHULLWHITE_H
#define GENERALHULLWHITE_H

#include "ql/models/shortrate/onefactormodels/hullwhite.hpp"
#include <vector>
using namespace QuantLib;

class GeneralHullWhite : public OneFactorAffineModel, public TermStructureConsistentModel
{

public:
    GeneralHullWhite(const Handle<QuantLib::YieldTermStructure>& termStructure);

    Real a(Time t) {return a_(t);}
    Real sigma(Time t) {return sigma_(t);}
    Real theta(Time t) {return theta_[0][int(std::round(t*12.0))];}
    Real sigma_p(Time t, Time T) const;
    Real A(Time t, Time T) const;
    Real B(Time t, Time T) const;
    Matrix scenario(Matrix& rnd);
    boost::shared_ptr<ShortRateDynamics> dynamics() const;
    void setTheta();

    Real discountBondOption(Option::Type type,
                            Real strike,
                            Time maturity,
                            Time bondMaturity) const;
protected:
    void generateArguments();

private:
    std::vector<Time> time_a{10,20};
    std::vector<Time> time_sigma{1,2,3,5,7,10};
    //PiecewiseConstantParameter a_{time_a};
    //PiecewiseConstantParameter sigma_{time_sigma};
    Parameter& a_;
    Parameter& sigma_;
    Matrix theta_;

};

inline boost::shared_ptr<OneFactorModel::ShortRateDynamics>
GeneralHullWhite::dynamics() const {
//        return boost::shared_ptr<ShortRateDynamics>(
//                                            new Dynamics(phi_, a(), sigma()));
    return nullptr;
}

#endif // GENERALHULLWHITE_H
