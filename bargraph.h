#ifndef BARGRAPH_H
#define BARGRAPH_H

#include <QtDataVisualization/q3dbars.h>
#include <QtDataVisualization/qbardataproxy.h>
#include <QtDataVisualization/qabstract3dseries.h>

#include <QtGui/QFont>
#include <QtCore/QDebug>
#include <QtCore/QStringList>
#include <QtCore/QPointer>
#include <QtCore/QPropertyAnimation>

#include <ql/math/matrix.hpp>

using namespace QtDataVisualization;
using namespace QuantLib;

class BarGraph : public QObject
{
    Q_OBJECT

public:
    explicit BarGraph(Q3DBars *bargraph);
    ~BarGraph();
    void setData(Matrix* m);
    void setRelativeGraph();

private:
    Q3DBars *m_graph;
    QBar3DSeries *m_primarySeries;
    QStringList m_optionMaturity;
    QStringList m_swapMaturity;

    QValue3DAxis* m_valueAxis = new QValue3DAxis;
    QCategory3DAxis* m_optionAxis = new QCategory3DAxis;
    QCategory3DAxis* m_swapAxis = new QCategory3DAxis;

};

#endif // BARGRAPH_H
