#ifndef SMITHWILSONWIDGET_H
#define SMITHWILSONWIDGET_H

#include <QHBoxLayout>
#include <QTableWidget>
#include <QListWidget>
#include <QComboBox>
#include <QSpinBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChartView>
#include <QWidget>
#include <ql/math/matrix.hpp>

#include "yieldtermstructure.h"
#include "spreadsheet.h"
#include "mytree.h"
#include "mainwindow.h"

QT_CHARTS_USE_NAMESPACE
class MainWindow;
class Spreadsheet;

//class MyTree;

using namespace QuantLib;
class SmithWilsonWidget : public QWidget
{
    Q_OBJECT

public:
    SmithWilsonWidget(QWidget *parent = 0);
    SmithWilsonWidget(MainWindow* mainPtr_);
    ~SmithWilsonWidget();
    //layout
    QHBoxLayout* mainLayout = new QHBoxLayout;
    QVBoxLayout* inputLayout = new QVBoxLayout;
    QVBoxLayout* curveLayout = new QVBoxLayout;
    QVBoxLayout* curveOptionLayout = new QVBoxLayout;
    QVBoxLayout* chartViewLayout = new QVBoxLayout;
    QHBoxLayout* inputLabelLayout = new QHBoxLayout;

    QHBoxLayout* typeLayout = new QHBoxLayout;

    QWidget* curveOptionContainer = new QWidget;
    QLabel *inputLabel = new QLabel(tr("Input Rate:"));
    QComboBox* inputComboBox = new QComboBox;

    Spreadsheet* inputRateTable;

    QLabel* ufrLabel = new QLabel(tr("UFR(Annually Compound, %):"));
    QLabel* alphaLabel = new QLabel(tr("Alpha:"));
    QLabel* spreadLabel = new QLabel(tr("Shock(Parallel Shift, bp):"));
    QLabel* llpLabel = new QLabel(tr("LLP:"));
    QLabel* convergenceLabel = new QLabel(tr("Convergence:"));
    QLabel* typeLabel = new QLabel(tr("Curve Type:"));

    QLineEdit* ufrEdit = new QLineEdit();
    QLineEdit* alphaEdit = new QLineEdit();
    QLineEdit* spreadEdit = new QLineEdit();
    QLineEdit* llpEdit = new QLineEdit();
    QLineEdit* convergenceEdit = new QLineEdit();
    QPushButton* generatePush = new QPushButton(tr("Generate"));
    QPushButton* savePush = new QPushButton(tr("Save"));
    QComboBox* typeComboBox = new QComboBox;

    QListWidget* curveList = new QListWidget;

    QChartView* chartView = new QChartView();
    boost::shared_ptr<MY::YieldTermStructure> currentYieldTermStructure;
    vector<boost::shared_ptr<MY::YieldTermStructure>> temporaryCurveVector;
    vector<QListWidgetItem*> temporaryCurveItemVector;


    void setData(Matrix* mPtr){dataMat.push_back(mPtr);}
    void setMainPtr(MainWindow* mainPtr){mainPtr_=mainPtr;}
    void init();
    void layoutSetting();
    void signalSetting();
    void updateInputComboBox();
    void update();
    void voidSetting();
    void addTempCurve();
    void addToProject();
    void delTempCurve();
    void drawCurve();
    void addTempCurve_oneClick();
private:
    std::vector<Matrix*> dataMat;
    MainWindow* mainPtr_;

private slots:
        void on_generatePush_clicked();
        void on_savePush_clicked();
        void on_inputComboBox_changed();
        void on_inputComboBox_highlighted();
        void on_curveList_changed();
        void on_typeComboBox_changed();


signals:
    void updated();
};

#endif // SMITHWILSONWIDGET_H
