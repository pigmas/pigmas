#include "generalhullwhite.h"
#include "ql/pricingengines/blackformula.hpp"
#include <QDebug>
#define myqDebug() qDebug()<<qSetRealNumberPrecision(20)

GeneralHullWhite::GeneralHullWhite(const Handle<YieldTermStructure>& termStructure)
    : OneFactorAffineModel(2),
      TermStructureConsistentModel(termStructure),
      a_(arguments_[0]),sigma_(arguments_[1]){

    //TermStructure Note
    //termStructure()-> : defined in TermStructureConsistentModel
    //termStructure_ : Member variable of TermStructureConsistentModel
    //termStructure()->discount(t) -> QuantLib::YieldTermstructure discount(t) -> MY::YieldTermstructure discountImpl(t)

    //a_ <-> argument
    //qDebug()<<"Initializer";
    //a_.setParam(0,0.01);
    //a_.setParam(1,0.02);
    //qDebug()<<a_(15);
    //a_.setParam(3,0.05);
    //qDebug()<<a_.size();
    a_=PiecewiseConstantParameter(time_a);
    sigma_=PiecewiseConstantParameter(time_sigma);
    a_.setParam(0,0.01);
    a_.setParam(1,0.01);
    a_.setParam(2,0.03);
    sigma_.setParam(0,0.007);
    sigma_.setParam(1,0.006);
    sigma_.setParam(2,0.005);
    sigma_.setParam(3,0.004);
    sigma_.setParam(4,0.003);
    sigma_.setParam(5,0.002);
    sigma_.setParam(6,0.001);

    generateArguments();
    //setParams(params);
}

Real GeneralHullWhite::sigma_p(Time t, Time T) const {
    Real temp{0};
    Real a_0=a_.params()[0];
    for(int i=0;i<sigma_.params().size()-1.;i++){
        if(time_sigma[i]<=t){
            if(i==0){
                temp=temp+sigma_.params()[i]*sigma_.params()[i]*(std::exp(2.0*a_0*time_sigma[i])-1);
            }else{
                temp=temp+sigma_.params()[i]*sigma_.params()[i]*(std::exp(2.0*a_0*time_sigma[i])-std::exp(2.0*a_0*time_sigma[i-1]));
            };
        }else{
            if(i==0){
                temp=temp+sigma_.params()[i]*sigma_.params()[i]*(std::exp(2.0*a_0*t)-1);
            }else{
                temp=temp+sigma_.params()[i]*sigma_.params()[i]*(std::exp(2.0*a_0*t)-std::exp(2.0*a_0*time_sigma[i-1]));
            };
            break;
        };
    };
    return std::sqrt((temp*std::exp(-2.0*a_0*t)/2/a_0))*B(t,T);
}

Real GeneralHullWhite::A(Time t, Time T) const {

    DiscountFactor discount1 = termStructure()->discount(t);
    DiscountFactor discount2 = termStructure()->discount(T);
    Rate forward = termStructure()->forwardRate(t, t+1.0/12.0, Continuous, NoFrequency);
    Real sigma = sigma_p(t,T);
    Real value = B(t,T)*forward - 0.5*sigma*sigma;
    return std::exp(value)*discount2/discount1;

}

Real GeneralHullWhite::B(Time t, Time T) const {


    //qDebug()<<"GHW B Called";

    if(T<=time_a[0]){
        return (1.0 - std::exp(-a_.params()[0]*(T - t)))/a_.params()[0];
    }else{
        return (1.0 - std::exp(-a_.params()[0]*(10.0-t)))/a_.params()[0]+std::exp(-a_.params()[0]*(10.0-t))*(1-std::exp(-a_.params()[1]*(T-10.0)))/a_.params()[1];
    }

}

Real GeneralHullWhite::discountBondOption(Option::Type type, Real strike,
                                   Time maturity,
                                   Time bondMaturity) const {

    Real v=sigma_p(maturity,bondMaturity);
    Real f = termStructure()->discount(bondMaturity);
    Real k = termStructure()->discount(maturity)*strike;
    return 10.0*blackFormula(type, k, f, v);


}

void GeneralHullWhite::setTheta(){
    //qDebug()<<"setTheta";
    //qDebug()<<sigma(6)<<"6"<<"0.003";
    //qDebug()<<sigma(5.1)<<"5.1"<<"0.003";
    //qDebug()<<sigma(5)<<"5"<<"0.003";
    //qDebug()<<sigma(4)<<"4"<<"0.004";
    //qDebug()<<termStructure()->maxTime();
    theta_=Matrix(1,termStructure()->maxTime());
    //qDebug()<<theta_.size1();//1
    //qDebug()<<theta_.size2();//1440
    //Rate forward1 = termStructure()->forwardRate(t-dt, t, Continuous, NoFrequency);
    //Rate forward2 = termStructure()->forwardRate(t, t+dt, Continuous, NoFrequency);
    double dt=1.0/12.0;
    theta_[0][0]=termStructure()->forwardRate(0,1.0/12.0, Continuous, NoFrequency);
    for(int i=0;i<theta_.size2()-1;i++){
        double t=(double(i)+1.0)/12.0;
        Rate forward1 = termStructure()->forwardRate(t-dt, t, Continuous, NoFrequency);
        Rate forward2 = termStructure()->forwardRate(t, t+dt, Continuous, NoFrequency);
        double theta_front,theta_back;
        //qDebug()<<t<<"a"<<a(t)<<"sigma"<<sigma(t)<<"theta"<<theta(t);
        //front
        theta_front=forward1+(forward2-forward1)/dt/a(t);

        //back

        double temp_1=0.0;
        double temp_2=0.0;

        for(int j=0;j<=i;j++){
            double u=(double(j)+1.0)/12.0;
            if(u<20){
                temp_1+=pow(sigma(u),2)*(exp(2.0*a(u)*u)-exp(2.0*a(u)*(u-dt)))/2.0/a(u);
            }else{
                temp_2+=pow(sigma(u),2)*(exp(2.0*a(u)*u)-exp(2.0*a(u)*(u-dt)))/2.0/a(u);
            }
        }

        if(t<20){
            theta_back=temp_1*exp(-2.0*a(t)*t)/a(t);
        }else{
            theta_back=(temp_1*exp(-2.0*a(1)*20.0)*exp(-2*a(t)*(t-20))+temp_2*exp(-2.0*a(t)*t))/a(t);//a(1)=a(0~20);
        }

        theta_[0][i+1]=theta_front+theta_back;
        //qDebug()<<t<<"a"<<a(t)<<"sigma"<<sigma(t)<<"theta"<<theta(t-1.0/12.0)<<"theta_front"<<theta_front<<"theta_back"<<theta_back;
    }

    //9.91667 a 0.01 sigma 0.002
    //10 a 0.05 sigma 0.5

    //19.9167 a 0.05 sigma 0.5
    //20 a 0.3 sigma 0.5
    //termStructure()->discount(3);

}

Matrix GeneralHullWhite::scenario(Matrix& rnd){
    double dt=1.0/12.0;
    int numScen = rnd.size1();
    int numTerm = rnd.size2();
    Matrix scen(numScen,numTerm);

    for(int i=0;i<numScen;i++){
        scen[i][0]=termStructure()->forwardRate(0.0,1.0/12.0, Continuous, NoFrequency);
    }

    for(int i=0;i<numScen;i++){
        for(int j=0;j<numTerm-1;j++){
            double t=(double(j)+1.0)/12.0;
            scen[i][j+1]=scen[i][j]+a(t)*(theta(t)-scen[i][j])*dt+sigma(t)*rnd[i][j]*sqrt((1-exp(-2*a(t)*dt))/2/a(t));
            //qDebug()<<t<<scen[i][j]<<a(t)<<theta(t)<<scen[i][j]<<dt<<sigma(t)<<rnd[i][j]<<sqrt((1-exp(-2*a(t)*dt))/2/a(t));
        }
    }
    return scen;
}

void GeneralHullWhite::generateArguments(){
    //qDebug()<<"ga called";
    setTheta();
}

