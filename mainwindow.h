#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStackedWidget>
#include <QPushButton>
#include <QtWidgets>
#include "datamanager.h"
#include "smithwilsonwidget.h"
#include "calibrationwidget.h"
#include "fancytabbar.h"
#include "rnwidget.h"
#include "scenariowidget.h"
#include "compareWidget.h"
#include "sliceWidget.h"
#include "oneclickwidget.h"
#include "databasewidget.h"
#include "martingaleWidget.h"
#include "sol2shockwidget.h"
#include "loginwidget.h"

class QAction;
class QActionGroup;
class QLabel;
class QMenu;
class MyTree;
class SmithWilsonWidget;
class FancyTabBar;
class QPushButton;
class CalibrationWidget;
class RnWidget;
class ScenarioWidget;
class CompareWidget;
class SliceWidget;
class OneClickWidget;
class DatabaseWidget;
class MartingaleWidget;
class Sol2ShockWidget;
class LoginWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    MyTree* getMyTree(){return myTree;}
    void init();
    void test();
    void test2();

    void signalSetting();
    bool eventFilter(QObject *target, QEvent *event);
    QTextEdit *logText;

    MyTree *myTree;
    DataManager *dataManager;
    QStackedWidget *mainStacked;
    DatabaseWidget *databaseWidget;
    OneClickWidget *oneClickWidget;
    SmithWilsonWidget *smithWilsonWidget;
    Sol2ShockWidget *sol2ShockWidget;
    CalibrationWidget *calibrationWidget;
    CompareWidget *compareWidget;
    SliceWidget * sliceWidget;
    RnWidget* rnWidget;
    ScenarioWidget* scenarioWidget;
    MartingaleWidget* martingaleWidget;
    FancyTabBar *tabbar;

    int taskNum=0;

protected:
#ifndef QT_NO_CONTEXTMENU
    void contextMenuEvent(QContextMenuEvent *event) override;
#endif // QT_NO_CONTEXTMENU
signals:
    void updated();

private slots:
    void newFile();
    void open();
    void save();
    void print();
    void closeProject();

    void updateButtonClicked();
    void updateWidgets();
    void updateScenParam();
    //void updateRateTable();
    void deleteItem();
    void smithAddToProject();
    void smithDeleteTempCurve();
    void rnAddToProject();
    void rnDelTemp();
    void scenAddToProject();
    void scenDelTemp();

private:
    void createActions();
    void createMenus();
    QPushButton* updateButton;

    QMenu *fileMenu;
    QAction *newAct;
    QAction *openAct;
    QAction *saveAct;
    QAction *printAct;
    QAction *exitAct;
    QLabel *infoLabel;

    QMenu* myTreeMenu;
    QMenu* swTermMenu;
    QMenu* rnListMenu;
    QMenu* scenListMenu;

    //Data
    std::vector<Matrix> itemVector;
    std::vector<Matrix> matrixVector;
};

#endif
