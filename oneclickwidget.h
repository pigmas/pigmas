#ifndef ONECLICKWIDGET_H
#define ONECLICKWIDGET_H

#include <QWidget>
#include "mainwindow.h"
#include "spreadsheet.h"
#include "surfacegraph.h"
#include "bargraph.h"
#include "ql/math/matrix.hpp"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QComboBox>
#include <QGridLayout>
#include <QProgressBar>
#include <QPlainTextEdit>
#include <QSplitter>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChartView>

#include<QtDataVisualization/Q3DSurface>
#include<QtDataVisualization/QSurface3DSeries>
#include<QtDataVisualization/Q3DBars>

class MainWindow;
class Spreadsheet;
class QtCharts::QChartView;
class MyThread;
class Worker;

using namespace QuantLib;
using namespace QtDataVisualization;

class OneClickWidget : public QWidget
{
    Q_OBJECT

public:
    OneClickWidget(QWidget *parent = 0);
    OneClickWidget(MainWindow* mainPtr = 0);

    QWidget* optionContainer = new QWidget;
    QComboBox* dateComboBox = new QComboBox;
    QPushButton* generatePush = new QPushButton(tr("Generate"));
    QPushButton* savePush = new QPushButton(tr("Save"));
    QProgressBar* progressBar = new QProgressBar;
    QPlainTextEdit* information = new QPlainTextEdit;

    QtCharts::QChartView* smithWilsonView = new QtCharts::QChartView();
    QtCharts::QChartView* scenarioView = new QtCharts::QChartView();
    QtCharts::QChartView* martingaleView = new QtCharts::QChartView();

    Q3DBars *relativeGraph = new Q3DBars();
    BarGraph *relativeModifier = new BarGraph(relativeGraph);
    QWidget *relativeContainer = QWidget::createWindowContainer(relativeGraph);
    QVBoxLayout* relativeContainerLayout = new QVBoxLayout;
    QLabel* relativeLabel = new QLabel("Relative Swaption Price Error");

    QtCharts::QChartView* voidView = new QtCharts::QChartView();
    QLabel* dateLabel = new QLabel(tr("Date:"));
    QLabel* chartOneLabel = new QLabel("Smith-Wilson Spot Termstructure");
    QLabel* chartTwoLabel = new QLabel("Forward Rate Scenarios");
    QLabel* chartThreeLabel = new QLabel("Relative Swaption Price Error : (Market-Model)/Market");
    QLabel* chartFourLabel = new QLabel("Martingale 1=1 Test 95% Confidence Interval");

    QWidget* tableContainer = new QWidget;

    Spreadsheet* rateTable;
    Spreadsheet* marketVolTable;
    Spreadsheet* paramsTable1;
    Spreadsheet* paramsTable2;

    QHBoxLayout* mainLayout = new QHBoxLayout;
    QVBoxLayout* optionLayout = new QVBoxLayout;
    QGridLayout* chartLayout = new QGridLayout;

    void init();
    void layoutSetting();
    void signalSetting();
    void chartUpdate();
    void dateComboUpdate();
    void oneClickFunction();
    void drawSmithWilsonChart();
    void drawRelativeBar();
    void scenChartShow();


    QString currentTempCurveName;
    QString currentTempModelName;
    QString currentTempRnName;
    QString currentTempScenName;
    QThread* thread = new QThread;
    Worker* worker;

private:
    MainWindow* mainPtr_;
    vector<Matrix> tempRnVector_;

private slots:
    void on_optionCombo_changed();
    void on_swapCombo_changed();
    void on_generatePush_clicked();
    void afterWorkerFinished();
    void on_savePush_clicked();

signals:
    void updated();


};



#endif
