#include "CIR.h"
#include <iostream>
#include <qDebug>
#include <boost/math/distributions/non_central_chi_squared.hpp>
#include <boost/math/distributions/normal.hpp>
using namespace std;

CIR::CIR(){

    modelIndex = 1;
    params_={0.03,0.04,0.05};
    paramsName_={"Alphe","Theta","Sigma"};

}

void cirCalibrationHelper(const real_1d_array &x, double &func, void* ptr){
    CIR* modelPtr = static_cast<CIR*>(ptr);
    std::vector<double> params = { x[0], x[1], x[2] };
    modelPtr->setParams(params);
    func = - modelPtr->MLE();
}

std::vector<double> CIR::calibrate(QuantLib::Matrix indexData){
    qDebug()<<" CIR::calibrate Called";
    this->setData(indexData);

    real_1d_array paramsArray;
    paramsArray.setcontent(params_.size(),&(params_[0]));

    real_1d_array bndl = "[-1000,-1000,0.0001]";
    real_1d_array bndu = "[1000,1000,1000]";
    minbcstate state;
    minbcreport rep;

    double epsg = 1.0e-16;
    double epsf = 0;
    double epsx = 0;
    ae_int_t maxits = 0;
    double diffstep = 1.0e-6;

    minbccreatef(paramsArray, diffstep, state);
    minbcsetbc(state, bndl, bndu);
    minbcsetcond(state, epsg, epsf, epsx, maxits);

    alglib::minbcoptimize(state, cirCalibrationHelper,0,this);
    minbcresults(state, paramsArray, rep);

    return params_;
}

double CIR::MLE(QuantLib::Matrix rateData){

	double alpha = this->params_[0]; double theta = this->params_[1]; double sigma = this->params_[2];
	double dt = 1.0 / 12.0;
	
	double c = (2 * alpha) / (pow(sigma,2)*(1 - exp(-alpha *dt)));
	double q = (2 * alpha * theta) / pow(sigma,2) - 1;
	
	int size = rateData.rows();

	QuantLib::Matrix u(size - 1, 1);
	QuantLib::Matrix v(size - 1, 1);


	double sum = 0;
	double divide = 0;
	double product = 0;
	double temp = 0;
	double tempSum = 0;
	double z = 0;

	for (int i = 0; i < size - 1; i++){
		u[i][0] = c * exp(-alpha *dt)* rateData[i][0];
		v[i][0] = c * rateData[i + 1][0];
		sum = u[i][0] + v[i][0];
		divide = v[i][0] / u[i][0];
		product = u[i][0] * v[i][0];
		z = 2 * sqrt(product);
        temp = -sum + log(divide)*q / 2 + log(gsl_sf_bessel_Inu_scaled(abs(q), z))+abs(real(z));
		tempSum = tempSum + temp;
	}
	
	return (size - 1)* log(c) + tempSum;

}

QuantLib::Matrix CIR::scenario(QuantLib::Matrix& rnd){
    qDebug()<<" CIR::scenario Called";

    double dt = 1.0/12.0;
    double r0 = 0.03;
    double a = params_[0];
    double theta = params_[1];
    double sigma = params_[2];

    int numScen = rnd.size1();
    int numTerm = rnd.size2();
    QuantLib::Matrix scen(numScen,numTerm);

    double c= sigma*sigma*(1-exp(-a*dt))/4/a;
    double df= 4*theta*a/sigma/sigma;

    for(int i=0;i<numScen;i++){
        scen[i][0]=r0;
    }

    for(int i=0;i<numScen;i++){
        for(int j=0;j<numTerm-1;j++){
            boost::math::normal nd(0,1);
            double runif = boost::math::cdf(nd,rnd[i][j]);
            double ncp = scen[i][j]*exp(-a*dt)/c;
            boost::math::non_central_chi_squared ncd(df,ncp);
            double ncrnd=boost::math::quantile(ncd,runif);
            scen[i][j+1]=c*ncrnd;
            //qDebug()<<" rnd[i][j] : "<<rnd[i][j]<<" runif : "<<runif<<" df : "<<df<<" ncp : "<<ncp<<" ncrnd"<< ncrnd;
        }
    }

    return scen;
}

