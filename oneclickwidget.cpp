
#include "OneClickWidget.h"

#include "ql/math/randomnumbers/randomsequencegenerator.hpp"
#include "ql/math/randomnumbers/mt19937uniformrng.hpp"
#include "ql/math/distributions/normaldistribution.hpp"


#include "delegate.h"
#include <QSizePolicy>
#include <QFileDialog>
#include <QInputDialog>
#include <QBarSeries>
#include <QBarSet>
#include <QChart>
#include <QThread>

using namespace QuantLib;
class GeneralHullWhite;
class Worker;

OneClickWidget::OneClickWidget(QWidget *parent)
    : QWidget(parent)
{
    init();
    signalSetting();

}

OneClickWidget::OneClickWidget(MainWindow* mainPtr)
{
    mainPtr_=mainPtr;
    init();
    layoutSetting();
    signalSetting();


}

void OneClickWidget::init(){
    generatePush->setEnabled(false);
    savePush->setEnabled(false);
}

void OneClickWidget::signalSetting(){
    //connect(this->optionCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(on_optionCombo_changed()));
    //connect(this->swapCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(on_swapCombo_changed()));
    connect(generatePush,SIGNAL(clicked(bool)),this,SLOT(on_generatePush_clicked()));
    connect(savePush,SIGNAL(clicked(bool)),this,SLOT(on_savePush_clicked()));

}


void OneClickWidget::layoutSetting(){

    //mainLayout->addWidget(optionContainer);
    //mainLayout->addLayout(chartLayout);

    QSplitter *mainSplitter = new QSplitter;
    mainSplitter->setOrientation(Qt::Horizontal);
    mainSplitter->addWidget(optionContainer);
    QWidget *chartContainer = new QWidget;
    chartContainer->setLayout(chartLayout);
    mainSplitter->addWidget(chartContainer);
    mainLayout->addWidget(mainSplitter);

    optionContainer->setLayout(optionLayout);
    optionLayout->addWidget(dateLabel);
    optionLayout->addWidget(dateComboBox);
    optionLayout->addWidget(generatePush);
    optionLayout->addWidget(savePush);
    optionLayout->addWidget(tableContainer);
    optionLayout->addWidget(progressBar);
    optionLayout->addWidget(information);

    information->appendPlainText("<Default Options>");
    information->appendPlainText("");
    information->appendPlainText("- Yield Curve -");
    information->appendPlainText("Extrapolation Method: Smith-Wilson");
    information->appendPlainText("Input: KAP Spot Rate");
    information->appendPlainText("UFR: 4.2%");
    information->appendPlainText("Liquidity Premium: 0bp");
        information->appendPlainText("");
    information->appendPlainText("- Calibration Option -");
    information->appendPlainText("Model: General Hull&White 1Factor");
    information->appendPlainText("Target: Bloomberg KRW Swaption Vol");
    information->appendPlainText("Optimization Method: Levenberg Marquardt");
        information->appendPlainText("");
    information->appendPlainText("- Random Number -");
    information->appendPlainText("Genration Method: Mersenne Twister 19937");
    information->appendPlainText("Seed: 28749");
    information->appendPlainText("Number of Simulation: 1000");
        information->appendPlainText("");
    information->appendPlainText("- Scenario Generation -");
    information->appendPlainText("Maturity: 1200 month");
    information->appendPlainText("Long Term Parameter: a=0.04, sigma=0.002");
        information->setReadOnly(true);

    progressBar->setTextVisible(false);
    progressBar->setVisible(false);
    optionContainer->setFixedWidth(300);

    optionLayout->setAlignment(Qt::AlignTop);
    chartLayout->addWidget(chartOneLabel,0,0,1,1);
    chartLayout->addWidget(smithWilsonView,1,0,1,1);
    chartLayout->addWidget(chartTwoLabel,2,0,1,1);
    chartLayout->addWidget(scenarioView,3,0,1,1);
    chartLayout->addWidget(chartThreeLabel,0,1,1,1);
    chartLayout->addWidget(voidView,1,1,1,1);
    //chartLayout->addWidget(relativeContainer,1,1,1,1);
    chartLayout->addWidget(chartFourLabel,2,1,1,1);
    chartLayout->addWidget(martingaleView,3,1,1,1);


    this->setLayout(mainLayout);

}

void OneClickWidget::chartUpdate(){

}

void OneClickWidget::on_optionCombo_changed(){
    qDebug()<<"Slice Widget on_optionCombo_changed() called";
    chartUpdate();

}
void OneClickWidget::on_swapCombo_changed(){
    qDebug()<<"Slice Widget on_swapCombo_changed() called";
    chartUpdate();

}
void OneClickWidget::on_generatePush_clicked(){
    qDebug()<<"OneClickWidget on_generatePush_clicked() called";


    progressBar->setRange(0,0);
    progressBar->setVisible(true);
    qApp->processEvents();



    oneClickFunction();

}

void OneClickWidget::afterWorkerFinished(){
    qDebug()<<"OneClickWidget afterWorkerFinished() called";
        emit(mainPtr_->calibrationWidget->addToProjectPush->clicked());
        drawRelativeBar();

        currentTempRnName=dateComboBox->itemText(dateComboBox->currentIndex())+" RN";
        mainPtr_->rnWidget->seedEdit->setText("28749");
        mainPtr_->rnWidget->maturityEdit->setText("1200");
        mainPtr_->rnWidget->nEdit->setText("1000");
        mainPtr_->rnWidget->generate_oneClick();
        mainPtr_->rnWidget->tempList->setCurrentRow(mainPtr_->rnWidget->tempList->count()-1);
        mainPtr_->rnWidget->addToProject();

        currentTempScenName=dateComboBox->itemText(dateComboBox->currentIndex())+" Scenario";
        mainPtr_->scenarioWidget->generate_oneClick();
        mainPtr_->scenarioWidget->tempScenarioList->setCurrentRow(mainPtr_->scenarioWidget->tempScenarioList->count()-1);
        mainPtr_->scenarioWidget->addToProject();

        scenChartShow();
        savePush->setEnabled(true);


    progressBar->setVisible(false);
}

void OneClickWidget::oneClickFunction(){
    Matrix gridRate = mainPtr_->dataManager->yieldDB.at(dateComboBox->currentIndex());
    Matrix tenor = mainPtr_->dataManager->yieldDbTenor;
    Matrix inputTemp=Matrix(tenor.size2(),2);

    for(int i=0;i<tenor.size2();i++){
        inputTemp[i][0]=tenor[0][i];
        inputTemp[i][1]=gridRate[0][i];
    }
    QString inputRateName=dateComboBox->itemText(dateComboBox->currentIndex())+" Rate";
    QString inputVolName=dateComboBox->itemText(dateComboBox->currentIndex())+" Vol";

    mainPtr_->getMyTree()->addInputItem(inputRateName,inputTemp);
    emit(mainPtr_->updated());
    currentTempCurveName=dateComboBox->itemText(dateComboBox->currentIndex())+" Termstructure";

    mainPtr_->smithWilsonWidget->addTempCurve_oneClick();
    mainPtr_->smithWilsonWidget->curveList->setCurrentRow(mainPtr_->smithWilsonWidget->temporaryCurveVector.size()-1);
    mainPtr_->smithWilsonWidget->addToProject();
    drawSmithWilsonChart();

    mainPtr_->getMyTree()->addVolItem(inputVolName,mainPtr_->dataManager->volDB.at(dateComboBox->currentIndex()));

    currentTempModelName=dateComboBox->itemText(dateComboBox->currentIndex())+" HW";
    mainPtr_->calibrationWidget->newModel_oneClick();
    mainPtr_->calibrationWidget->calibrate_oneCliclk();

//    emit(mainPtr_->calibrationWidget->calibratePush->clicked());

}
void OneClickWidget::dateComboUpdate(){
    qDebug()<<"OneClickWidget dateComboUpdate() called";
    int dateSize = mainPtr_->dataManager->dateVector.size();

    for(int i=0;i<dateSize;i++){
        std::ostringstream out;
        out<<QuantLib::io::iso_date(mainPtr_->dataManager->dateVector.at(i));
        dateComboBox->addItem(QString::fromStdString(out.str()));
    }

}

void OneClickWidget::drawSmithWilsonChart(){

    QChart *chart = new QChart();

    QLineSeries* series = new QLineSeries();
    MY::YieldTermStructure rth=mainPtr_->getMyTree()->getYieldItemVector().back()->getTermStructure();

    for(int j=0;j<rth.maxTime();j++){
        double t=double(j)/12.0;
        series->append(j,(exp(rth.zeroRate(t, Continuous))-1.0)*100.0);
    }
    series->setName("Spot Rate(Annually Compound, %)");
    chart->addSeries(series);

    smithWilsonView->setChart(chart);
    chart->createDefaultAxes();
    chart->setTitle("Smith Wilson Extrapolation");
    smithWilsonView->setRenderHint(QPainter::Antialiasing);

}

void OneClickWidget::drawRelativeBar(){

    Matrix* relative = new Matrix(6,6);

    Matrix* marketPrice = mainPtr_->calibrationWidget->currentMarketPricePtr_;
    Matrix* modelPrice = mainPtr_->calibrationWidget->currentModelPricePtr_;

    for(int i=0;i<6;i++){
        for(int j=0;j<6;j++){
            (*relative)[i][j]=((*marketPrice)[i][j]-(*modelPrice)[i][j])/(*marketPrice)[i][j]*100.0;
        }
    }

    relativeModifier->setData(relative);
    relativeModifier->setRelativeGraph();
    //chartLayout->removeWidget(voidView);
    chartLayout->addWidget(relativeContainer,1,1,1,1);

    relativeGraph->setTitle("Swaption Price Relative Errors");


}

void OneClickWidget::scenChartShow(){
    Matrix* scen=mainPtr_->getMyTree()->getScenarioItemVector().back()->getDataMatrixPtr();
    int numRow=scen->size1();
    int numCol=scen->size2();
    //int lowerIndex=int(numRow*0.05)-1;
    //int upperIndex=int(numRow*0.95);


//    Matrix disc=mainPtr_->scenarioWidget->disc;

//    vector<double> lowerScen;
//    vector<double> upperScen;


//    for(int j=0;j<numCol;j++){
//        vector<double> temp;

//        for(int i=0;i<numRow;i++){
//            temp.push_back((*scen)[i][j]);
//        }
//        std::sort(temp.begin(),temp.end());
//        lowerScen.push_back(temp[upperIndex]);
//        upperScen.push_back(temp[lowerIndex]);
//    }

    MY::YieldTermStructure ts=mainPtr_->getMyTree()->getYieldItemVector().back()->getTermStructure();

//    QChart *chart1 = new QChart();

//    QLineSeries* lower = new QLineSeries();
//    for(int j=0;j<numCol;j++){
//        lower->append(j,(exp(lowerScen[j])-1.0)*100.0);
//    }
//    chart1->addSeries(lower);

//    QLineSeries* upper = new QLineSeries();
//    for(int j=0;j<numCol;j++){
//        upper->append(j,(exp(upperScen[j])-1.0)*100.0);
//    }
//    chart1->addSeries(upper);

//    QLineSeries* baseCurve = new QLineSeries();
//        for(int i=0;i<numCol;i++){
//            double t=double(i)/12.0;

//                baseCurve->append(i,(exp(ts.forwardRate(t,t+1.0/12.0, Continuous, NoFrequency))-1.0)*100.0);

//        }
    QChart *chart1 = new QChart();

    for(int i=0;i<numRow;i++){
        QLineSeries* series = new QLineSeries();
        for(int j=0;j<numCol;j++){
            series->append(j,(*scen)[i][j]);
        }
        chart1->addSeries(series);
   }


    scenarioView->setChart(chart1);
    scenarioView->setRenderHint(QPainter::Antialiasing);
    chart1->legend()->hide();
    chart1->createDefaultAxes();
    chart1->setTitle("Scenarios");

    //martingale
    Matrix tempDisc = mainPtr_->scenarioWidget->tempDiscount;
    vector<double> sdVector;
    vector<double> upperVector;
    vector<double> lowerVector;




    double sum = 0.0, mean, standardDeviation = 0.0;

    for(int j = 0; j < numCol; j++){

        for(int i = 0; i < numRow; i++)
        {
            //sum += ts.discount(t)/tempDisc[i][j];
            sum += tempDisc[i][j];
        }

        mean = sum/double(numRow);
        for(int i = 0; i < numRow; ++i)
            standardDeviation += pow(tempDisc[i][j] - mean, 2);

        sdVector.push_back(sqrt(standardDeviation / double(numRow)));
        upperVector.push_back(1.0+sdVector.at(j)*1.96/sqrt(double(numRow))/numRow/5);
        lowerVector.push_back(1.0-sdVector.at(j)*1.96/sqrt(double(numRow))/numRow/5);

    }

    Matrix mid(1,numCol);
    for(int j=0;j<numCol;j++){
        double temp=0.0;
        for(int i=0;i<numRow;i++){
            temp=temp+tempDisc[i][j];
        }
        mid[0][j]=temp/double(numRow);
    }



    QChart *chart2 = new QChart();

    QLineSeries* lowerSeries = new QLineSeries();
    for(int j=0;j<numCol;j++){
        lowerSeries->append(j,lowerVector[j]);
    }
    chart2->addSeries(lowerSeries);

    QLineSeries* upperSeries = new QLineSeries();
    for(int j=0;j<numCol;j++){
        upperSeries->append(j,upperVector[j]);
    }
    chart2->addSeries(upperSeries);

    QLineSeries* middleSeries = new QLineSeries();
    for(int j=0;j<numCol;j++){
        double t=double(j)/12.0;
        middleSeries->append(j,ts.discount(t)/mid[0][j]);
    }
    chart2->addSeries(middleSeries);

    QLineSeries* baseSeries = new QLineSeries();
    for(int j=0;j<numCol;j++){
        baseSeries->append(j,1.0);
    }
    chart2->addSeries(baseSeries);

    martingaleView->setChart(chart2);
    martingaleView->setRenderHint(QPainter::Antialiasing);
    chart2->legend()->hide();
    chart2->createDefaultAxes();
    chart2->setTitle("1 to 1 Test");

}

void OneClickWidget::on_savePush_clicked(){
    if(mainPtr_->getMyTree()->getScenarioItemVector().size()>0){

        QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                            "/home",
                                                            QFileDialog::ShowDirsOnly
                                                            | QFileDialog::DontResolveSymlinks);
        // scenario
        QString fileName=dateComboBox->itemText(dateComboBox->currentIndex())+"_Scen.csv";

        std::ofstream file(QString(dir+"/"+fileName).toStdString());
        Matrix* matrix=mainPtr_->getMyTree()->getScenarioItemVector().back()->getDataMatrixPtr();
        for(unsigned int i = 0; i != matrix->rows(); i++)
        {
         std::string stream;
         for(unsigned int j = 0; j != matrix->columns(); j++)
         {
            stream += (std::to_string((*matrix)[i][j]) + ",");
         }
            file << stream << std::endl;
        }
        // close text file
        file.close();

        // forward rate
        QString fileName2=dateComboBox->itemText(dateComboBox->currentIndex())+"_Forward.csv";

        std::ofstream file2(QString(dir+"/"+fileName2).toStdString());

        MY::YieldTermStructure ts=mainPtr_->getMyTree()->getYieldItemVector().back()->getTermStructure();

        Matrix matrix2=Matrix(1,ts.maxTime());

        for(int i=0;i<ts.maxTime();i++){
            double t=double(i)/12.0;

                matrix2[0][i]=ts.forwardRate(t,t+1.0/12.0, Continuous, NoFrequency);

        }

        for(unsigned int i = 0; i != matrix2.rows(); i++)
        {
         std::string stream;
         for(unsigned int j = 0; j != matrix2.columns(); j++)
         {
            stream += (std::to_string(matrix2[i][j]) + ",");
         }
            file2 << stream << std::endl;
        }
        // close text file
        file2.close();

        QMessageBox msgBox;
        msgBox.setText(QString("File Saved : ")+QString(dir+"/"+fileName2)+", "+QString(dir+"/"+fileName));
        msgBox.exec();
    }

}
