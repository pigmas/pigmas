#ifndef LOGINWIDGET_H
#define LOGINWIDGET_H

#include <QWidget>
#include "mainwindow.h"
#include "realmainwindow.h"

namespace Ui{
    class LoginWidget;
}
class RealMainWindow;
class MainWindow;

class LoginWidget : public QWidget
{
    Q_OBJECT

public:
    MainWindow* mainWindow = 0;
    RealMainWindow* realMain = 0;
    explicit LoginWidget(QWidget *parent = 0);
    Ui::LoginWidget *ui;
    ~LoginWidget();


public slots:
    void selectWorld();

private slots:
    void on_loginButton_clicked();


private:

};


#endif // LOGINWIDGET_H

