#-------------------------------------------------
#
# Project created by QtCreator 2018-02-08T16:45:29
#
#-------------------------------------------------

QT       += core gui
QT       += charts
QT       += datavisualization

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = mainwindow
TEMPLATE = app

#include
#laptop
#INCLUDEPATH += c:/boost/boost_1_66_0
#INCLUDEPATH += c:/LL/QuantLib-1.12

#bloomberg
INCLUDEPATH += C:/boost/boost_1_66_0
INCLUDEPATH += C:/QuantLib/QuantLib-1.12

#All
INCLUDEPATH += C:/gslinclude/bin
INCLUDEPATH += C:/Alglib/src
#INCLUDEPATH += C:/eigen3

#libs
#laptop
#LIBS += c:\boost_msvc\lib\libboost_math_tr1-vc140-mt-gd-x64-1_66.lib
#LIBS += c:\LL\QuantLib-1.12\lib\QuantLib-vc140-x64-mt-gd.lib
#LIBS += C:/gsl/lib/gsl64.lib
#LIBS += C:/alglib/lib/alglib64.lib

#bloomberg debug
LIBS += c:/boost_msvc/lib/libboost_math_c99l-vc141-mt-gd-x64-1_66.lib
LIBS += C:/QuantLib/QuantLib-1.12/lib/QuantLib-vc141-x64-mt-gd.lib
LIBS += C:/gsl/lib/gsl64.lib
LIBS += C:/alglib/lib/alglib64.lib

#bloomberg release
#LIBS += c:/boost_msvc/lib/libboost_math_c99l-vc141-mt-x64-1_66.lib
#LIBS += C:/QuantLib/QuantLib-1.12/lib/QuantLib-vc141-x64-mt.lib
#LIBS += C:/gsl/lib/gsl64_release2.lib
#LIBS += C:/alglib/lib/alglib64_release.lib

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    fancytab.cpp \
    fancytabbar.cpp \
    mytree.cpp \
    cell.cpp \
    spreadsheet.cpp \
    datamanager.cpp \
    mytreeitem.cpp \
    smithwilsonwidget.cpp \
    smithwilsonengine.cpp \
    yieldtermstructure.cpp \
    rateconverter.cpp \
    generalhullwhite.cpp \
    nullibor.cpp \
    dataclass.cpp \
    calibrationwidget.cpp \
    surfacegraph.cpp \
    delegate.cpp \
    rnwidget.cpp \
    scenariowidget.cpp \
    bargraph.cpp \
    loginwidget.cpp \
    calibrationwidget.cpp \
    compareWidget.cpp \
    sliceWidget.cpp \
    oneclickwidget.cpp \
    databasewidget.cpp \
    martingaleWidget.cpp \
    sol2shockwidget.cpp \
    CIR.cpp \
    LN.cpp \
    RSLN2.cpp \
    Vasicek.cpp \
    rwmodel.cpp \
    qcustomplot.cpp \
    realcalibrationwidget.cpp \
    realmainwindow.cpp \
    realrnwidget.cpp \
    realscenwidget.cpp


HEADERS += \
        mainwindow.h \
    fancytab.h \
    fancytabbar.h \
    mytree.h \
    cell.h \
    spreadsheet.h \
    datamanager.h \
    mytreeitem.h \
    smithwilsonwidget.h \
    smithwilsonengine.h \
    yieldtermstructure.h \
    rateconverter.h \
    generalhullwhite.h \
    nullibor.h \
    dataclass.h \
    calibrationwidget.h \
    surfacegraph.h \
    delegate.h \
    rnwidget.h \
    scenariowidget.h \
    bargraph.h \
    loginwidget.h \
    calibrationwidget.h \
    compareWidget.h \
    sliceWidget.h \
    oneclickwidget.h \
    databasewidget.h \
    martingaleWidget.h \
    sol2shockwidget.h \
    CIR.h \
    LN.h \
    RSLN2.h \
    Vasicek.h \
    rwmodel.h \
    qcustomplot.h \
    realcalibrationwidget.h \
    realmainwindow.h \
    realrnwidget.h \
    realscenwidget.h

FORMS += \
    login.ui

DISTFILES +=

RESOURCES += \
    button.qrc \
    images.qrc \
    translation.qrc \
    csv.qrc
