<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko_KR">
<context>
    <name>CalibrationWidget</name>
    <message>
        <source>QInputDialog::getText()</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Model Name:</source>
        <translation>모형이름:</translation>
    </message>
    <message>
        <source>Market Swaption Price</source>
        <translation>시장 스왑션 가격</translation>
    </message>
    <message>
        <source>Model Swaption Price</source>
        <translation>모형 스왑션 가격</translation>
    </message>
    <message>
        <source>Input Volatility:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Termstructure:</source>
        <translation>금리기간구조:</translation>
    </message>
    <message>
        <source>Model:</source>
        <translation>모형:</translation>
    </message>
    <message>
        <source>New Model</source>
        <translation>새 모형</translation>
    </message>
    <message>
        <source>Remove Model</source>
        <translation>모형 제거</translation>
    </message>
    <message>
        <source>Market Price</source>
        <translation>시장 가격</translation>
    </message>
    <message>
        <source>Model Price</source>
        <translation>모형 가격</translation>
    </message>
    <message>
        <source>Calibrate</source>
        <translation>모수추정</translation>
    </message>
    <message>
        <source>Add to project</source>
        <translation>프로젝트에 추가</translation>
    </message>
</context>
<context>
    <name>CompareWidget</name>
    <message>
        <source>Absolute Errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative Errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Absolute Swaption Price Errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative Swaption Price Errors</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginWidget</name>
    <message>
        <source>Form</source>
        <translation>KIDI-ESG Pro</translation>
    </message>
    <message>
        <source>Username:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Beta 1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select Item:</source>
        <translation> </translation>
    </message>
    <message>
        <source>Real World</source>
        <translation>현실세계(Real World)</translation>
    </message>
    <message>
        <source>Risk Neutral</source>
        <translation>위험중립(Risk Neutral)</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>열기</translation>
    </message>
    <message>
        <source>Cancle</source>
        <translation>취소</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Menus</source>
        <translation>보험개발원 ESG</translation>
    </message>
    <message>
        <source>Database</source>
        <translation>데이터베이스</translation>
    </message>
    <message>
        <source>One Click</source>
        <translation>간편메뉴</translation>
    </message>
    <message>
        <source>Sol2 Shock</source>
        <translation>Sol2충격시나리오</translation>
    </message>
    <message>
        <source>Smith-Wilson</source>
        <translation type="unfinished">스미스-윌슨</translation>
    </message>
    <message>
        <source>Calibration</source>
        <translation>모수추정</translation>
    </message>
    <message>
        <source>Compare</source>
        <translation>스왑션 가격 비교</translation>
    </message>
    <message>
        <source>Slice</source>
        <translation>스왑션 가격 단면</translation>
    </message>
    <message>
        <source>Random Number</source>
        <translation>난수생성</translation>
    </message>
    <message>
        <source>Scenario Generator</source>
        <translation>시나리오생성</translation>
    </message>
    <message>
        <source>Martingale Widget</source>
        <translation>마팅게일 테스트</translation>
    </message>
    <message>
        <source>&lt;i&gt;Choose a menu option, or right-click to invoke a context menu&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A context menu is available by right-clicking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invoked &lt;b&gt;File|New&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invoked &lt;b&gt;File|Open&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open csv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Csv files (*.csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>QInputDialog::getText()</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Variable Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invoked &lt;b&gt;File|Save&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invoked &lt;b&gt;File|Print&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>새로 만들기</translation>
    </message>
    <message>
        <source>Create a new file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Open...</source>
        <translation>열기</translation>
    </message>
    <message>
        <source>Open an existing file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>저장</translation>
    </message>
    <message>
        <source>Save the document to disk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Print...</source>
        <translation>인쇄</translation>
    </message>
    <message>
        <source>Print the document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>닫기</translation>
    </message>
    <message>
        <source>Exit the application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>파일</translation>
    </message>
    <message>
        <source>Yield Curve</source>
        <translation type="unfinished">금리기간구조</translation>
    </message>
    <message>
        <source>Scenario</source>
        <translation type="unfinished">시나리오생성</translation>
    </message>
</context>
<context>
    <name>OneClickWidget</name>
    <message>
        <source>Open Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generate</source>
        <translation>실행</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>저장</translation>
    </message>
    <message>
        <source>Date:</source>
        <translation>날짜:</translation>
    </message>
</context>
<context>
    <name>RealCalibrationWidget</name>
    <message>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>120 : 10 Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>60 : 5 Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>36 : 3 Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t Calibrate</source>
        <translation>모수추정 불가</translation>
    </message>
    <message>
        <source>Model Type:</source>
        <translation>모형 종류:</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>모형 이름:</translation>
    </message>
    <message>
        <source>model</source>
        <translation></translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="unfinished">열기</translation>
    </message>
    <message>
        <source>Cancle</source>
        <translation type="unfinished">취소</translation>
    </message>
    <message>
        <source>Calibration Target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>KOSPI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>S&amp;P 500</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TSE 300</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>KRW OIS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New Model</source>
        <translation>새 모형</translation>
    </message>
    <message>
        <source>Models:</source>
        <translation>모형:</translation>
    </message>
    <message>
        <source>Params:</source>
        <translation>모수:</translation>
    </message>
    <message>
        <source>Target Data:</source>
        <translation>모수추정 데이터:</translation>
    </message>
    <message>
        <source>Last Date:</source>
        <translation>최종일:</translation>
    </message>
    <message>
        <source>N Data:</source>
        <translation>추정 기간:</translation>
    </message>
    <message>
        <source>Calibrate</source>
        <translation>모수추정</translation>
    </message>
    <message>
        <source>Debug</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RealMainWindow</name>
    <message>
        <source>Calibration</source>
        <translation>모수추정</translation>
    </message>
    <message>
        <source>Random Number</source>
        <translation>난수생성</translation>
    </message>
    <message>
        <source>Scenario</source>
        <translation>시나리오생성</translation>
    </message>
    <message>
        <source>Random Number Generator</source>
        <translation>난수 생성기</translation>
    </message>
    <message>
        <source>Scenario Generator</source>
        <translation>시나리오 생성기</translation>
    </message>
    <message>
        <source>Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Market Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New</source>
        <translation>불러오기</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>제거</translation>
    </message>
    <message>
        <source>Select Item:</source>
        <translation type="unfinished"> </translation>
    </message>
    <message>
        <source>Market Data Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Interest Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Equity Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Model Type:</source>
        <translation type="unfinished">모형 종류:</translation>
    </message>
    <message>
        <source>IR : Vasicek</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>IR : CIR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>EQ : LN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>EQ : RSLN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Normal(0,1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unif(0,1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Short Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Equity Return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="unfinished">열기</translation>
    </message>
    <message>
        <source>Cancle</source>
        <translation type="unfinished">취소</translation>
    </message>
    <message>
        <source>Open csv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Csv files (*.csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>파일</translation>
    </message>
    <message>
        <source>&amp;New Project</source>
        <translation>새 프로젝트</translation>
    </message>
    <message>
        <source>New Project</source>
        <translation>새 프로젝트</translation>
    </message>
    <message>
        <source>&amp;Open Project</source>
        <translation>프로젝트 열기</translation>
    </message>
    <message>
        <source>Open Project</source>
        <translation>프로젝트 열기</translation>
    </message>
    <message>
        <source>&amp;Save Project</source>
        <translation>프로젝트 저장</translation>
    </message>
    <message>
        <source>Save Project</source>
        <translation>프로젝트 저장</translation>
    </message>
    <message>
        <source>Open dat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Csv files (*.dat)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RealRnWidget</name>
    <message>
        <source>QInputDialog::getText()</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Random Number Name:</source>
        <translation>난수:</translation>
    </message>
    <message>
        <source>Method:</source>
        <translation>난수 생성방법:</translation>
    </message>
    <message>
        <source>Seed Number:</source>
        <translation>시드:</translation>
    </message>
    <message>
        <source>Month:</source>
        <translation>개월 수:</translation>
    </message>
    <message>
        <source>N:</source>
        <translation>시나리오 수:</translation>
    </message>
    <message>
        <source>Normal Random Number:</source>
        <translation>정규 난수:</translation>
    </message>
    <message>
        <source>Month Slice Histogram:</source>
        <translation>월 기간 히스토그램:</translation>
    </message>
    <message>
        <source>N Slice Histogram:</source>
        <translation>시나리오 히스토그램:</translation>
    </message>
    <message>
        <source>Generate</source>
        <translation>난수 생성</translation>
    </message>
</context>
<context>
    <name>RealScenWidget</name>
    <message>
        <source>QInputDialog::getText()</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scenario Name:</source>
        <translation>시나리오 이름:</translation>
    </message>
    <message>
        <source>Model:</source>
        <translation>모형:</translation>
    </message>
    <message>
        <source>Params:</source>
        <translation>모수:</translation>
    </message>
    <message>
        <source>Random Number:</source>
        <translation>난수:</translation>
    </message>
    <message>
        <source>Generate</source>
        <translation>시나리오 생성</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>시나리오 저장</translation>
    </message>
    <message>
        <source>Scenario</source>
        <translation>시나리오:</translation>
    </message>
</context>
<context>
    <name>RnWidget</name>
    <message>
        <source>QInputDialog::getText()</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Random Number Name:</source>
        <translation type="unfinished">난수 이름:</translation>
    </message>
    <message>
        <source>Method:</source>
        <translation>난수 생성방법:</translation>
    </message>
    <message>
        <source>Seed Number:</source>
        <translation>시드:</translation>
    </message>
    <message>
        <source>Month:</source>
        <translation>개월 수:</translation>
    </message>
    <message>
        <source>N:</source>
        <translation>시나리오 수:</translation>
    </message>
    <message>
        <source>Temporary RN:</source>
        <translation>난수 목록:</translation>
    </message>
    <message>
        <source>Month Slice Histogram:</source>
        <translation>월 기간 히스토그램:</translation>
    </message>
    <message>
        <source>N Slice Histogram:</source>
        <translation>시나리오 히스토그램:</translation>
    </message>
    <message>
        <source>Generate</source>
        <translation>난수생성</translation>
    </message>
</context>
<context>
    <name>ScenarioWidget</name>
    <message>
        <source>QInputDialog::getText()</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scenario Name:</source>
        <translation>시나리오 이름:</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Model:</source>
        <translation>모형:</translation>
    </message>
    <message>
        <source>a:</source>
        <translation></translation>
    </message>
    <message>
        <source>sigma:</source>
        <translation></translation>
    </message>
    <message>
        <source>Termstructure:</source>
        <translation>금리기간구조:</translation>
    </message>
    <message>
        <source>Random Number:</source>
        <translation>난수:</translation>
    </message>
    <message>
        <source>Generate</source>
        <translation>시나리오 생성</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>시나리오 저장</translation>
    </message>
    <message>
        <source>Temporary Scenario</source>
        <translation>시나리오 목록</translation>
    </message>
</context>
<context>
    <name>SliceWidget</name>
    <message>
        <source>Option Maturity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Swap Maturity:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SmithWilsonWidget</name>
    <message>
        <source>Open Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>QInputDialog::getText()</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Termstructure Name:</source>
        <translation>금리기간구조:</translation>
    </message>
    <message>
        <source>Input Rate:</source>
        <translation>입력 금리:</translation>
    </message>
    <message>
        <source>UFR(Annually Compound, %):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alpha:</source>
        <translation>수렴속도(Alpha):</translation>
    </message>
    <message>
        <source>Shock(Parallel Shift, bp):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>LLP:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Convergence:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Curve Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generate</source>
        <translation>금리기간구조 생성</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>저장</translation>
    </message>
</context>
<context>
    <name>Spreadsheet</name>
    <message>
        <source>Spreadsheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot write file %1:
%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot read file %1:
%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The file is not a spreadsheet file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The information cannot be pastedbecause the copy and paste area are not the same size.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
