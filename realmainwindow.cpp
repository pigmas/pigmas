#include "realmainwindow.h"

RealMainWindow::RealMainWindow(QWidget *parent) : QMainWindow(parent)
{
    qDebug()<<"# RealMainWindow Constructor Called";
    createActions();
    createMenus();
    QFont f;
    f.setPointSize(10);
    this->setFont(f);

    // 0. Declare
    calibrationWidget = new RealCalibrationWidget(this);
    rnWidget = new RealRnWidget(this);
    scenWidget = new RealScenWidget(this);

    // 1. Layout
    this->setCentralWidget(mainWidget);
    mainWidget->setLayout(mainLayout);
    mainLayout->addWidget(mainTabBar);
    mainLayout->addWidget(treeSplitter);
    treeSplitter->setOrientation(Qt::Horizontal);
    treeSplitter->addWidget(debugSplitter);
    treeSplitter->addWidget(projectView);
    debugSplitter->setOrientation(Qt::Vertical);
    debugSplitter->addWidget(mainStacked);
    debugSplitter->addWidget(logText);
    mainStacked->addWidget(calibrationTabs);
    mainStacked->addWidget(rnTabs);
    mainStacked->addWidget(scenTabs);

    // 2. Implementation
    mainTabBar->addFancyTab(QIcon(":/new/prefix1/img/theta.svg"), tr("Calibration"));
    mainTabBar->addFancyTab(QIcon(":/new/prefix1/img/dice.svg"), tr("Random Number"));
    mainTabBar->addFancyTab(QIcon(":/new/prefix1/img/scenario.svg"), tr("Scenario"));
    calibrationTabs->addTab(calibrationWidget,tr("Calibration"));
    rnTabs->addTab(rnWidget,tr("Random Number Generator"));
    scenTabs->addTab(scenWidget,tr("Scenario Generator"));

        // projectView
    projectView->setModel(projectModel);
    projectView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    projectView->setStyleSheet("QTreeView { font-size: " + QString::number(10) + "pt; }");
    projectModel->setHorizontalHeaderLabels(QStringList("Project"));
    QStandardItem* marketDataNode = new QStandardItem("Market Data");
    QStandardItem* modelNode = new QStandardItem("Model");
    QStandardItem* randomNumberNode = new QStandardItem("Random Number");
    QStandardItem* scenarioNode = new QStandardItem("Scenario");
    projectModel->appendRow(marketDataNode);
    projectModel->appendRow(modelNode);
    projectModel->appendRow(randomNumberNode);
    projectModel->appendRow(scenarioNode);
    projectView->installEventFilter(this);
    projectViewMenu->addAction(tr("New"),this,SLOT(newTreeItem()));
    projectViewMenu->addAction(tr("Remove"),this,SLOT(delTreeItem()));
        // mainTabBar

    // 3. Size Policy
    treeSplitter->setSizes({850,150});
    debugSplitter->setSizes({850,150});
    projectView->setMinimumWidth(250);

    // 4. Signals
    connect(mainTabBar,SIGNAL(activeIndexChanged(qint32)),mainStacked,SLOT(setCurrentIndex(int)));

    // 5. Test

    readMarketData(":/csv/kospi_180228.csv","KOSPI");
    readMarketData(":/csv/snp.csv","SNP 500");
    readMarketData(":/csv/tse.csv","TSE 300");
    readMarketData(":/csv/kwcr1t.csv","KRW OIS");

    updateTree();
    calibrationWidget->updateAll();
    projectView->expandAll();
    mainTabBar->setActiveIndex(0);

}


void RealMainWindow::updateTree(){
    qDebug()<<"# RealMainWindow::updateTree() Called";
    //Clear All Model Items
    for(int i=0;i<projectModel->rowCount();i++){
        projectModel->item(i)->setRowCount(0);
    }
    //Update
        //Market Data
    for(int i=0;i<marketDataDB_.size();i++){
        projectModel->item(0)->appendRow(new QStandardItem(marketDataDB_.at(i)->name));
    };
        //Model
    for(int i=0;i<modelDB_.size();i++){
        projectModel->item(1)->appendRow(new QStandardItem(QString::fromStdString(modelDB_.at(i)->modelName)));
    };
        //RandomNumber
    for(int i=0;i<randomNumberDB_.size();i++){
        projectModel->item(2)->appendRow(new QStandardItem(randomNumberDB_.at(i)->name));
    };
        //Scenario
    for(int i=0;i<scenarioDB_.size();i++){
        projectModel->item(3)->appendRow(new QStandardItem(scenarioDB_.at(i)->name));
    };
}

void RealMainWindow::readMarketData(QString filePath, QString dataName){
    qDebug()<<"# RealMainWindow::readMarketData() Called";

    QTemporaryDir temporaryDir;
    QFile::copy(filePath, temporaryDir.path() + "/temp.txt");
    filePath= temporaryDir.path() + "/temp.txt";

    ifstream file(filePath.toStdString());
    marketDataDB_.push_back(boost::make_shared<DataClass>());
    marketDataDB_.back()->timeset = dm.csv_TS_Time(file);
    marketDataDB_.back()->dataset = dm.csv_TS_Data(file);
    marketDataDB_.back()->name = dataName;

}
void RealMainWindow::readModelData(int modelId, QString modelName){
    switch(modelId){
    case 0:
        modelDB_.push_back(boost::make_shared<MY::Vasicek>());
        break;
    case 1:
        modelDB_.push_back(boost::make_shared<CIR>());
        break;
    case 2:
        modelDB_.push_back(boost::make_shared<LN>());
        break;
    case 3:
        modelDB_.push_back(boost::make_shared<RSLN2>());
        break;
    }
    modelDB_.back()->modelName = modelName.toStdString();
}

void RealMainWindow::readRandomNumberData(QString filePath, QString dataName){
    qDebug()<<"# RealMainWindow::readMarketData() Called";

    QTemporaryDir temporaryDir;
    QFile::copy(filePath, temporaryDir.path() + "/temp.txt");
    filePath= temporaryDir.path() + "/temp.txt";

    ifstream file(filePath.toStdString());
    randomNumberDB_.push_back(boost::make_shared<DataClass>());
    randomNumberDB_.back()->dataMat = dm.csv_Matrix(file);
    randomNumberDB_.back()->name = dataName;

}

void RealMainWindow::readScenarioData(QString filePath, QString dataName){
    qDebug()<<"# RealMainWindow::readMarketData() Called";

    QTemporaryDir temporaryDir;
    QFile::copy(filePath, temporaryDir.path() + "/temp.txt");
    filePath= temporaryDir.path() + "/temp.txt";

    ifstream file(filePath.toStdString());
    scenarioDB_.push_back(boost::make_shared<DataClass>());
    scenarioDB_.back()->dataMat = dm.csv_Matrix(file);
    scenarioDB_.back()->name = dataName;

}

bool RealMainWindow::eventFilter(QObject *target, QEvent *event){
    //qDebug()<<"# RealMainWindow::eventFilter Called";
    QContextMenuEvent* e = dynamic_cast<QContextMenuEvent*>(event);
    //qDebug()<<"target : "<<target<<", event : "<<event<<", e : "<<e<<" QCursor::pos() : "<<QCursor::pos();
    if (event->type() == QEvent::ContextMenu && e!=0)
    {
        qDebug()<<" RealMain::eventFilter, projectView Context Menu Called : ";
        if(target==projectView){
                qDebug()<<"MainWindow MyTree Context Menu";
                projectViewMenu->exec(QCursor::pos());
                return true;
        }
    }
    return false;
}

void RealMainWindow::delTreeItem(){
    qDebug()<<"# RealMainWindow::delTreeItem() Called";
    int parentIndex = projectView->currentIndex().parent().row();
    int childIndex = projectView->currentIndex().row();
    qDebug()<<"parentIndex"<<parentIndex<<"childIndex"<<childIndex;
    if(parentIndex!=-1){
        switch(parentIndex){
        case 0:
            marketDataDB_.erase(marketDataDB_.begin()+childIndex);
            break;
        case 1:
            modelDB_.erase(modelDB_.begin()+childIndex);
            break;
        case 2:
            randomNumberDB_.erase(randomNumberDB_.begin()+childIndex);
            break;
        case 3:
            scenarioDB_.erase(scenarioDB_.begin()+childIndex);
            break;
        }
        updateTree();
        calibrationWidget->updateAll();
        rnWidget->updateAll();
        scenWidget->updateAll();
    }
}

void RealMainWindow::newTreeItem(){
    qDebug()<<"# RealMainWindow::newTreeItem() Called";
    // Declare
    QDialog* dialog = new QDialog(this);
    QVBoxLayout* mainLayout = new QVBoxLayout;
    QWidget* listContainer = new QWidget;
    QVBoxLayout* listLayout = new QVBoxLayout;
    QWidget* marketContainer = new QWidget;
    QVBoxLayout* marketLayout = new QVBoxLayout;
    QPlainTextEdit* marketText = new QPlainTextEdit;
    QWidget* modelContainer = new QWidget;
    QVBoxLayout* modelLayout = new QVBoxLayout;
    QPlainTextEdit* modelText = new QPlainTextEdit;
    QWidget* randomNumberContainer = new QWidget;
    QVBoxLayout* randomNumberLayout = new QVBoxLayout;
    QPlainTextEdit* randomNumberText = new QPlainTextEdit;
    QWidget* scenarioContainer = new QWidget;
    QVBoxLayout* scenarioLayout = new QVBoxLayout;
    QPlainTextEdit* scenarioText = new QPlainTextEdit;

    QWidget* optionContainer = new QWidget;
    QHBoxLayout* optionLayout = new QHBoxLayout;
    QStackedWidget* typeStacked = new QStackedWidget;
    QStackedWidget* textStacked = new QStackedWidget;

        // List
    QLabel* itemListLabel = new QLabel(tr("Select Item:"));
    QListWidget* itemList = new QListWidget;
    itemList->setStyleSheet("QListWidget { font-size: " + QString::number(10) + "pt; }");
    itemList->addItem(tr("Market Data"));
    itemList->addItem(tr("Model"));
    itemList->addItem(tr("Random Number"));
    itemList->addItem(tr("Scenario"));
    itemList->setCurrentRow(0);
    listContainer->setLayout(listLayout);
    listLayout->addWidget(itemListLabel);
    listLayout->addWidget(itemList);
        // Market Data List
    QLabel* marketLabel = new QLabel(tr("Market Data Type:"));
    QListWidget* marketList = new QListWidget;
    marketList->setStyleSheet("QListWidget { font-size: " + QString::number(10) + "pt; }");
    marketList->addItem(tr("Interest Rate"));
    marketList->addItem(tr("Equity Index"));
    marketList->setCurrentRow(0);
    marketContainer->setLayout(marketLayout);
    marketLayout->addWidget(marketLabel);
    marketLayout->addWidget(marketList);
        // Model List
    QLabel* modelLabel = new QLabel(tr("Model Type:"));
    QListWidget* modelList = new QListWidget;
    modelList->setStyleSheet("QListWidget { font-size: " + QString::number(10) + "pt; }");
    modelList->addItem(tr("IR : Vasicek"));
    modelList->addItem(tr("IR : CIR"));
    modelList->addItem(tr("EQ : LN"));
    modelList->addItem(tr("EQ : RSLN"));
    modelList->setCurrentRow(0);
    modelContainer->setLayout(modelLayout);
    modelLayout->addWidget(modelLabel);
    modelLayout->addWidget(modelList);
        // Random Number List
    QLabel* randomNumberLabel = new QLabel("Random Number Type:");
    QListWidget* randomNumberList = new QListWidget;
    randomNumberList->setStyleSheet("QListWidget { font-size: " + QString::number(10) + "pt; }");
    randomNumberList->addItem(tr("Normal(0,1)"));
    randomNumberList->addItem(tr("Unif(0,1)"));
    randomNumberList->setCurrentRow(0);
    randomNumberContainer->setLayout(randomNumberLayout);
    randomNumberLayout->addWidget(randomNumberLabel);
    randomNumberLayout->addWidget(randomNumberList);
        // Scenario List
    QLabel* scenarioLabel = new QLabel("Scenario Type:");
    QListWidget* scenarioList = new QListWidget;
    scenarioList->setStyleSheet("QListWidget { font-size: " + QString::number(10) + "pt; }");
    scenarioList->addItem(tr("Short Rate"));
    scenarioList->addItem(tr("Equity Return"));
    scenarioList->setCurrentRow(0);
    scenarioContainer->setLayout(scenarioLayout);
    scenarioLayout->addWidget(scenarioLabel);
    scenarioLayout->addWidget(scenarioList);
        // Type Stacked
    typeStacked->addWidget(marketContainer);
    typeStacked->addWidget(modelContainer);
    typeStacked->addWidget(randomNumberContainer);
    typeStacked->addWidget(scenarioContainer);
    connect(itemList,SIGNAL(currentRowChanged(int)),typeStacked,SLOT(setCurrentIndex(int)));
        // Text
            // Market Text
            marketText->setReadOnly(true);
            marketText->appendPlainText("1");
            // Model Text
            modelText->setReadOnly(true);
            modelText->appendPlainText("2");
            // RN Text
            randomNumberText->setReadOnly(true);
            randomNumberText->appendPlainText("3");
            // Scenario Text
            scenarioText->setReadOnly(true);
            scenarioText->appendPlainText("4");
        // Type Stacked
    textStacked->addWidget(marketText);
    textStacked->addWidget(modelText);
    textStacked->addWidget(randomNumberText);
    textStacked->addWidget(scenarioText);
    textStacked->setVisible(false);
    connect(itemList,SIGNAL(currentRowChanged(int)),textStacked,SLOT(setCurrentIndex(int)));

        // Button Box
    QWidget* buttonBox = new QWidget;
    QHBoxLayout* buttonBoxLayout = new QHBoxLayout;
    QPushButton* okButton = new QPushButton(tr("Ok"));
    QPushButton* cancleButton = new QPushButton(tr("Cancle"));
    QSpacerItem* hSpacer = new QSpacerItem(1,1,QSizePolicy::Expanding,QSizePolicy::Minimum);
    buttonBox->setLayout(buttonBoxLayout);
    buttonBoxLayout->addSpacerItem(hSpacer);
    buttonBoxLayout->addWidget(okButton);
    buttonBoxLayout->addWidget(cancleButton);
    connect(okButton, SIGNAL(clicked(bool)), dialog, SLOT(accept()));
    connect(cancleButton, SIGNAL(clicked(bool)), dialog, SLOT(reject()));

    // Layout
    optionContainer->setLayout(optionLayout);
    optionLayout->addWidget(listContainer);
    optionLayout->addWidget(typeStacked);
    optionLayout->addWidget(textStacked);
    mainLayout->addWidget(optionContainer);
    mainLayout->addWidget(buttonBox);
    dialog->setLayout(mainLayout);

    // Implementation
    optionLayout->setContentsMargins(5,5,5,5);
    mainLayout->setContentsMargins(0,0,0,0);
    int result = dialog->exec();
    int selectedItem = itemList->currentIndex().row();
    QListWidget* selectedList = qobject_cast<QListWidget*>(typeStacked->widget(itemList->currentIndex().row())->children().at(2));
    int selectedType = selectedList->currentIndex().row();
    if(result==1){
        switch(selectedItem){
            case 0:
                newMarketItem(selectedType);
                break;
            case 1:
                newModelItem(selectedType);
                break;
            case 2:
                newRandomNumberItem(selectedType);
                break;
            case 3:
                newScenarioItem(selectedType);
                break;
        }
    }
    delete dialog;
}
void RealMainWindow::newMarketItem(int type){
    qDebug()<<" RealMainWindow::newMarketItem() Called type : "<<type;
    //type : 0 = EQ, 1 = IR
    QString filePath = QFileDialog::getOpenFileName(this,tr("Open csv"),".",tr("Csv files (*.csv)"));
    qDebug()<<"filePath"<<filePath;
    QInputDialog* inputDialog = new QInputDialog(this);
    bool dialogResult;

    if(filePath!=""){
        QString newName = inputDialog->getText(this, "Rename Label", "New name:", QLineEdit::Normal,
                                               "DEFAULT TEXT", &dialogResult);
        if(dialogResult==1){
            readMarketData(filePath,newName);
            updateTree();
            calibrationWidget->updateAll();
        }
    }
    delete inputDialog;
}
void RealMainWindow::newModelItem(int type){
    qDebug()<<" RealMainWindow::newModelItem() Called type : "<<type;
    //type : 0 = IR_Vasicek, 1 = IR_CIR, 2 = EQ_LN, 3 = EQ_RSLN
    QInputDialog* inputDialog = new QInputDialog(this);
    bool dialogResult;
    QString newName = inputDialog->getText(this, "Rename Label", "New name:", QLineEdit::Normal,
                                           "DEFAULT TEXT", &dialogResult);
    if(dialogResult==1){
        readModelData(type,newName);
        updateTree();
        calibrationWidget->updateAll();
    }
    delete inputDialog;
}
void RealMainWindow::newRandomNumberItem(int type){
    qDebug()<<" RealMainWindow::newRandomNumberItem() Called type : "<<type;
    //type : 0 = Normal, 1 = Unif
    QString filePath = QFileDialog::getOpenFileName(this,tr("Open csv"),".",tr("Csv files (*.csv)"));
    qDebug()<<"filePath"<<filePath;
    QInputDialog* inputDialog = new QInputDialog(this);
    bool dialogResult;

    if(filePath!=""){
        QString newName = inputDialog->getText(this, "Rename Label", "New name:", QLineEdit::Normal,
                                                   "DEFAULT TEXT", &dialogResult);
        if(dialogResult==1){
        readRandomNumberData(filePath,newName);
        updateTree();
        }
     }
     delete inputDialog;
}
void RealMainWindow::newScenarioItem(int type){
    qDebug()<<" RealMainWindow::newScenarioItem() Called type : "<<type;
    //type : 0 = EQ, 1 = IR
    QString filePath = QFileDialog::getOpenFileName(this,tr("Open csv"),".",tr("Csv files (*.csv)"));
    qDebug()<<"filePath"<<filePath;
    QInputDialog* inputDialog = new QInputDialog(this);
    bool dialogResult;

    if(filePath!=""){
        QString newName = inputDialog->getText(0, "Rename Label", "New name:", QLineEdit::Normal,
                                               "DEFAULT TEXT", &dialogResult);
        if(dialogResult==1){
            readScenarioData(filePath,newName);
            updateTree();
        }
    }
    delete inputDialog;
}

void RealMainWindow::saveProject(QString path){
    qDebug()<<" RealMainWindow::saveProject() Called";

    QJsonObject projectObject;

    QJsonArray marketArray;
    QJsonArray modelArray;
    QJsonArray rnArray;
    QJsonArray scenArray;

    int numMarket = marketDataDB_.size();
    for(int i=0;i<numMarket;i++){
        QJsonObject marketObject;
        boost::shared_ptr<DataClass> marketDC = marketDataDB_.at(i);
        QString name = marketDC->name;
        std::vector<QString> timeVec = marketDC->timeset;
        std::vector<double> dataVec = marketDC->dataset;
        QJsonArray time, data;
        int numRow = timeVec.size();
        for(int j=0;j<numRow;j++){
            time.push_back(timeVec[j]);
            data.push_back(dataVec[j]);
        }
        marketObject["name"] = name;
        marketObject["time"] = time;
        marketObject["data"] = data;
        marketArray.push_back(marketObject);
    }

    int numModel = modelDB_.size();
    for(int i=0;i<numModel;i++){
        QJsonObject modelObject;
        boost::shared_ptr<RwModel> modelDC = modelDB_.at(i);
        int modelType = modelDC->modelIndex;
        std::string name = modelDC->modelName;
        std::vector<double> paramsVec = modelDC->getParams();
        QJsonArray params;
        int numRow = paramsVec.size();
        for(int j=0;j<numRow;j++){
            params.push_back(paramsVec[j]);
        }
        modelObject["type"] = modelType;
        modelObject["name"] = QString::fromStdString(name);
        modelObject["params"] = params;
        modelArray.push_back(modelObject);
    }

    int numRn = randomNumberDB_.size();
    for(int i=0;i<numRn;i++){
        QJsonObject rnObject;
        boost::shared_ptr<DataClass> rnDC = randomNumberDB_.at(i);
        QString name = rnDC->name;
        QuantLib::Matrix dataMat = rnDC->dataMat;
        int numRow = dataMat.size1();
        int numCol = dataMat.size2();
        QJsonArray dataRows;
        for(int j=0;j<numRow;j++){
            QJsonArray dataCols;
            for(int k=0;k<numCol;k++){
                dataCols.push_back(dataMat[j][k]);
            }
            dataRows.push_back(dataCols);
        }
        rnObject["name"] = name;
        rnObject["data"] = dataRows;
        rnArray.push_back(rnObject);
    }

    int numScen = scenarioDB_.size();
    for(int i=0;i<numScen;i++){
        QJsonObject scenObject;
        boost::shared_ptr<DataClass> scenDC = scenarioDB_.at(i);
        QString name = scenDC->name;
        QuantLib::Matrix dataMat = scenDC->dataMat;
        int numRow = dataMat.size1();
        int numCol = dataMat.size2();
        QJsonArray dataRows;
        for(int j=0;j<numRow;j++){
            QJsonArray dataCols;
            for(int k=0;k<numCol;k++){
                dataCols.push_back(dataMat[j][k]);
            }
            dataRows.push_back(dataCols);
        }
        scenObject["name"] = name;
        scenObject["data"] = dataRows;
        scenArray.push_back(scenObject);
    }

    projectObject["market"]=marketArray;
    projectObject["model"]=modelArray;
    projectObject["rn"]=rnArray;
    projectObject["scen"]=scenArray;

    //QFile saveFile("c:/saveproject/test.json");
    //QFile saveFile("c:/saveproject/test.dat");
    QFile saveFile(path);
    saveFile.open(QIODevice::WriteOnly);
    QJsonDocument saveDoc(projectObject);
    //saveFile.write(saveDoc.toJson());
    saveFile.write(saveDoc.toBinaryData());
    saveFile.close();

}

void RealMainWindow::loadProject(QString path){
    qDebug()<<" RealMainWindow::loadProject() Called";
    newProject();
    // Load Example
    //QFile loadFile(QString("c:/saveproject/test.json"));
    //QFile loadFile(QString("c:/saveproject/test.dat"));
    QFile loadFile(path);
    loadFile.open(QIODevice::ReadOnly);
    QByteArray loadData = loadFile.readAll();
    //QJsonDocument loadDoc(QJsonDocument::fromJson(loadData));
    QJsonDocument loadDoc(QJsonDocument::fromBinaryData(loadData));
    QJsonObject loadObject = loadDoc.object();

    QJsonArray marketArray = loadObject["market"].toArray();
    QJsonArray modelArray = loadObject["model"].toArray();
    QJsonArray rnArray = loadObject["rn"].toArray();
    QJsonArray scenArray = loadObject["scen"].toArray();

    int numMarket = marketArray.size();
    for(int i=0;i<numMarket;i++){
        QJsonObject marketObject = marketArray.at(i).toObject();
        boost::shared_ptr<DataClass> marketDC(boost::make_shared<DataClass>());
        std::vector<QString> timeVec;
        std::vector<double> dataVec;
        QString name = marketObject["name"].toString();
        QJsonArray timeArray = marketObject["time"].toArray();
        QJsonArray dataArray = marketObject["data"].toArray();
        int numRow = timeArray.size();
        for(int j=0;j<numRow;j++){
            timeVec.push_back(timeArray.at(j).toString());
            dataVec.push_back(dataArray.at(j).toDouble());
        }
        marketDC->name = name;
        marketDC->timeset = timeVec;
        marketDC->dataset = dataVec;
        marketDataDB_.push_back(marketDC);
    }

    int numModel = modelArray.size();
    for(int i=0;i<numModel;i++){
        QJsonObject modelObject = modelArray.at(i).toObject();
        int modelType = modelObject["type"].toInt();
        switch(modelType){
        case 0:
            modelDB_.push_back(boost::make_shared<MY::Vasicek>());
            break;
        case 1:
            modelDB_.push_back(boost::make_shared<CIR>());
            break;
        case 2:
            modelDB_.push_back(boost::make_shared<LN>());
            break;
        case 3:
            modelDB_.push_back(boost::make_shared<RSLN2>());
            break;
        }
        std::string name = modelObject["name"].toString().toStdString();
        std::vector<double> paramsVec;
        QJsonArray paramsArray = modelObject["params"].toArray();
        int numRow = paramsArray.size();
        for(int j=0;j<numRow;j++){
            paramsVec.push_back(paramsArray.at(j).toDouble());
        }
        modelDB_.back()->modelName=name;
        modelDB_.back()->setParams(paramsVec);
    }

    int numRn = rnArray.size();
    for(int i=0;i<numRn;i++){
        QJsonObject rnObject = rnArray.at(i).toObject();
        boost::shared_ptr<DataClass> rnDC(boost::make_shared<DataClass>());
        QString name = rnObject["name"].toString();
        int numRow = rnObject["data"].toArray().size();
        int numCol = rnObject["data"].toArray().at(0).toArray().size();
        QuantLib::Matrix dataMat(numRow,numCol);
        for(int j=0;j<numRow;j++){
            for(int k=0;k<numCol;k++){
                dataMat[j][k]=rnObject["data"].toArray().at(j).toArray().at(k).toDouble();
            }
        }
        rnDC->name = name;
        rnDC->dataMat = dataMat;
        randomNumberDB_.push_back(rnDC);
    }

    int numScen = scenArray.size();
    for(int i=0;i<numScen;i++){
        QJsonObject scenObject = scenArray.at(i).toObject();
        boost::shared_ptr<DataClass> scenDC(boost::make_shared<DataClass>());
        QString name = scenObject["name"].toString();
        int numRow = scenObject["data"].toArray().size();
        int numCol = scenObject["data"].toArray().at(0).toArray().size();
        QuantLib::Matrix dataMat(numRow,numCol);
        for(int j=0;j<numRow;j++){
            for(int k=0;k<numCol;k++){
                dataMat[j][k]=scenObject["data"].toArray().at(j).toArray().at(k).toDouble();
            }
        }
        scenDC->name = name;
        scenDC->dataMat = dataMat;
        scenarioDB_.push_back(scenDC);
    }

    updateTree();
    calibrationWidget->updateAll();
    rnWidget->updateAll();
    scenWidget->updateAll();

}

void RealMainWindow::newProject(){
    qDebug()<<"# RealMainWindow::newProject() Called";
    marketDataDB_.clear();
    modelDB_.clear();
    randomNumberDB_.clear();
    scenarioDB_.clear();
    updateTree();
    calibrationWidget->updateAll();
    rnWidget->updateAll();
    scenWidget->updateAll();
}

void RealMainWindow::createMenus(){
    qDebug()<<"# RealMainWindow::createMenus() Called";
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newAct);
    fileMenu->addAction(openAct);
    fileMenu->addAction(saveAct);
    fileMenu->addAction(closeAct);

    fileMenu->addAction(debugAct);
}

void RealMainWindow::createActions(){
    qDebug()<<"# RealMainWindow::createActions() Called";
    newAct = new QAction(tr("&New Project"), this);
    newAct->setStatusTip(tr("New Project"));
    connect(newAct, &QAction::triggered, this, &RealMainWindow::newProjectAct);
    openAct = new QAction(tr("&Open Project"), this);
    openAct->setStatusTip(tr("Open Project"));
    connect(openAct, &QAction::triggered, this, &RealMainWindow::openProjectAct);
    saveAct = new QAction(tr("&Save Project"), this);
    saveAct->setStatusTip(tr("Save Project"));
    connect(saveAct, &QAction::triggered, this, &RealMainWindow::saveProjectAct);
    closeAct = new QAction(tr("&Close Project"), this);
    closeAct->setStatusTip(tr("Close Project"));
    connect(closeAct, &QAction::triggered, this, &RealMainWindow::closeProjectAct);

    debugAct = new QAction(tr("&Debug"), this);
    debugAct->setStatusTip(tr("Debug"));
    connect(debugAct, &QAction::triggered, this, &RealMainWindow::debugProjectAct);


}

void RealMainWindow::newProjectAct(){
    qDebug()<<"# RealMainWindow::newProjectAct() Called";
    newProject();

}
void RealMainWindow::openProjectAct(){
    qDebug()<<"# RealMainWindow::openProjectAct() Called";
    QString filePath = QFileDialog::getOpenFileName(this,tr("Open dat"),".",tr("Csv files (*.dat)"));
    qDebug()<<"OpenfilePath"<<filePath;
    if(filePath!=""){
        loadProject(filePath);
    }

}
void RealMainWindow::saveProjectAct(){
    qDebug()<<"# RealMainWindow::saveProjectAct() Called";
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                        "/home",
                                                        QFileDialog::ShowDirsOnly
                                                        | QFileDialog::DontResolveSymlinks);
    QInputDialog* inputDialog = new QInputDialog();
    bool dialogResult;
    QString fileName;
    if(dir!=""){
        QString newName = inputDialog->getText(this, "Rename Label", "New name:", QLineEdit::Normal,
                                               "Project", &dialogResult);
        if(dialogResult==1){
            fileName=QString(newName+".dat");
            QString filePath = QString(dir+"/"+fileName);
            saveProject(filePath);
            qDebug()<<filePath;
        }
    }
    delete inputDialog;
}
void RealMainWindow::closeProjectAct(){
    qDebug()<<"# RealMainWindow::closeProjectAct() Called";
    this->close();
    this->deleteLater();
    emit(realMainSignal());
}

void RealMainWindow::debugProjectAct(){
    qDebug()<<"# RealMainWindow::debugProjectAct() Called";

}

RealMainWindow::~RealMainWindow(){
    qDebug()<<"# RealMainWindow Destructor Called";
    delete mainWidget;
}


