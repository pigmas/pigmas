#ifndef REALMAINWINDOW_H
#define REALMAINWINDOW_H

#include <boost/shared_ptr.hpp>

#include <QMainWindow>
#include <QWidget>
#include <QHBoxLayout>
#include <QSplitter>
#include <QTreeView>
#include <QStackedWidget>
#include <QTabWidget>
#include <QTextEdit>
#include <QStandardItemModel>
#include <QTemporaryDir>
#include <QListWidget>
#include <QSpacerItem>
#include <QPlainTextEdit>
#include <QFileDialog>
#include <QInputDialog>

#include "fancytabbar.h"
#include "dataclass.h"
#include "rwmodel.h"
#include "datamanager.h"

#include "realcalibrationwidget.h"
#include "realrnwidget.h"
#include "realscenwidget.h"

class RealCalibrationWidget;
class RealRnWidget;
class RealScenWidget;

class RealMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit RealMainWindow(QWidget *parent = nullptr);
    ~RealMainWindow();
    QWidget* mainWidget = new QWidget(this);
    FancyTabBar* mainTabBar = new FancyTabBar(this);
    QHBoxLayout* mainLayout = new QHBoxLayout;
    QStackedWidget* mainStacked = new QStackedWidget(this);
    QTabWidget* calibrationTabs = new QTabWidget(this);
    RealCalibrationWidget* calibrationWidget;
    QTabWidget* rnTabs = new QTabWidget(this);
    RealRnWidget* rnWidget;
    QTabWidget* scenTabs = new QTabWidget(this);
    RealScenWidget* scenWidget;
    QSplitter* debugSplitter = new QSplitter(this);
    QTextEdit* logText = new QTextEdit(this);
    QSplitter* treeSplitter = new QSplitter(this);
    QTreeView* projectView = new QTreeView(this);
    QMenu* projectViewMenu = new QMenu(this);
    QStandardItemModel* projectModel = new QStandardItemModel(this);

    void readMarketData(QString filePath,QString dataName);
    void readModelData(int modelId, QString modelName);
    void readRandomNumberData(QString filePath,QString dataName);
    void readScenarioData(QString filePath,QString dataName);
    void updateTree();
    bool eventFilter(QObject *target, QEvent *event);

    std::vector<boost::shared_ptr<DataClass>> marketDataDB(){ return marketDataDB_; }
    std::vector<boost::shared_ptr<RwModel>> modelDB(){ return modelDB_; }
    std::vector<boost::shared_ptr<DataClass>> randomNumberDB(){ return randomNumberDB_; }
    std::vector<boost::shared_ptr<DataClass>> scenarioDB(){ return scenarioDB_; }
    void setMarketDataDB(std::vector<boost::shared_ptr<DataClass>> marketDataDB){ marketDataDB_= marketDataDB; }
    void setModelDB(std::vector<boost::shared_ptr<RwModel>> modelDB){ modelDB_ = modelDB; }
    void setRandomNumberDB(std::vector<boost::shared_ptr<DataClass>> randomNumberDB){ randomNumberDB_ = randomNumberDB; }
    void setScenarioDB(std::vector<boost::shared_ptr<DataClass>> scenarioDB){ scenarioDB_ = scenarioDB; }

    DataManager dm;

    std::vector<boost::shared_ptr<DataClass>> marketDataDB_;
    std::vector<boost::shared_ptr<RwModel>> modelDB_;
    std::vector<boost::shared_ptr<DataClass>> randomNumberDB_;
    std::vector<boost::shared_ptr<DataClass>> scenarioDB_;

signals:
    void realMainSignal();

public slots:
    void delTreeItem();
    void newTreeItem();
    void newMarketItem(int type);
    void newModelItem(int type);
    void newRandomNumberItem(int type);
    void newScenarioItem(int type);
    void saveProject(QString Path);
    void loadProject(QString Path);
    void newProject();
    void newProjectAct();
    void openProjectAct();
    void saveProjectAct();
    void closeProjectAct();

    void debugProjectAct();

private:
    void createMenus();
    void createActions();
    QMenu *fileMenu;
    QAction *newAct;
    QAction *openAct;
    QAction *saveAct;
    QAction *closeAct;

    QAction *debugAct;

};

#endif // REALMAINWINDOW_H
