#ifndef SLICEWIDGET_H
#define SLICEWIDGET_H

#include <QWidget>
#include "mainwindow.h"
#include "spreadsheet.h"
#include "surfacegraph.h"
#include "bargraph.h"
#include "ql/math/matrix.hpp"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QComboBox>

#include <QtCharts/QLineSeries>
#include <QtCharts/QChartView>

#include<QtDataVisualization/Q3DSurface>
#include<QtDataVisualization/QSurface3DSeries>
#include<QtDataVisualization/Q3DBars>

class MainWindow;
class Spreadsheet;
class QtCharts::QChartView;


using namespace QuantLib;
using namespace QtDataVisualization;

class SliceWidget : public QWidget
{
    Q_OBJECT

public:
    SliceWidget(QWidget *parent = 0);
    SliceWidget(MainWindow* mainPtr = 0);

    QtCharts::QChartView* optionView = new QtCharts::QChartView();
    QtCharts::QChartView* swapView = new QtCharts::QChartView();

    QVBoxLayout* mainLayout = new QVBoxLayout;
    QHBoxLayout* chartLayout = new QHBoxLayout;
    QHBoxLayout* selectLayout = new QHBoxLayout;

    Spreadsheet* absoluteTable;
    Spreadsheet* relativeTable;

    QLabel* optionLabel = new QLabel(tr("Option Maturity:"));
    QLabel* swapLabel = new QLabel(tr("Swap Maturity:"));

    QComboBox* optionCombo = new QComboBox;
    QComboBox* swapCombo = new QComboBox;


    void init();
    void layoutSetting();
    void signalSetting();
    void generate();
    void chartUpdate();


private:
    MainWindow* mainPtr_;
    vector<Matrix> tempRnVector_;

private slots:
    void on_optionCombo_changed();
    void on_swapCombo_changed();
signals:
    void updated();
};

#endif // CALIBRATIONWIDGET_H
