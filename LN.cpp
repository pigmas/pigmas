#include "ln.h"
#include <gsl/gsl_sf_bessel.h>
#include <iostream>
#include <math.h>
#include <QDebug>
#include <algorithm>
using namespace std;

LN::LN(){

    modelIndex = 2;
    params_={0.01,0.04};
    paramsName_={"Mu","Sigma"};

}
std::vector<double> LN::calibrate(QuantLib::Matrix indexData){
    qDebug()<<" LN::calibrate Called";
    // indexData input : d0=today d1=past
    // Transform
    int sizeIndex = indexData.rows();
    QuantLib::Matrix temp(sizeIndex,1);
    for(int i=0;i<sizeIndex;i++){
        temp[i][0]=indexData[sizeIndex-1-i][0];
    }
    indexData = temp;
    // calibrate() logic indexData : d0=past d1=today
	QuantLib::Matrix logReturn(sizeIndex-1,1);
	for (int i = 0; i < sizeIndex-1; i++){
		logReturn[i][0]=log(indexData[i+1][0] / indexData[i][0]);
	}
	QuantLib::Matrix ones(1, sizeIndex - 1);
	for (int j = 0; j < sizeIndex - 1; j++){
		ones[0][j] = 1.0;
	}
	QuantLib::Matrix mean = (ones*logReturn / (sizeIndex - 1));
	QuantLib::Matrix stdev = (QuantLib::transpose(logReturn)*logReturn - (sizeIndex - 1)*mean*mean) / (sizeIndex - 1);
	stdev[0][0] = sqrt(stdev[0][0]);
    params_ = { mean[0][0], stdev[0][0] };
    return params_;
}

QuantLib::Matrix LN::scenario(QuantLib::Matrix& rnd){
    qDebug()<<" LN::scenario Called";

    double mu = params_[0];
    double sigma = params_[1];
    double dt = 1.0/12.0;

    int numScen = rnd.size1();
    int numTerm = rnd.size2();
    QuantLib::Matrix scen(numScen,numTerm);

    for(int i=0;i<numScen;i++){
        for(int j=0;j<numTerm;j++){
            scen[i][j]=exp(mu*dt-0.5*dt*sigma*sigma+sigma*sqrt(dt)*rnd[i][j])-1.0;
        }
    }
    return scen;

}
