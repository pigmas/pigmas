#include "yieldtermstructure.h"

using namespace QuantLib;

MY::YieldTermStructure::YieldTermStructure(const QuantLib::Date& referenceDate,
                                           const QuantLib::DayCounter& dayCounter)
 : QuantLib::YieldTermStructure(referenceDate, QuantLib::Calendar(), dayCounter)
{

}

MY::YieldTermStructure::YieldTermStructure(QuantLib::Matrix dm_)
{
    discount_Month=dm_;
}

void MY::YieldTermStructure::setDiscountMonth(QuantLib::Matrix dm_)
{
    discount_Month=dm_;
}

//double MY::YieldTermStructure::discount(double t)
//{
//    return discount_Month[0][int(round(t*12.0-1.0))];//not use
//}


