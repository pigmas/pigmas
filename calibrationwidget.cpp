#include "calibrationwidget.h"
#include "nullibor.h"

#include "ql/quotes/simplequote.hpp"
#include "ql/termstructures/yield/flatforward.hpp"
#include "ql/models/shortrate/calibrationhelpers/swaptionhelper.hpp"
#include "ql/pricingengines/swaption/jamshidianswaptionengine.hpp"
#include "ql/math/optimization/levenbergmarquardt.hpp"

#include "delegate.h"
#include <QSizePolicy>
#include <QInputDialog>
#include <QSplitter>

using namespace QuantLib;
class GeneralHullWhite;


CalibrationWidget::CalibrationWidget(QWidget *parent)
    : QWidget(parent)
{
    init();
    signalSetting();
}

CalibrationWidget::CalibrationWidget(MainWindow* mainPtr)
{
    mainPtr_=mainPtr;
    init();
    signalSetting();

}

void CalibrationWidget::init(){
    layoutSetting();
    buttonCheck();
    for(int i=0;i<6;i++){
        for(int j=0;j<6;j++){
                (*currentMarketVolPtr_)[i][j]=0.3;
        }
    }
    Array newArray(10);
    newArray[0]=0.05;newArray[1]=0.04;newArray[2]=0.03;
    newArray[3]=0.007;newArray[4]=0.006;newArray[5]=0.005;newArray[6]=0.004;newArray[7]=0.003;newArray[8]=0.002;newArray[9]=0.001;
    temporaryParamsVector.push_back(newArray);

}

void CalibrationWidget::signalSetting(){
    connect(modelComboBox,SIGNAL(currentIndexChanged(const QString&)),this,SLOT(on_modelComboBox_changed()));
    connect(inputVolComboBox,SIGNAL(currentIndexChanged(const QString&)),this,SLOT(on_inputVolComboBox_changed()));

    connect(newModelPush,SIGNAL(clicked(bool)),this,SLOT(on_newModelPush_clicked()));
    connect(delModelPush,SIGNAL(clicked(bool)),this,SLOT(on_delModelPush_clicked()));
    connect(calibratePush,SIGNAL(clicked(bool)),this,SLOT(on_calibratePush_clicked()));

    connect(addToProjectPush,SIGNAL(clicked(bool)),this,SLOT(on_addToProjectPush_clicked()));
    connect(modelPricePush,SIGNAL(clicked(bool)),this,SLOT(on_modelPricePush_clicked()));
    connect(marketPricePush,SIGNAL(clicked(bool)),this,SLOT(on_marketPricePush_clicked()));
}

void CalibrationWidget::buttonCheck(){
    if(inputVolComboBox->currentIndex()==-1 || termstructureComboBox->currentIndex()==-1 || modelComboBox->currentIndex()==-1){
        calibratePush->setEnabled(false);
        marketPricePush->setEnabled(false);
        modelPricePush->setEnabled(false);
    }else{
        calibratePush->setEnabled(true);
        marketPricePush->setEnabled(true);
        modelPricePush->setEnabled(true);
    }
    if(termstructureComboBox->currentIndex()==-1 || modelComboBox->currentIndex()==-1){
        //modelPricePush->setEnabled(false);//default vol setting errorr
    }else{
        //modelPricePush->setEnabled(true);
    }
    if(modelComboBox->currentIndex()==-1){
        delModelPush->setEnabled(false);
        addToProjectPush->setEnabled(false);
        paramsTable1->setEnabled(false);
        paramsTable2->setEnabled(false);
    }else{
        delModelPush->setEnabled(true);
        addToProjectPush->setEnabled(true);
        paramsTable1->setEnabled(true);
        paramsTable2->setEnabled(true);
    }
}

void CalibrationWidget::marketGraphUpdate(int mode){
    qDebug()<<"mode=1 : vol, mode=2 : price";

    currentMarketVolPtr_=mainPtr_->getMyTree()->getVolItemVector().at(inputVolComboBox->currentIndex())->getDataMatrixPtr();
    if(mode==1){
        marketModifier->setVolatilitySurface(currentMarketVolPtr_);
    }else if(mode==2){
        marketModifier->setVolatilitySurface(currentMarketPricePtr_);
    }

}

void CalibrationWidget::modelGraphUpdate(){

    modelModifier->setVolatilitySurface(currentModelPricePtr_);

}



void CalibrationWidget::update(){
    //update vol combobox
    qDebug()<<"CalibrationWidget slot update called";
    buttonCheck();
    paramsUpdate();
        inputVolComboBox->clear();
        qDebug()<<mainPtr_->getMyTree()->getVolItemVector().size()<<"calibrationWidget Update getVolItemVectorSizeCalled";



        for(int i=0;i<mainPtr_->getMyTree()->getVolItemVector().size();i++){
            inputVolComboBox->addItem(mainPtr_->getMyTree()->getVolItemVector()[i]->text());
        }
            inputVolComboBox->setCurrentIndex(mainPtr_->getMyTree()->getVolItemVector().size()-1);
        //qDebug()<<mainPtr_->getMyTree()->getInputItemVector().size()<<"updateSize";
        //update termstructure combobox

        termstructureComboBox->clear();
        for(int i=0;i<mainPtr_->getMyTree()->getYieldItemVector().size();i++){
            termstructureComboBox->addItem(mainPtr_->getMyTree()->getYieldItemVector()[i]->text());
        }
        termstructureComboBox->setCurrentIndex(mainPtr_->getMyTree()->getYieldItemVector().size()-1);

}
void CalibrationWidget::tableUpdate(){
    qDebug()<<"table update called";

    int i=modelComboBox->currentIndex();

    tempParams1_=Matrix(1,2);
    tempParams2_=Matrix(1,6);

    tempParams1_[0][0]=temporaryParamsVector.at(i)[0];
    tempParams1_[0][1]=temporaryParamsVector.at(i)[1];

    tempParams2_[0][0]=temporaryParamsVector.at(i)[3];
    tempParams2_[0][1]=temporaryParamsVector.at(i)[4];
    tempParams2_[0][2]=temporaryParamsVector.at(i)[5];
    tempParams2_[0][3]=temporaryParamsVector.at(i)[6];
    tempParams2_[0][4]=temporaryParamsVector.at(i)[7];
    tempParams2_[0][5]=temporaryParamsVector.at(i)[8];

    paramsTable1->readMatrix(&tempParams1_);
    paramsTable2->readMatrix(&tempParams2_);

    QStringList aHeaderLabels;
    aHeaderLabels<<"0~10"<<"10~20";
    paramsTable1->setHorizontalHeaderLabels(aHeaderLabels);

    QStringList sigmaHeaderLabels;
    sigmaHeaderLabels<<"0~1"<<"1~2"<<"2~3"<<"3~5"<<"5~7"<<"7~10";
    paramsTable2->setHorizontalHeaderLabels(sigmaHeaderLabels);


}
void CalibrationWidget::paramsUpdate(){
    if(modelComboBox->currentIndex()!=-1){
        qDebug()<<"params update called";


        int i=modelComboBox->currentIndex();
//        paramsArray=Array(10);
//        paramsArray[0]=tempParams1_[0][0];
//        paramsArray[1]=tempParams1_[0][1];
//        paramsArray[2]=temporaryModelVector.at(modelComboBox->currentIndex())->params().at(3);
//        paramsArray[3]=tempParams2_[0][0];
//        paramsArray[4]=tempParams2_[0][1];
//        paramsArray[5]=tempParams2_[0][2];
//        paramsArray[6]=tempParams2_[0][3];
//        paramsArray[7]=tempParams2_[0][4];
//        paramsArray[8]=tempParams2_[0][5];
//        paramsArray[9]=temporaryModelVector.at(modelComboBox->currentIndex())->params().at(9);
//        temporaryModelVector.at(modelComboBox->currentIndex())->setParams(paramsArray);
//        qDebug()<<temporaryModelVector.at(modelComboBox->currentIndex())->params().at(0)<<"paramupdate paramcheck";
        temporaryParamsVector.at(i)[0]=tempParams1_[0][0];
        temporaryParamsVector.at(i)[1]=tempParams1_[0][1];

        temporaryParamsVector.at(i)[3]=tempParams2_[0][0];
        temporaryParamsVector.at(i)[4]=tempParams2_[0][1];
        temporaryParamsVector.at(i)[5]=tempParams2_[0][2];
        temporaryParamsVector.at(i)[6]=tempParams2_[0][3];
        temporaryParamsVector.at(i)[7]=tempParams2_[0][4];
        temporaryParamsVector.at(i)[8]=tempParams2_[0][5];


    }
}
void CalibrationWidget::calibrator(int mode){
    qDebug()<<"Calibration Widget on_calibratePush_clicked()";
    qDebug()<<"1: Calibration";
    qDebug()<<"2: Model Vol Price";
    qDebug()<<"3: Market Vol Price";

    int modelIndex=modelComboBox->currentIndex();
    int termIndex=termstructureComboBox->currentIndex();
    int volIndex=inputVolComboBox->currentIndex();

    if(termstructureComboBox->currentIndex()!=-1){


        ts=mainPtr_->getMyTree()->getYieldItemVector().at(termIndex)->getTermStructure();
        Matrix dm_=ts.getDiscountMonth();
        boost::shared_ptr<MY::YieldTermStructure> rth(new MY::YieldTermStructure);
        rth->setDiscountMonth(dm_);
        Handle<QuantLib::YieldTermStructure> currentTermHandle(rth);

        boost::shared_ptr<GeneralHullWhite> modelGHWcalibration(new GeneralHullWhite(currentTermHandle));
        modelGHWcalibration->setParams(temporaryParamsVector.at(modelIndex));
        qDebug()<<modelGHWcalibration->discountBond(3,10,0.01)<<"calib model discount bond(3,10) ";

        boost::shared_ptr<IborIndex> nullIndex(new NullIbor(currentTermHandle));

        std::vector<Period> optionMaturities;

        optionMaturities.push_back(Period(1, Years));
        optionMaturities.push_back(Period(2, Years));
        optionMaturities.push_back(Period(3, Years));
        optionMaturities.push_back(Period(5, Years));
        optionMaturities.push_back(Period(7, Years));
        optionMaturities.push_back(Period(10, Years));

        std::vector<Period> swapMaturities;

        swapMaturities.push_back(Period(1, Years));
        swapMaturities.push_back(Period(2, Years));
        swapMaturities.push_back(Period(3, Years));
        swapMaturities.push_back(Period(5, Years));
        swapMaturities.push_back(Period(7, Years));
        swapMaturities.push_back(Period(10, Years));


        std::vector<boost::shared_ptr<CalibrationHelper>> calibHelper;
        Volatility swaptionVols[36];

        currentMarketVolPtr_=mainPtr_->getMyTree()->getVolItemVector().at(volIndex)->getDataMatrixPtr();

        int numRow=currentMarketVolPtr_->size1();
        int numCol=currentMarketVolPtr_->size2();

        for(int i=0;i<numRow;i++){
            for(int j=0;j<numCol;j++){
                int k=i*6+j;
                swaptionVols[k]=(*currentMarketVolPtr_)[i][j];
            }
        }

        std::list<Time> times;
        boost::shared_ptr<PricingEngine> jamsPricingEngineGHW(new JamshidianSwaptionEngine(modelGHWcalibration));


        for(int i=0;i<numRow;i++){
            for(int j=0;j<numCol;j++){

                int k=i*6+j;

                swaptionVols[k]=(*currentMarketVolPtr_)[i][j];
                boost::shared_ptr<Quote> vol(new SimpleQuote(swaptionVols[k]));
                calibHelper.push_back(boost::shared_ptr<CalibrationHelper>(new SwaptionHelper(optionMaturities[i],
                                                                                              swapMaturities[j],
                                                                                              Handle<Quote>(vol),
                                                                                              nullIndex,
                                                                                              nullIndex->tenor(),
                                                                                              nullIndex->dayCounter(),
                                                                                              nullIndex->dayCounter(),
                                                                                              currentTermHandle)));
                calibHelper.back()->addTimesTo(times);
            }
        }

        TimeGrid grid(times.begin(), times.end(), 30);

        for (int i=0; i<calibHelper.size(); i++){
            calibHelper[i]->setPricingEngine(jamsPricingEngineGHW);
        }

        if(mode==1){
            //1. Calibration
            LevenbergMarquardt om;

            modelGHWcalibration->calibrate(calibHelper, om, EndCriteria(400, 100, 1.0e-8, 1.0e-8, 1.0e-8),BoundaryConstraint(0.00001,0.3));

            for(int i=0;i<36;i++){
                qDebug()<<calibHelper[i]->modelValue()<<calibHelper[i]->marketValue()<<"model/market value"<<i;
            }
            for(int j=0;j<10;j++){
                qDebug()<<modelGHWcalibration->params()[j]<<"param"<<j;
            }

            temporaryParamsVector.at(modelIndex)=modelGHWcalibration->params();
            tableUpdate();
        }else if(mode==2){
            for(int i=0;i<6;i++){
                for(int j=0;j<6;j++){
                    int k=i*6+j;
                    (*currentModelPricePtr_)[i][j]=calibHelper[k]->modelValue();
                }
            }
        }else if(mode==3){
            for(int i=0;i<6;i++){
                for(int j=0;j<6;j++){
                    int k=i*6+j;
                    (*currentMarketPricePtr_)[i][j]=calibHelper[k]->marketValue();
                }
            }
        }

    }

//    std::ofstream file("c:/aaa.csv");
//    Matrix matrix=ts.getDiscountMonth();
//    for(unsigned int i = 0; i != matrix.rows(); i++)
//    {
//     // concatenate column values into string separated by semicolon
//     std::string stream;
//     for(unsigned int j = 0; j != matrix.columns(); j++)
//     {
//      stream += (std::to_string(matrix[i][j]) + ";");
//     }
//     // print string into text file
//     file << stream << std::endl;
//    }
//    // close text file
//    file.close();

}

void CalibrationWidget::on_inputVolComboBox_changed(){
    qDebug()<<"CalibrationWidget inputComboBox changed called";
    qDebug()<<inputVolComboBox->currentIndex()<<"current index";
    buttonCheck();

    //qDebug()<<mainPtr_->getMyTree()->getItemVector().at(inputComboBox->currentIndex())->text();
    qDebug()<<mainPtr_->getMyTree()->getVolItemVector().size()<<"changedSize";
    if(inputVolComboBox->currentIndex()!=-1){
        if(modelComboBox->currentIndex()!=-1){
            marketGraphUpdate(2);
        }
    }
    if(inputVolComboBox->currentIndex()==-1){
        //void chart
        marketModifier->voidGraph();
    }
    //update();

}

void CalibrationWidget::on_modelComboBox_changed(){
    qDebug()<<"CalibrationWidget modelComboBox changed called";
    qDebug()<<modelComboBox->currentIndex()<<"current index";
    buttonCheck();

    int i=modelComboBox->currentIndex();
    if(i==-1){
    }
    if(i!=-1){

        tableUpdate();

    }
    update();

}


void CalibrationWidget::on_newModelPush_clicked(){
    qDebug()<<"Calibration Widget on_newModelPush_clicked()";

    bool ok;
    QString varName = QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                  tr("Model Name:"), QLineEdit::Normal,
                                  "", &ok);
    if(varName==""){
        varName="null";
    }
    //temporary curve for initialize Model
    //Date settlementDate(19, February, 2002);
    //boost::shared_ptr<Quote> flatRate(new QuantLib::SimpleQuote(0.05));
    //Handle<QuantLib::YieldTermStructure> rhTempTermstructure(boost::shared_ptr<FlatForward>(new FlatForward(settlementDate,Handle<Quote>(flatRate),Actual365Fixed())));
    //boost::shared_ptr<GeneralHullWhite> modelGHWinitial(new GeneralHullWhite(rhTempTermstructure));
    //temporaryModelVector.push_back(modelGHWinitial);
    Array newArray(10);
    newArray[0]=0.05;newArray[1]=0.04;newArray[2]=0.03;
    newArray[3]=0.007;newArray[4]=0.006;newArray[5]=0.005;newArray[6]=0.004;newArray[7]=0.003;newArray[8]=0.002;newArray[9]=0.001;
    temporaryParamsVector.push_back(newArray);
    modelComboBox->addItem(varName);
    //qDebug()<<temporaryModelVector.at(modelComboBox->currentIndex())->discountBond(3,10,0.01)<<"new model discount bond(3,10)";

//    temporaryCurveNameVector.push_back(varName);
//    temporaryCurveVector.push_back(currentYieldTermStructure);
    QString msg;
    msg+="Task No.";
    msg+=QString::number(mainPtr_->taskNum);
        msg+=" ";
    msg+=QDateTime::currentDateTime().toString();
    msg+=" ";
    msg+=" New HW Model added";
    mainPtr_->logText->append(msg);
    mainPtr_->taskNum++;


    update();

}
void CalibrationWidget::on_delModelPush_clicked(){
    qDebug()<<"Calibration Widget on_delModelPush_clicked()";

    //temporaryModelVector.erase(temporaryModelVector.begin()+modelComboBox->currentIndex());
    temporaryParamsVector.erase(temporaryParamsVector.begin()+modelComboBox->currentIndex());
    modelComboBox->removeItem(modelComboBox->currentIndex());
}

void CalibrationWidget::on_calibratePush_clicked(){


    //qDebug()<<thread->priority()<<"Priority Before";


    progressBar->setVisible(true);

    thread = new QThread;
    worker = new Worker(mainPtr_);

    worker->moveToThread(thread);
    connect(thread, SIGNAL (started()), worker, SLOT (process()));
    connect(worker, SIGNAL (finished()),this,SLOT (afterWorkerFinished()));
    connect(worker, SIGNAL (finished()), thread, SLOT (quit()));
    connect(worker, SIGNAL (finished()), worker, SLOT (deleteLater()));
    connect(thread, SIGNAL (finished()), thread, SLOT (deleteLater()));
    this->thread->start();
    thread->setPriority(QThread::HighestPriority);
    qDebug()<<thread->priority()<<"Priority After";

}
void CalibrationWidget::afterWorkerFinished(){
    calibrator(2);
    calibrator(3);
    marketGraphUpdate(2);
    modelGraphUpdate();
    tableUpdate();
    marketVolTable->readMatrix(mainPtr_->calibrationWidget->currentMarketPricePtr_);
    modelVolTable->readMatrix(mainPtr_->calibrationWidget->currentModelPricePtr_);
    mainPtr_->compareWidget->barGraphUpdate();
    mainPtr_->sliceWidget->chartUpdate();
    progressBar->setVisible(false);


}

void CalibrationWidget::calibrate_oneCliclk(){


    //qDebug()<<thread->priority()<<"Priority Before";


    progressBar->setVisible(true);

    thread = new QThread;
    worker = new Worker(mainPtr_);

    worker->moveToThread(thread);
    connect(thread, SIGNAL (started()), worker, SLOT (process()));
    connect(worker, SIGNAL (finished()),this,SLOT (afterWorkerFinished()));
    connect(worker, SIGNAL (finished()),mainPtr_->oneClickWidget,SLOT (afterWorkerFinished()));
    connect(worker, SIGNAL (finished()), thread, SLOT (quit()));
    connect(worker, SIGNAL (finished()), worker, SLOT (deleteLater()));
    connect(thread, SIGNAL (finished()), thread, SLOT (deleteLater()));
    this->thread->start();
    thread->setPriority(QThread::HighestPriority);
    qDebug()<<thread->priority()<<"Priority After";

}
void CalibrationWidget::on_modelPricePush_clicked(){
    qDebug()<<"Calibration Widget on_modelPricePush_clicked()";
    calibrator(2);
    modelGraphUpdate();
    qDebug()<<(*currentModelPricePtr_)[1][1];
    modelVolTable->readMatrix(mainPtr_->calibrationWidget->currentModelPricePtr_);
    mainPtr_->compareWidget->barGraphUpdate();
    mainPtr_->sliceWidget->chartUpdate();
}
void CalibrationWidget::on_marketPricePush_clicked(){
    qDebug()<<"Calibration Widget on_marketPricePush_clicked()";
    calibrator(3);
    marketGraphUpdate(2);
    qDebug()<<(*currentMarketPricePtr_)[1][1];
    marketVolTable->readMatrix(mainPtr_->calibrationWidget->currentMarketPricePtr_);
    mainPtr_->compareWidget->barGraphUpdate();
    mainPtr_->sliceWidget->chartUpdate();

}

void CalibrationWidget::on_addToProjectPush_clicked(){
    qDebug()<<"Calibration Widget on_addToProjectPush_clicked()";
    qDebug()<<temporaryParamsVector.at(modelComboBox->currentIndex())[0];
    Matrix saveParams(1,10);
    for(int i=0;i<10;i++){
        saveParams[0][i]=temporaryParamsVector.at(modelComboBox->currentIndex())[i];
    }

    QString varName=modelComboBox->itemText(modelComboBox->currentIndex());
    mainPtr_->getMyTree()->addModelItem(varName,saveParams);

    QString msg;
    msg+="Task No.";
    msg+=QString::number(mainPtr_->taskNum);
        msg+=" ";
    msg+=QDateTime::currentDateTime().toString();
    msg+=" ";
    msg+=" HW Model added To Project";
    mainPtr_->logText->append(msg);
    mainPtr_->taskNum++;
}

void CalibrationWidget::layoutSetting(){

    paramsTable1 = new Spreadsheet(mainPtr_);
    paramsTable1->setItemDelegate(new ItemDelegate);
    paramsTable1->setParamsTable();

    paramsTable2 = new Spreadsheet(mainPtr_);
    paramsTable2->setItemDelegate(new ItemDelegate);
    paramsTable2->setParamsTable();
    paramsTable2->setStyleSheet("font-size: " + QString::number(8.5) + "pt;");

    marketVolTable = new Spreadsheet(mainPtr_);
    marketVolTable->setItemDelegate(new ItemDelegate);
    marketVolTable->setVolTable();

    modelVolTable = new Spreadsheet(mainPtr_);
    modelVolTable->setItemDelegate(new ItemDelegate);
    modelVolTable->setVolTable();

    //paramsLayout->addWidget(paramsTable1);



    //mainLayout->addLayout(volLayout);
    //volLayout->addLayout(cubeLayout);
    //volLayout->addLayout(tableLayout);
    QWidget* cubeLayoutContainer = new QWidget;
    QWidget* tableLayoutContainer = new QWidget;
    cubeLayoutContainer->setLayout(cubeLayout);
    tableLayoutContainer->setLayout(tableLayout);
    tableLayoutContainer->setFixedHeight(230);
    QSplitter* volSplitter = new QSplitter;
    volSplitter->setOrientation(Qt::Vertical);
    volSplitter->addWidget(cubeLayoutContainer);
    volSplitter->addWidget(tableLayoutContainer);

    //mainLayout->addWidget(formContainer);
    //mainLayout->addWidget(volSplitter);

    QSplitter* mainSplitter = new QSplitter;
    mainSplitter->addWidget(formContainer);
    mainSplitter->addWidget(volSplitter);
    mainLayout->addWidget(mainSplitter);



    marketContainerLayout->addWidget(marketContainerLabel);
    marketContainerLabel->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
    marketContainerLayout->addWidget(marketContainer);
    marketContainer->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    modelContainerLayout->addWidget(modelContainerLabel);
    modelContainerLabel->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
    modelContainerLayout->addWidget(modelContainer);
    modelContainer->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    cubeLayout->addLayout(marketContainerLayout);
    cubeLayout->addLayout(modelContainerLayout);

    tableLayout->addWidget(marketVolTable);
    tableLayout->addWidget(modelVolTable);

    formContainer->setFixedWidth(300);

    formContainer->setLayout(optionLayout);
    optionLayout->setAlignment(Qt::AlignTop);
    optionLayout->addWidget(newModelPush);
    optionLayout->addWidget(delModelPush);
    optionLayout->addWidget(modelLabel);
    //modelLabel->setAlignment(Qt::AlignRight);
    optionLayout->addWidget(modelComboBox);
    optionLayout->addWidget(termstructureLabel);
    //termstructureLabel->setAlignment(Qt::AlignRight);
    optionLayout->addWidget(termstructureComboBox);
    optionLayout->addWidget(inputVolLabel);
    //inputVolLabel->setAlignment(Qt::AlignRight);
    optionLayout->addWidget(inputVolComboBox);
    optionLayout->addWidget(aLabel);
    optionLayout->addWidget(paramsTable1);
    optionLayout->addWidget(sigmaLabel);
    optionLayout->addWidget(paramsTable2);
    optionLayout->addWidget(calibratePush);
    optionLayout->addWidget(marketPricePush);
    optionLayout->addWidget(modelPricePush);
    optionLayout->addWidget(addToProjectPush);
    optionLayout->addWidget(progressBar);
    progressBar->setRange(0,0);
    progressBar->setTextVisible(false);
    progressBar->setVisible(false);


    this->setLayout(mainLayout);
    //test

    QSlider *axisMinSliderX = new QSlider(Qt::Horizontal, this);
    axisMinSliderX->setMinimum(0);
    axisMinSliderX->setTickInterval(1);
    axisMinSliderX->setEnabled(false);
    axisMinSliderX->hide();
    QSlider *axisMaxSliderX = new QSlider(Qt::Horizontal, this);
    axisMaxSliderX->setMinimum(1);
    axisMaxSliderX->setTickInterval(1);
    axisMaxSliderX->setEnabled(false);
    axisMaxSliderX->hide();
    QSlider *axisMinSliderZ = new QSlider(Qt::Horizontal, this);
    axisMinSliderZ->setMinimum(0);
    axisMinSliderZ->setTickInterval(1);
    axisMinSliderZ->setEnabled(false);
    axisMinSliderZ->hide();
    QSlider *axisMaxSliderZ = new QSlider(Qt::Horizontal, this);
    axisMaxSliderZ->setMinimum(1);
    axisMaxSliderZ->setTickInterval(1);
    axisMaxSliderZ->setEnabled(false);
    axisMaxSliderZ->hide();

    marketModifier->setAxisMinSliderX(axisMinSliderX);
    marketModifier->setAxisMaxSliderX(axisMaxSliderX);
    marketModifier->setAxisMinSliderZ(axisMinSliderZ);
    marketModifier->setAxisMaxSliderZ(axisMaxSliderZ);

    modelModifier->setAxisMinSliderX(axisMinSliderX);
    modelModifier->setAxisMaxSliderX(axisMaxSliderX);
    modelModifier->setAxisMinSliderZ(axisMinSliderZ);
    modelModifier->setAxisMaxSliderZ(axisMaxSliderZ);

    //marketModifier->enableSqrtSinModel(true);
    //marketModifier->setVolatilitySurface();



}

void CalibrationWidget::newModel_oneClick(){
    qDebug()<<"Calibration Widget newModel_oneClick()";

    Array newArray(10);
    newArray[0]=0.05;newArray[1]=0.04;newArray[2]=0.03;
    newArray[3]=0.007;newArray[4]=0.006;newArray[5]=0.005;newArray[6]=0.004;newArray[7]=0.003;newArray[8]=0.002;newArray[9]=0.001;
    temporaryParamsVector.push_back(newArray);
    modelComboBox->addItem(mainPtr_->oneClickWidget->currentTempModelName);

    update();

}

Worker::Worker(MainWindow* mainPtr) { // Constructor
    mainPtr_=mainPtr;// you could copy data from constructor arguments to internal variables here.
}

Worker::~Worker() { // Destructor
    // free resources
}

void Worker::process() { // Process. Start processing data.

    mainPtr_->calibrationWidget->calibrator(1);
    emit finished();

    QString msg;
    msg+="Task No.";
    msg+=QString::number(mainPtr_->taskNum);
        msg+=" ";
    msg+=QDateTime::currentDateTime().toString();
    msg+=" ";
    msg+=" HW Model Calibrate";
    mainPtr_->logText->append(msg);
    mainPtr_->taskNum++;
}
