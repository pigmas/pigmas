#include "smithwilsonwidget.h"
#include "smithwilsonengine.h"
#include "datamanager.h"
#include "mainwindow.h"
#include "delegate.h"

#include <QHBoxLayout>
#include <QTableWidget>
#include <QListWidget>
#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChartView>
#include <QDebug>
#include <QInputDialog>

QT_CHARTS_USE_NAMESPACE

SmithWilsonWidget::SmithWilsonWidget(QWidget *parent)
    : QWidget(parent)
{
    init();
    signalSetting();
}

SmithWilsonWidget::SmithWilsonWidget(MainWindow* mainPtr)
{
    mainPtr_=mainPtr;
    init();
    signalSetting();
}

SmithWilsonWidget::~SmithWilsonWidget()
{

}


// data from sw
// data from gridRate
void SmithWilsonWidget::init(){

    layoutSetting();


    chartView->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    inputRateTable->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Expanding);
    curveList->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Expanding);


}

void SmithWilsonWidget::layoutSetting(){
    inputRateTable = new Spreadsheet(mainPtr_);
    inputRateTable->setItemDelegate(new ItemDelegate);
    curveOptionLayout->addWidget(ufrLabel);
    curveOptionLayout->addWidget(ufrEdit);
    ufrEdit->setText("4.20");
    //curveOptionLayout->addWidget(alphaLabel);
    //curveOptionLayout->addWidget(alphaEdit);
    curveOptionLayout->addWidget(spreadLabel);
    curveOptionLayout->addWidget(spreadEdit);
    spreadEdit->setText("0");
    //curveOptionLayout->addWidget(llpLabel);
    //curveOptionLayout->addWidget(llpEdit);
    //curveOptionLayout->addWidget(convergenceLabel);
    //curveOptionLayout->addWidget(convergenceEdit);
    //curveOptionLayout->addWidget(typeLabel);
    //curveOptionLayout->addWidget(typeComboBox);
    curveOptionLayout->addWidget(generatePush);
    curveOptionLayout->addWidget(savePush);
    curveOptionLayout->addWidget(curveList);

    curveOptionContainer->setLayout(curveOptionLayout);
    curveOptionContainer->setFixedWidth(250);

    typeComboBox->addItem("Forward");
    typeComboBox->addItem("Discount");
    typeComboBox->addItem("Spot Rate");
    chartViewLayout->addLayout(typeLayout);
    typeLayout->addSpacerItem(new QSpacerItem(20,20,QSizePolicy::Expanding));
    typeLayout->addWidget(typeLabel);
    typeLayout->addWidget(typeComboBox);
    chartViewLayout->addWidget(chartView);

    inputLabelLayout->addWidget(inputLabel);
    inputLabel->setAlignment(Qt::AlignRight);
    inputLabelLayout->addWidget(inputComboBox);
    updateInputComboBox();

    curveLayout->addLayout(chartViewLayout);

    inputLayout->addLayout(inputLabelLayout);
    inputLayout->addWidget(inputRateTable);

    mainLayout->addWidget(curveOptionContainer);
    mainLayout->addLayout(inputLayout);
    mainLayout->addLayout(curveLayout);

    this->resize(1300,800);
    this->setLayout(mainLayout);
    voidSetting();
}

void SmithWilsonWidget::signalSetting(){
    connect(generatePush,SIGNAL(clicked()),this,SLOT(on_generatePush_clicked()));
    connect(savePush,SIGNAL(clicked()),this,SLOT(on_savePush_clicked()));
    connect(inputComboBox,SIGNAL(highlighted(int)), this, SLOT(on_inputComboBox_highlighted()));
    connect(inputComboBox,SIGNAL(currentIndexChanged(const QString&)),this,SLOT(on_inputComboBox_changed()));
    connect(typeComboBox,SIGNAL(currentIndexChanged(QString)),this,SLOT(on_typeComboBox_changed()));
    connect(curveList,SIGNAL(itemChanged(QListWidgetItem*)),SLOT(on_curveList_changed()));
}
void SmithWilsonWidget::on_curveList_changed(){
    qDebug()<<"on_curveList_changed() called";

    drawCurve();
}
void SmithWilsonWidget::on_typeComboBox_changed(){
    qDebug()<<"on_typeComboBox_changed() called";
    drawCurve();
}

void SmithWilsonWidget::drawCurve(){
    qDebug()<<"SmithWilsonWidget drawCurve() called";

    QChart *chart = new QChart();

    for(int i=0;i<temporaryCurveItemVector.size();i++){
        qDebug()<<"item"<<i<<"state"<<int(temporaryCurveItemVector.at(i)->checkState());

        if(int(temporaryCurveItemVector.at(i)->checkState()==2)){
            QLineSeries* series = new QLineSeries();
            boost::shared_ptr<MY::YieldTermStructure> rth(new MY::YieldTermStructure);
            rth=temporaryCurveVector.at(i);
            for(int j=0;j<rth->maxTime();j++){
                double t=double(j)/12.0;
                if(typeComboBox->currentIndex()==0){
                    series->append(j,(exp(rth->forwardRate(t,t+1.0/12.0, Continuous, NoFrequency))-1.0)*100.0);
                }else if(typeComboBox->currentIndex()==1){
                    series->append(j,rth->discount(t));
                }else if(typeComboBox->currentIndex()==2){
                    series->append(j,(exp(rth->zeroRate(t,Continuous))-1.0)*100.0);
                }
            }
            series->setName(temporaryCurveItemVector.at(i)->text());
            chart->addSeries(series);

        }

    }
    chartView->setChart(chart);

    chart->createDefaultAxes();
    chart->setTitle("Smith Wilson Extrapolation");

    chartView->setRenderHint(QPainter::Antialiasing);

    mainPtr_->sol2ShockWidget->update();

}

void SmithWilsonWidget::on_generatePush_clicked(){
    qDebug()<<"pushed";
    addTempCurve();
    drawCurve();

    QString msg;
    msg+="Task No.";
    msg+=QString::number(mainPtr_->taskNum);
        msg+=" ";
    msg+=QDateTime::currentDateTime().toString();
    msg+=" Smith Wilson Curve Generated";
    msg+=" UFR:";
    msg+=ufrEdit->text();
    msg+=" Spread:";
    msg+=spreadEdit->text();
    msg+="bp";
    mainPtr_->logText->append(msg);
    mainPtr_->taskNum++;

}
void SmithWilsonWidget::on_savePush_clicked(){
    qDebug()<<"SmithWilsonWidget SavePushclicked";
    int i=curveList->currentIndex().row();
    if(i!=-1){

        QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                            "/home",
                                                            QFileDialog::ShowDirsOnly
                                                            | QFileDialog::DontResolveSymlinks);

        QString fileName=curveList->item(i)->text()+"_Forward.csv";

        std::ofstream file(QString(dir+"/"+fileName).toStdString());

        MY::YieldTermStructure ts=*temporaryCurveVector.at(i);

        Matrix matrix=Matrix(1,ts.maxTime());

        for(int i=0;i<ts.maxTime();i++){
            double t=double(i)/12.0;

                matrix[0][i]=ts.forwardRate(t,t+1.0/12.0, Continuous, NoFrequency);

        }

        for(unsigned int i = 0; i != matrix.rows(); i++)
        {
         std::string stream;
         for(unsigned int j = 0; j != matrix.columns(); j++)
         {
            stream += (std::to_string(matrix[i][j]) + ",");
         }
            file << stream << std::endl;
        }
        // close text file
        file.close();
        QMessageBox msgBox;
        msgBox.setText(QString("File Saved : ")+QString(dir+"/"+fileName));
        msgBox.exec();


    }

}

void SmithWilsonWidget::on_inputComboBox_changed(){
    qDebug()<<"smithWilsonWidget inputComboBox changed called";
    qDebug()<<inputComboBox->currentIndex();

    //qDebug()<<mainPtr_->getMyTree()->getItemVector().at(inputComboBox->currentIndex())->text();
    qDebug()<<mainPtr_->getMyTree()->getInputItemVector().size()<<"changedSize";
    if(inputComboBox->currentIndex()!=-1){
        inputRateTable->readMatrix(mainPtr_->getMyTree()->getInputItemVector().at(inputComboBox->currentIndex())->getDataMatrixPtr());
        //inputRateTable->updateMatrix(mainPtr_->getMyTree()->getInputItemVector().at(inputComboBox->currentIndex())->getDataMatrixPtr());
        generatePush->setEnabled(true);
        savePush->setEnabled(true);
        inputRateTable->setEnabled(true);
        inputRateTable->setRateTable();
        inputRateTable->show();
    }
    if(inputComboBox->currentIndex()==-1){
        voidSetting();

    }


}

void SmithWilsonWidget::on_inputComboBox_highlighted(){
 //
    qDebug()<<"inputComboBox_highlighted";
}
void SmithWilsonWidget::addTempCurve(){
    qDebug()<<"SmithWilsonWidget pass button clicked";
    bool ok;
    QString varName = QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                  tr("Termstructure Name:"), QLineEdit::Normal,
                                  "", &ok);
    //default name
    if(varName==""){
        varName="null";
    }
    SmithWilsonEngine* sw = new SmithWilsonEngine;
    QString ufr = ufrEdit->text();
    double ufr_double=ufr.toDouble();
    double ufr_Rate=log(1+ufr_double/100.0);
    QString shock = spreadEdit->text();
    qDebug()<<double(shock.toInt())<<"double";
    qDebug()<<shock<<"shock";
    double shock_double=double(shock.toInt())/100.0;
    QString alpha = alphaEdit->text();

    if(ufr_Rate!=0 && ufr_Rate<0.45 && ufr_Rate>-0.09){
        sw->setUFR(ufr_Rate);
    }
    if(alpha.toDouble()>0.01 && alpha.toDouble()<0.4){
        sw->setAlpha(alpha.toDouble());
    }
    Matrix* inputRate=mainPtr_->getMyTree()->getInputItemVector().at(inputComboBox->currentIndex())->getDataMatrixPtr();
    Matrix tempRate=Matrix(inputRate->size1(),inputRate->size2());
    for(int i=0;i<inputRate->size1();i++){
        tempRate[i][0]=(*inputRate)[i][0];
        tempRate[i][1]=(*inputRate)[i][1]+shock_double;
    }
    sw->setRate(tempRate);

    boost::shared_ptr<MY::YieldTermStructure> rth(new MY::YieldTermStructure);
    rth->setDiscountMonth(sw->generate());
    currentYieldTermStructure=rth;

   // mainPtr_->getMyTree()->getYieldItemVector().back()->text()
    //qDebug()<<currentYieldTermStructure->discount(3);
    temporaryCurveItemVector.push_back(new QListWidgetItem(varName));
    temporaryCurveVector.push_back(currentYieldTermStructure);
    //mainPtr_->getMyTree()->addYieldItem(varName,currentYieldTermStructure);
    //curveList->addItem(temporaryCurveItemVector.back());
    curveList->addItem(temporaryCurveItemVector.back());
    temporaryCurveItemVector.back()->setFlags(temporaryCurveItemVector.back()->flags()|Qt::ItemIsUserCheckable);
    temporaryCurveItemVector.back()->setCheckState(Qt::Checked);

    ufrEdit->setText(QString::number((exp(sw->getUFR())-1.0)*100.0,'f', 2));
    alphaEdit->setText(QString::number(sw->getAlpha(),'f', 5));


 }
void SmithWilsonWidget::updateInputComboBox(){
    //not use
}
void SmithWilsonWidget::update(){
    qDebug()<<"smithWilsonWidget slot update called";
    inputComboBox->clear();
    qDebug()<<mainPtr_->getMyTree()->getInputItemVector().size()<<"getInputItemVectorSizeCalled";

    for(int i=0;i<mainPtr_->getMyTree()->getInputItemVector().size();i++){
        inputComboBox->addItem(mainPtr_->getMyTree()->getInputItemVector()[i]->text());
    }

//    qDebug()<<mainPtr_->getMyTree()->getInputItemVector().size()<<"updateSize";
    inputComboBox->setCurrentIndex(mainPtr_->getMyTree()->getInputItemVector().size()-1);


}
void SmithWilsonWidget::voidSetting(){
        generatePush->setEnabled(false);
        savePush->setEnabled(false);
        inputRateTable->setEnabled(false);


}
void SmithWilsonWidget::addToProject(){
    qDebug()<<"Smith Wilson Widget addToPrject()";
    qDebug()<<curveList->currentIndex()<<"currentIndex()";
    qDebug()<<curveList->currentIndex().row()<<"curveList->currentIndex().row()";
    if(curveList->currentIndex().row()!=-1){
        mainPtr_->getMyTree()->addYieldItem(temporaryCurveItemVector.at(curveList->currentIndex().row())->text(),
                                            temporaryCurveVector.at(curveList->currentIndex().row()));
        QString msg;
        msg+="Task No.";
        msg+=QString::number(mainPtr_->taskNum);
            msg+=" ";
        msg+=QDateTime::currentDateTime().toString();
        msg+=" ";
        msg+=temporaryCurveItemVector.at(curveList->currentIndex().row())->text();
        msg+=" Termstructure added to Project";
        mainPtr_->logText->append(msg);
        mainPtr_->taskNum++;
    }

}

void SmithWilsonWidget::delTempCurve(){
    qDebug()<<"Smith Wilson Widget delTempCurve()";
    qDebug()<<curveList->currentIndex().row()<<"currentIndex().row()";
    if(curveList->currentIndex().row()!=-1){
    qDebug()<<temporaryCurveVector.size()<<"temporaryCurveVector.size() 1";
    //temporaryCurveItemVector.push_back(varName);
    temporaryCurveItemVector.erase(temporaryCurveItemVector.begin()+curveList->currentIndex().row());
    temporaryCurveVector.erase(temporaryCurveVector.begin()+curveList->currentIndex().row());
    curveList->takeItem(curveList->currentIndex().row());
    }
    //qDebug()<<curveList->currentIndex().row()<<"curveList->currentIndex().row()";
    //qDebug()<<temporaryCurveVector.size()<<"temporaryCurveVector.size() 2";
    //qDebug()<<temporaryCurveItemVector.size()<<"temporaryCurveItemVector.size() 2";


}

void SmithWilsonWidget::addTempCurve_oneClick(){
    SmithWilsonEngine* sw = new SmithWilsonEngine;

    Matrix* inputRate=mainPtr_->getMyTree()->getInputItemVector().back()->getDataMatrixPtr();

    sw->setRate(*inputRate);

    boost::shared_ptr<MY::YieldTermStructure> rth(new MY::YieldTermStructure);
    rth->setDiscountMonth(sw->generate());

    currentYieldTermStructure=rth;


    temporaryCurveItemVector.push_back(new QListWidgetItem(mainPtr_->oneClickWidget->currentTempCurveName));
    temporaryCurveVector.push_back(currentYieldTermStructure);

    curveList->addItem(temporaryCurveItemVector.back());
    temporaryCurveItemVector.back()->setFlags(temporaryCurveItemVector.back()->flags()|Qt::ItemIsUserCheckable);
    temporaryCurveItemVector.back()->setCheckState(Qt::Checked);

    ufrEdit->setText(QString::number((exp(sw->getUFR())-1.0)*100.0,'f', 2));
    alphaEdit->setText(QString::number(sw->getAlpha(),'f', 5));

    QString msg;
    msg+="Task No.";
    msg+=QString::number(mainPtr_->taskNum);
        msg+=" ";
    msg+=QDateTime::currentDateTime().toString();
    msg+=" ";
    msg+=" Smith Wilson Curve Generate";
    mainPtr_->logText->append(msg);
    mainPtr_->taskNum++;


}
