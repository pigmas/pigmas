#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include "boost/math/distributions/normal.hpp"
#include "ql/math/matrix.hpp"
#include "ql/utilities/dataparsers.hpp"
#include <QApplication>
#include <QDebug>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iterator>
#include <QStandardItemModel>

using namespace QuantLib;
using namespace std;

class DataManager
{

public:
    explicit DataManager();
    void qDebug_Matrix(Matrix m);
    Matrix csv_Matrix(ifstream& data);
    Matrix csv_Matrix(ifstream& data,int rownum,int colnum);
    Matrix csv_Matrix(ifstream& data,int rownum,int colnum,int start, int end);
    std::vector<QString> csv_TS_Time(ifstream& data);
    std::vector<double> csv_TS_Data(ifstream& data);
    Matrix vToMatrix(std::vector<double> v);
    void fnc();

    Matrix row(Matrix m,int i);
    Matrix col(Matrix m,int j);

    vector<Date> dateVector;
    Matrix yieldDbTenor;
    vector<Matrix> yieldDB;
    vector<Matrix> volDB;
    void readYieldDB(ifstream& data);
    void readVolDB(ifstream& data);
    void csvWrite(QString fileName, Matrix m);

signals:

public slots:

};

#endif // DATAMANAGER_H
