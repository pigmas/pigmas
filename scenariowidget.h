#ifndef SCENARIOWidget_H
#define SCENARIOWidget_H

#include <QWidget>
#include "mainwindow.h"
#include "spreadsheet.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QGridLayout>
#include <QComboBox>
#include <QPushButton>
#include <QLineEdit>
#include <QListWidget>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChartView>


class MainWindow;
class Spreadsheet;
class QtCharts::QChartView;

class ScenarioWidget : public QWidget
{
    Q_OBJECT

public:
    ScenarioWidget(QWidget *parent = 0);
    ScenarioWidget(MainWindow* mainPtr = 0);


    QWidget* optionContainer = new QWidget;
    QLabel *modelLabel = new QLabel(tr("Model:"));
    QComboBox* modelComboBox = new QComboBox;
    QLabel *aLabel = new QLabel(tr("a:"));
    Spreadsheet *aTable;
    QLabel *sigmaLabel = new QLabel(tr("sigma:"));
    Spreadsheet *sigmaTable;
    QLabel *termLabel = new QLabel(tr("Termstructure:"));
    QComboBox *termComboBox = new QComboBox;
    QLabel *rnLabel = new QLabel(tr("Random Number:"));
    QComboBox *rnComboBox = new QComboBox;
    QPushButton *generatePush = new QPushButton(tr("Generate"));
    QPushButton *savePush = new QPushButton(tr("Save"));
    QLabel *tempScenarioLabel = new QLabel(tr("Temporary Scenario"));
    QListWidget *tempScenarioList = new QListWidget;

    QWidget* chart1Container = new QWidget;
    QWidget* chart2Container = new QWidget;


    QtCharts::QChartView* scenChartView = new QtCharts::QChartView();
    QtCharts::QChartView* martingaleChartView = new QtCharts::QChartView();

    QHBoxLayout* mainLayout = new QHBoxLayout;
    QVBoxLayout* scenChartLayout = new QVBoxLayout;
    QVBoxLayout* martingaleChartLayout = new QVBoxLayout;

    QVBoxLayout* optionLayout = new QVBoxLayout;


    void init();
    void layoutSetting();
    void signalSetting();

    void update();
    void tableUpdate();
    void paramsUpdate();

    void generate();
    void generate_oneClick();
    void addTemp(QString varName,Matrix scen);
    void delTemp();
    void addToProject();
    void scenChartShow(Matrix& scen);
    void buttonCheck();
    Matrix martingale(Matrix& scen);
    Matrix tempDiscount;
    Matrix disc;

private:
    MainWindow* mainPtr_;
    Array* currentParamsPtr_;
    MY::YieldTermStructure ts;
    Matrix* currentRnPtr_;
    vector<Matrix> tempScenVector_;
    Matrix table_a;
    Matrix table_sigma;

private slots:
    void on_modelComboBox_changed();
    void on_generatePush_clicked();
    void on_savePush_clicked();

signals:
    void updated();
};

#endif // CALIBRATIONWIDGET_H
