#ifndef RATECONVERTER_H
#define RATECONVERTER_H
#include "ql/math/matrix.hpp"
using namespace QuantLib;

class RateConverter
{
public:
    RateConverter();

    Matrix contToAnnual(Matrix cont);
    Matrix annualToCont(Matrix ann);

    Matrix contToMonthly(Matrix cont);
    Matrix montlyToCont(Matrix mon);

    Matrix contToQuarterly(Matrix cont);
    Matrix quarterlyToCont(Matrix quart);

    Matrix discountToPar(Matrix discount);
    Matrix parToDiscount(Matrix par);

    Matrix discountToForward(Matrix discount);
    Matrix forwardToDiscount(Matrix forward);

    Matrix discountYear(Matrix discountMonth);
    Matrix discountQuarter(Matrix discountMonth);

};

#endif // RATECONVERTER_H
