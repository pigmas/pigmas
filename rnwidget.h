#ifndef RNWIDGET_H
#define RNWIDGET_H

#include <QWidget>
#include "mainwindow.h"

#include<QVBoxLayout>
#include<QHBoxLayout>
#include<QFormLayout>
#include<QGridLayout>
#include<QComboBox>
#include<QPushButton>
#include<QLineEdit>
#include<QListWidget>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChartView>


class MainWindow;
class QtCharts::QChartView;
using namespace QuantLib;


class RnWidget : public QWidget
{
    Q_OBJECT

public:
    RnWidget(QWidget *parent = 0);
    RnWidget(MainWindow* mainPtr = 0);


    QWidget* optionContainer = new QWidget;
    QLabel *methodLabel = new QLabel(tr("Method:"));
    QComboBox* methodComboBox = new QComboBox;
    QLabel* seedLabel = new QLabel(tr("Seed Number:"));
    QLineEdit *seedEdit = new QLineEdit;
    QLabel *maturityLabel = new QLabel(tr("Month:"));
    QLineEdit *maturityEdit = new QLineEdit;
    QLabel *nLabel = new QLabel(tr("N:"));
    QLineEdit *nEdit = new QLineEdit;
    QLabel *tempListLabel = new QLabel(tr("Temporary RN:"));
    QListWidget *tempList = new QListWidget;

    QLabel *maturityHistLabel = new QLabel(tr("Month Slice Histogram:"));
    QComboBox *maturityHistComboBox = new QComboBox;

    QLabel *nHistLabel = new QLabel(tr("N Slice Histogram:"));
    QComboBox *nHistComboBox = new QComboBox;

    //QWidget* hist1Container = new QWidget;
    //QWidget* hist2Container = new QWidget;
    QtCharts::QChartView* hist1View = new QtCharts::QChartView();
    QtCharts::QChartView* hist2View = new QtCharts::QChartView();

    QPushButton* generatePush = new QPushButton(tr("Generate"));

    QHBoxLayout* mainLayout = new QHBoxLayout;
    QVBoxLayout* optionLayout = new QVBoxLayout;
    QVBoxLayout* histLayout = new QVBoxLayout;
    QHBoxLayout* histOptionLayout = new QHBoxLayout;
    QHBoxLayout* chartLayout = new QHBoxLayout;



    void init();
    void layoutSetting();
    void signalSetting();
    void generate();
    void generate_oneClick();

    void updateChart();

    Matrix hist(Matrix dat, double first, double last, int n);

    void addTemp(QString varName,Matrix rnd);
    void delTemp();
    void addToProject();

private:
    MainWindow* mainPtr_;
    vector<Matrix> tempRnVector_;

private slots:
    void on_generatePush_clicked();
    void on_tempList_Changed(int n);
    void on_Maturity_Changed();
    void on_N_Changed();

signals:
    void updated();
};

#endif // CALIBRATIONWIDGET_H
