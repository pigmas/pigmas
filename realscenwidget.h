#ifndef REALSCENWIDGET_H
#define REALSCENWIDGET_H

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QComboBox>
#include <QPushButton>
#include <QLineEdit>
#include <QListWidget>
#include <QTableView>
#include <QInputDialog>
#include <QSplitter>

#include "realmainwindow.h"
#include "delegate.h"
#include "qcustomplot.h"
#include "dataclass.h"
#include "datamanager.h"

class RealMainWindow;
class ItemDelegate;
class NoEditDelegate;

class RealScenWidget : public QWidget
{
    Q_OBJECT

public:
    RealScenWidget(QWidget *parent = 0);
    ~RealScenWidget();

    QWidget* optionContainer = new QWidget;
    QLabel *modelLabel = new QLabel(tr("Model:"));
    QComboBox* modelComboBox = new QComboBox;
    QLabel *paramsLabel = new QLabel(tr("Params:"));
    QTableView* paramsTable = new QTableView(this);
    QLabel *rnLabel = new QLabel(tr("Random Number:"));
    QComboBox *rnComboBox = new QComboBox;
    QPushButton *generatePush = new QPushButton(tr("Generate"));
    QPushButton *savePush = new QPushButton(tr("Save"));
    QLabel *tempScenarioLabel = new QLabel(tr("Scenario"));
    QListWidget *tempScenarioList = new QListWidget;

    QSplitter* mainSplitter = new QSplitter;
    QWidget* chartContainer = new QWidget;
    QHBoxLayout* chartContainerLayout = new QHBoxLayout;

    QWidget* chart1Container = new QWidget;
    QVBoxLayout* chart1Layout = new QVBoxLayout;

    QCustomPlot* customPlot = new QCustomPlot;
    QHBoxLayout* mainLayout = new QHBoxLayout;
    QVBoxLayout* optionLayout = new QVBoxLayout;

    //paramsTable Declare
    QStandardItemModel* paramsTableModel = new QStandardItemModel(this);
    ItemDelegate* myDelegate;
    NoEditDelegate* noDelegate;

    void init();
    void layoutSetting();
    void signalSetting();

    void updateModelComboBox();
    void updateParamsTable();
    void updateRnComboBox();
    void updateScenList();
    void updateChart();
    void updateAll();

    void generate();
    void addTemp(QString varName,Matrix scen);
    void delTemp();
    void setMainPtr(){ mainPtr_ = qobject_cast<RealMainWindow*>(this->parent()); }

private:
    //std::vector<boost::shared_ptr<DataClass>> tempScenVector_;
    RealMainWindow* mainPtr_;
    DataManager dm;

private slots:
    void on_modelComboBox_changed();
    void on_generatePush_clicked();
    void on_savePush_clicked();
    void on_tempScenarioList_Changed();

signals:
    void updated();
};

#endif // REALSCENWIDGET_H
