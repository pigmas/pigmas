#include "rnwidget.h"
#include "delegate.h"

#include "ql/math/randomnumbers/randomsequencegenerator.hpp"
#include "ql/math/randomnumbers/mt19937uniformrng.hpp"
#include "ql/math/distributions/normaldistribution.hpp"

#include <QSizePolicy>
#include <QInputDialog>
#include <QBarSeries>
#include <QBarSet>
#include <QChart>


using namespace QuantLib;
class GeneralHullWhite;


RnWidget::RnWidget(QWidget *parent)
    : QWidget(parent)
{
    init();
    signalSetting();
}

RnWidget::RnWidget(MainWindow* mainPtr)
{
    mainPtr_=mainPtr;
    init();
    layoutSetting();
    signalSetting();

}

void RnWidget::init(){

    seedEdit->setText("28749");
    maturityEdit->setText("1200");
    nEdit->setText("1000");

}

void RnWidget::signalSetting(){
    connect(generatePush,SIGNAL(clicked(bool)),this,SLOT(on_generatePush_clicked()));
    connect(tempList,SIGNAL(currentRowChanged(int)),this,SLOT(on_tempList_Changed(int)));
    connect(maturityHistComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(on_Maturity_Changed()));
    connect(nHistComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(on_N_Changed()));
}
void RnWidget::on_tempList_Changed(int n){
    disconnect(maturityHistComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(on_Maturity_Changed()));
    disconnect(nHistComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(on_N_Changed()));
    if(n==-1){
        maturityHistComboBox->clear();
        nHistComboBox->clear();
    }
    if(n!=-1){
        int numRow=tempRnVector_.at(n).size1();
        int numCol=tempRnVector_.at(n).size2();
        for(int j=0;j<numCol;j++){
            maturityHistComboBox->addItem(QString::number(j+1));
        }
        for(int i=0;i<numRow;i++){

            nHistComboBox->addItem(QString::number(i+1));
        }

    }
    connect(maturityHistComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(on_Maturity_Changed()));
    connect(nHistComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(on_N_Changed()));
}

void RnWidget::on_Maturity_Changed(){
    if(maturityHistComboBox->currentIndex()!=-1){
        updateChart();
    }
}
void RnWidget::on_N_Changed(){
    if(nHistComboBox->currentIndex()!=-1){
        updateChart();
    }
}

void RnWidget::layoutSetting(){

    optionContainer->setLayout(optionLayout);
    optionContainer->setFixedWidth(300);

    mainLayout->addWidget(optionContainer);
    mainLayout->addLayout(histLayout);
    histLayout->addLayout(histOptionLayout);
    histLayout->addLayout(chartLayout);

    optionLayout->addWidget(methodLabel);
    optionLayout->addWidget(methodComboBox);

    optionLayout->addWidget(seedLabel);
    optionLayout->addWidget(seedEdit);
    optionLayout->addWidget(maturityLabel);
    optionLayout->addWidget(maturityEdit);
    optionLayout->addWidget(nLabel);
    optionLayout->addWidget(nEdit);
    optionLayout->addWidget(generatePush);
    optionLayout->addWidget(tempListLabel);
    optionLayout->addWidget(tempList);

    histOptionLayout->addWidget(maturityHistLabel);
    histOptionLayout->addWidget(maturityHistComboBox);
    histOptionLayout->addWidget(nHistLabel);
    histOptionLayout->addWidget(nHistComboBox);
    chartLayout->addWidget(hist1View);
    chartLayout->addWidget(hist2View);

    methodComboBox->addItem("Mersenne Twister 19937");

    this->setLayout(mainLayout);



}

void RnWidget::on_generatePush_clicked(){
    generate();
}

void RnWidget::generate(){

    bool ok;
    QString varName = QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                  tr("Random Number Name:"), QLineEdit::Normal,
                                  "", &ok);
    if(varName==""){
        varName="null";
    }

    QString seed = seedEdit->text();
    QString numRow = nEdit->text();
    QString numCol = maturityEdit->text();

    qDebug()<<seed.toInt();
    qDebug()<<numRow.toInt();
    qDebug()<<numCol.toInt();

    MersenneTwisterUniformRng uGenerator(seed.toInt());
    Matrix rnd(numRow.toInt(),numCol.toInt());
    InverseCumulativeNormal normInv;

    for (int i=0; i<numRow.toInt();i++){
        for (int j=0; j<numCol.toInt(); ++j){
          rnd[i][j] = normInv(uGenerator.next().value);
        }
    }
    addTemp(varName,rnd);

    QString msg;
    msg+="Task No.";
    msg+=QString::number(mainPtr_->taskNum);
    msg+=" ";
    msg+=QDateTime::currentDateTime().toString();
    msg+=" ";
    msg+=" RN Generated";
    mainPtr_->logText->append(msg);
    mainPtr_->taskNum++;

}

void RnWidget::updateChart(){
    if(tempList->currentRow()!=-1){
        Matrix rnd=tempRnVector_.at(tempList->currentRow());
        int numRow=rnd.size1();
        int numCol=rnd.size2();

        Matrix dat1=Matrix(1,numRow);
        for (int i=0;i<numRow;i++){
            dat1[0][i]=rnd[i][maturityHistComboBox->currentIndex()];
        }

        Matrix histMatrix1 = hist(dat1, -3, 3, 15);


        QBarSet* set1= new QBarSet("New");
        for(int i=0;i<histMatrix1.size2();i++){
            set1->append(histMatrix1[0][i]);
        }
        QBarSeries *series1= new QBarSeries();
        series1->setBarWidth(1);
        set1->setLabel(maturityHistComboBox->currentText()+" Month Random Number Histogram");
        series1->append(set1);

        QChart *chart1 = new QChart();
        chart1->addSeries(series1);
        chart1->createDefaultAxes();

        hist1View->setChart(chart1);
        hist1View->setRenderHint(QPainter::Antialiasing);

        Matrix dat2=Matrix(1,numCol);
        for (int j=0;j<numCol;j++){
            dat2[0][j]=rnd[nHistComboBox->currentIndex()][j];
        }

        Matrix histMatrix2 = hist(dat2, -3, 3, 15);

        QBarSet* set2= new QBarSet("New");
        for(int i=0;i<histMatrix2.size2();i++){
            set2->append(histMatrix2[0][i]);
        }
        QBarSeries *series2= new QBarSeries();
        series2->setBarWidth(1);
        set2->setLabel(nHistComboBox->currentText()+" Scenario Random Number Histogram");
        series2->append(set2);
        QChart *chart2 = new QChart();

        chart2->addSeries(series2);
        chart2->createDefaultAxes();
        hist2View->setChart(chart2);
        hist2View->setRenderHint(QPainter::Antialiasing);


    }

}

void RnWidget::addTemp(QString varName,Matrix rnd){

    tempList->addItem(varName);
    tempRnVector_.push_back(rnd);

}
void RnWidget::delTemp(){
    if(tempList->currentIndex().row()!=-1){
        tempRnVector_.erase(tempRnVector_.begin()+tempList->currentIndex().row());
        tempList->takeItem(tempList->currentIndex().row());
    }
}

void RnWidget::addToProject(){

    int i=tempList->currentIndex().row();
    if(i!=-1){
        mainPtr_->getMyTree()->addRnItem(tempList->item(i)->text(),
                                            tempRnVector_.at(i));
        QString msg;
        msg+="Task No.";
        msg+=QString::number(mainPtr_->taskNum);
        msg+=" ";
        msg+=QDateTime::currentDateTime().toString();
        msg+=" ";
        msg+=" RN added to Project";
        mainPtr_->logText->append(msg);
        mainPtr_->taskNum++;
    }
}

Matrix RnWidget::hist(Matrix dat, double first, double last, int n){

    double step=(last-first)/(n-2.0);
    std::vector<double> hold;

    for(int i=0;i<n-1;i++){
        hold.push_back(first+i*step);
    }
    Matrix nMatrix = Matrix(1,n);

    for(int i=0;i<nMatrix.size2();i++){
        nMatrix[0][i]=0.0;
    }
    for(int i=0;i<dat.size2();i++){

        for(int j=0;j<hold.size();j++){
            if(dat[0][i]<hold[j]){
                nMatrix[0][j]=nMatrix[0][j]+1;
            }

        }
    }
    nMatrix[0][n-1]=dat.size2();
    for(int i=0;i<nMatrix.size2();i++){
        //qDebug()<<nMatrix[0][i];
    }

    Matrix histMatrix = Matrix(1,n);
    histMatrix[0][0]=nMatrix[0][0];

    for(int i=1;i<nMatrix.size2();i++){
        histMatrix[0][i]=nMatrix[0][i]-nMatrix[0][i-1];
        //qDebug()<<histMatrix[0][i];
    }

    return histMatrix;

}

void RnWidget::generate_oneClick(){

    QString varName = mainPtr_->oneClickWidget->currentTempRnName;
    QString seed = seedEdit->text();
    QString numRow = nEdit->text();
    QString numCol = maturityEdit->text();

    qDebug()<<seed.toInt();
    qDebug()<<numRow.toInt();
    qDebug()<<numCol.toInt();

    MersenneTwisterUniformRng uGenerator(seed.toInt());
    Matrix rnd(numRow.toInt(),numCol.toInt());
    InverseCumulativeNormal normInv;

    for (int i=0; i<numRow.toInt();i++){
        for (int j=0; j<numCol.toInt(); ++j){
          rnd[i][j] = normInv(uGenerator.next().value);
        }
    }

    addTemp(varName,rnd);
    tempList->setCurrentRow(tempList->count()-1);
    updateChart();

    QString msg;
    msg+="Task No.";
    msg+=QString::number(mainPtr_->taskNum);
    msg+=" ";
    msg+=QDateTime::currentDateTime().toString();
    msg+=" ";
    msg+=" RN Generate";
    mainPtr_->logText->append(msg);
    mainPtr_->taskNum++;

}
