#ifndef MYTREE_H
#define MYTREE_H
#include "spreadsheet.h"
#include "mytreeitem.h"
#include "mainwindow.h"

#include <QWidget>
#include <QTreeView>
#include <QStandardItemModel>
#include <QItemSelection>
#include <QTimer>

class MainWindow;
class Spreadsheet;

class MyTree : public QWidget
{
    Q_OBJECT



public:
    MyTree(MainWindow* mainPtr);
    ~MyTree();
    void addInputItem(QString varName, Matrix& m);
    void addYieldItem(QString varName, boost::shared_ptr<MY::YieldTermStructure>& ts);
    void addVolItem(QString varName, Matrix& m);
    void addModelItem(QString varName, Matrix& m);
    void addRnItem(QString varName,Matrix& m);
    void addScenItem(QString varName,Matrix& m);
    std::vector<MyTreeItem*> getInputItemVector(){return inputItemVector;}
    std::vector<MyTreeItem*> getYieldItemVector(){return yieldItemVector;}
    std::vector<MyTreeItem*> getVolItemVector(){return volItemVector;}
    std::vector<MyTreeItem*> getModelItemVector(){return modelItemVector;}
    std::vector<MyTreeItem*> getRNItemVector(){return rnItemVector;}
    std::vector<MyTreeItem*> getScenarioItemVector(){return scenarioItemVector;}
    void init();
    void signalSetting();
    void delItem();
    void setMainPtr(MainWindow* mainPtr){mainPtr_=mainPtr;}
    QTreeView *treeView;
    QStandardItemModel *standardModel;
    Spreadsheet *spreadsheet;
    QStandardItem *rootNode;
    QStandardItem *input;
    QStandardItem *yield;
    QStandardItem *vol;
    QStandardItem *model;
    QStandardItem *randomnumber;
    QStandardItem *scenario;
    std::vector<MyTreeItem*> inputItemVector;
    std::vector<MyTreeItem*> yieldItemVector;
    std::vector<MyTreeItem*> volItemVector;
    std::vector<MyTreeItem*> modelItemVector;
    std::vector<MyTreeItem*> rnItemVector;
    std::vector<MyTreeItem*> scenarioItemVector;
    MainWindow* mainPtr_;

protected:
private:


private slots:
    void on_treeView_doubleClicked(const QModelIndex& index);
signals:
    void updated();
};

#endif // TREE_H
