#ifndef NULLIBOR_H
#define NULLIBOR_H
#include "ql/indexes/iborindex.hpp"

using namespace QuantLib;

class NullIbor : public IborIndex
{
public:
    NullIbor(const Handle<QuantLib::YieldTermStructure>& h = Handle<QuantLib::YieldTermStructure>());
};

#endif // NULLIBOR_H
