#ifndef CALIBRATIONWIDGET_H
#define CALIBRATIONWIDGET_H

#include <QWidget>
#include "mainwindow.h"
#include "spreadsheet.h"
#include "surfacegraph.h"
#include "generalhullwhite.h"
#include "yieldtermstructure.h"
#include "bargraph.h"

#include<QVBoxLayout>
#include<QHBoxLayout>
#include<QFormLayout>

#include<QGridLayout>
#include<QComboBox>
#include<QPushButton>
#include<QProgressBar>

#include<QtDataVisualization/Q3DSurface>
#include<QtDataVisualization/QSurface3DSeries>
#include<QtDataVisualization/Q3DBars>

class MainWindow;
class Spreadsheet;
class GeneralHullWhite;
class Worker;
class QProgressBar;

using namespace QtDataVisualization;

class CalibrationWidget : public QWidget
{
    Q_OBJECT

public:
    CalibrationWidget(QWidget *parent = 0);
    CalibrationWidget(MainWindow* mainPtr = 0);

    Q3DSurface* marketGraph = new Q3DSurface();
    Q3DSurface* modelGraph = new Q3DSurface();
    SurfaceGraph *marketModifier = new SurfaceGraph(marketGraph);
    SurfaceGraph *modelModifier = new SurfaceGraph(modelGraph);
    QWidget* marketContainer = QWidget::createWindowContainer(marketGraph);
    QWidget* modelContainer = QWidget::createWindowContainer(modelGraph);
    QVBoxLayout* marketContainerLayout = new QVBoxLayout;
    QVBoxLayout* modelContainerLayout = new QVBoxLayout;
    QLabel* marketContainerLabel = new QLabel(tr("Market Swaption Price"));
    QLabel* modelContainerLabel = new QLabel(tr("Model Swaption Price"));



    Q3DBars *barGraph = new Q3DBars();
    BarGraph *barModifier = new BarGraph(barGraph);
    QWidget *relativeContainer = QWidget::createWindowContainer(barGraph);

    QWidget* chartContainer = new QWidget;
    QWidget* formContainer = new QWidget;

    QLabel *inputVolLabel = new QLabel(tr("Input Volatility:"));
    QComboBox* inputVolComboBox = new QComboBox;

    QLabel *termstructureLabel = new QLabel(tr("Termstructure:"));
    QComboBox* termstructureComboBox = new QComboBox;
    QLabel *aLabel = new QLabel("a:");
    QLabel *sigmaLabel = new QLabel("sigma:");
    QLabel *modelLabel = new QLabel(tr("Model:"));
    QComboBox* modelComboBox = new QComboBox;

    Spreadsheet* paramsTable1;
    Spreadsheet* paramsTable2;
    Spreadsheet* marketVolTable;
    Spreadsheet* modelVolTable;

    QPushButton* newModelPush = new QPushButton(tr("New Model"));
    QPushButton* delModelPush = new QPushButton(tr("Remove Model"));
    QPushButton* marketPricePush = new QPushButton(tr("Market Price"));
    QPushButton* modelPricePush = new QPushButton(tr("Model Price"));
    QPushButton* calibratePush = new QPushButton(tr("Calibrate"));
    QPushButton* addToProjectPush = new QPushButton(tr("Add to project"));

    QHBoxLayout* mainLayout = new QHBoxLayout;
    QVBoxLayout* configLayout = new QVBoxLayout;
    QVBoxLayout* optionLayout = new QVBoxLayout;
    //QVBoxLayout* compareLayout = new QVBoxLayout;

    QVBoxLayout* volLayout = new QVBoxLayout;
    QHBoxLayout* cubeLayout = new QHBoxLayout;
    QHBoxLayout* tableLayout = new QHBoxLayout;

    QThread* thread;
    Worker* worker;
    QProgressBar* progressBar = new QProgressBar;

    void init();
    void layoutSetting();
    void signalSetting();
    void setMainPtr(MainWindow* mainPtr){mainPtr_=mainPtr;}
    void update();
    void paramsUpdate();
    void tableUpdate();
    void voidSetting();
    void enableSetting();
    void calibrator(int mode);
    void buttonCheck();
    void marketGraphUpdate(int mode);
    void modelGraphUpdate();
    void newModel_oneClick();
    void calibrate_oneCliclk();

    Matrix* currentMarketPricePtr_{new Matrix(6,6)};
    Matrix* currentModelPricePtr_{new Matrix(6,6)};

private:
    MainWindow* mainPtr_;
    Matrix* currentMarketVolPtr_{new Matrix(6,6)};


//    vector<boost::shared_ptr<GeneralHullWhite>> temporaryModelVector;
    vector<Array> temporaryParamsVector;

    Matrix tempParams1_;
    Matrix tempParams2_;
    MY::YieldTermStructure ts;
    //Array paramsArray;





private slots:
        void on_modelComboBox_changed();
        void on_inputVolComboBox_changed();

        void on_newModelPush_clicked();
        void on_delModelPush_clicked();
        void on_calibratePush_clicked();
        void on_modelPricePush_clicked();
        void on_marketPricePush_clicked();
        void on_addToProjectPush_clicked();

        void afterWorkerFinished();

signals:
    void updated();
};

class Worker : public QObject {
    Q_OBJECT
public:
    Worker(MainWindow* mainPtr);
    ~Worker();
public slots:
    void process();
signals:
    void finished();

private:
    MainWindow* mainPtr_;
    // add your variables here
};

#endif // CALIBRATIONWIDGET_H
