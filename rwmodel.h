#ifndef RWMODEL_H
#define RWMODEL_H

#include <ql/math/matrix.hpp>
#include <vector>
#include <QString>
#include <QObject>

class RwModel
{

public:
    RwModel();

    virtual std::vector<double> calibrate(QuantLib::Matrix indexData){ return {0}; }
    virtual std::vector<double> calibrate(){ return {0}; }
    virtual QuantLib::Matrix scenario(QuantLib::Matrix& rnd){ return rnd; }

    void setParams(std::vector<double> params){ params_ = params; }
    void setParam(int index, double param){ if(params_.size()>index){params_[index]=param;} }
    void setData(QuantLib::Matrix data){ data_ = data; }
    std::vector<double> getParams(){ return params_; }
    std::vector<QString> getParamsName(){ return paramsName_; }

    int modelIndex;
    std::string modelName;
    std::vector<std::string> modelType { "IR_Vasicek", "IR_CIR" , "EQ_LN", "EQ_RSLN2" };


private:

protected:
    QuantLib::Matrix data_;
    std::vector<double> params_;
    std::vector<QString> paramsName_;

    //Model Code 0 : Vasicek
    // params : alpha, theta, sigma
    //Model Code 1 : CIR
    // params : alpha, theta, sigma
    //Model Code 2 : LN
    // params : mu, sigma
    //Model Code 3 : RSLN
    // params : mu1, mu2, sigma1, sigma2, p12, p21


};

#endif // RWMODEL_H
