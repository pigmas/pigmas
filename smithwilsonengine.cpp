#include "smithwilsonengine.h"
#include <QDebug>
#define myqDebug() qDebug()<<qSetRealNumberPrecision(20)
using namespace std;

SmithWilsonEngine::SmithWilsonEngine()
{

    tenor_ = Matrix(1,13);
    gridRate_ = Matrix(1,13);
    alpha_ = 0.12;
    UFR_ = log(1.042);

    tenor_[0][0]=0.25;   gridRate_[0][0]=1.340;
    tenor_[0][1]=0.50;   gridRate_[0][1]=1.447;
    tenor_[0][2]=0.75;   gridRate_[0][2]=1.536;
    tenor_[0][3]=1;      gridRate_[0][3]=1.569;
    tenor_[0][4]=1.5;    gridRate_[0][4]=1.630;
    tenor_[0][5]=2;      gridRate_[0][5]=1.638;
    tenor_[0][6]=2.5;    gridRate_[0][6]=1.658;
    tenor_[0][7]=3;      gridRate_[0][7]=1.660;
    tenor_[0][8]=5;      gridRate_[0][8]=1.820;
    tenor_[0][9]=7;      gridRate_[0][9]=2.011;
    tenor_[0][10]=10;    gridRate_[0][10]=2.112;
    tenor_[0][11]=15;    gridRate_[0][11]=2.167;
    tenor_[0][12]=20;    gridRate_[0][12]=2.169;




}

Matrix SmithWilsonEngine::generate()
{
    Matrix mu = Matrix(tenor_.size2(),1);
    Matrix p = Matrix(tenor_.size2(),1);
    Matrix zeta = Matrix(tenor_.size2(),1);
    int num_Month = 1440;
    Matrix discount_Month = Matrix(1,num_Month);
    Matrix wilsonMatrix = Matrix(tenor_.size2(),tenor_.size2());

    for(int i=0;i<tenor_.size2();i++){
        for(int j=0;j<tenor_.size2();j++){

            wilsonMatrix[i][j]=exp(-UFR_*(tenor_[0][i]+tenor_[0][j]))
                                *(alpha_*min(tenor_[0][i],tenor_[0][j])
                                -0.5*exp(-alpha_*max(tenor_[0][i],tenor_[0][j]))
                                *(exp(alpha_*min(tenor_[0][i],tenor_[0][j]))
                                -exp(-alpha_*min(tenor_[0][i],tenor_[0][j]))));


        }
    }

    for(int i=0;i<mu.size1();i++){
        mu[i][0]=exp(-UFR_*tenor_[0][i]);
    }

    for(int i=0;i<p.size1();i++){
        p[i][0]=exp(-gridRate_[0][i]/100*tenor_[0][i]);
    }

    zeta=inverse(wilsonMatrix)*(p-mu);

    Matrix wilsonTargetMatrix=Matrix(num_Month,tenor_.size2());

    double month=1;

    for(int i=0;i<num_Month;i++){
        for(int j=0;j<tenor_.size2();j++){

            wilsonTargetMatrix[i][j]=exp(-UFR_*((month/12)+tenor_[0][j]))
                                *(alpha_*min((month/12),tenor_[0][j])
                                -0.5*exp(-alpha_*max((month/12),tenor_[0][j]))
                                *(exp(alpha_*min((month/12),tenor_[0][j]))
                                -exp(-alpha_*min((month/12),tenor_[0][j]))));


        }
        month++;
    }

    month=1;

    for(int j=0;j<num_Month;j++){
        discount_Month[0][j]=exp(-UFR_*month/12)+(wilsonTargetMatrix*zeta)[j][0];
        month++;
    }

    //dm.qDebug_Matrix(discount_Month);

    return discount_Month;

}

void SmithWilsonEngine::setRate(Matrix rateTable){
    tenor_=Matrix(1,rateTable.size1());
    gridRate_=Matrix(1,rateTable.size1());

    for(int i=0;i<rateTable.size1();i++){
            tenor_[0][i]=rateTable[i][0];   gridRate_[0][i]=rateTable[i][1];
    }
}
