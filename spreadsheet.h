#ifndef SPREADSHEET_H
#define SPREADSHEET_H

#include <QTableWidget>
#include "boost/math/distributions/normal.hpp"
#include "ql/math/matrix.hpp"
#include "mainwindow.h"
#include "delegate.h"

class QStyledItemDelegate;
class Cell;
class MainWindow;
class SpreadsheetCompare;
using namespace QuantLib;
using namespace std;

class Spreadsheet : public QTableWidget
{
    Q_OBJECT
public:
    Spreadsheet(QWidget *parent = 0);
    Spreadsheet(MainWindow* mainPtr);

    bool autoRecalculate() const { return autoRecalc;}
    QString currentLocation() const;
    QString currentFormula() const;
    QTableWidgetSelectionRange selectedRange() const;

    void clear();
    bool readFile(const QString &fileName);
    bool readMatrix(Matrix* m);
    bool updateMatrix(Matrix* m);
    bool writeFile(const QString &fileName);
    void sort(const SpreadsheetCompare &compare);
    void setRateTable();
    void setParamsTable();
    void setVolTable();
    void setScenParamsTable();
    void setMainPtr(MainWindow* mainPtr){mainPtr_=mainPtr;}
    void signalSetting();
    void createYieldTable(vector<Matrix>& yieldVector, vector<Date>& dateVector, Matrix& yieldDbTenor);
    void createVolTable(vector<Matrix> volVector, vector<Date> dateVector);

public slots:
    void cut();
    void copy();
    void paste();
    void del();
    void selectCurrentColumn();
    void selectCurrentRow();
    void recalculate();
    void setAutoRecalculate(bool recalc);
    void findNext(const QString &str, Qt::CaseSensitivity cs);
    void findPrevious(const QString &str, Qt::CaseSensitivity cs);

private slots:
    void somethingChanged();

signals:
    void modified();

private:
    enum {MagicNumber = 0x7FCAB3,   RowCount = 999, ColumnCount = 26};
    Cell *cell(int row, int column) const;
    QString text(int row, int column) const;
    QString formula(int row, int column) const;
    void setFormula(int row, int column, const QString &formula);
    bool autoRecalc;
    MainWindow* mainPtr_;
};

class SpreadsheetCompare {
public:
    bool operator()(const QStringList &row1, const QStringList &row2) const;
    enum {KeyCount = 3};
    int keys[KeyCount];
    bool ascending[KeyCount];

};

#endif // SPREADSHEET_H
