#ifndef SOL2SHOCKWIDGET_H
#define SOL2SHOCKWIDGET_H

#include <QHBoxLayout>
#include <QTableWidget>
#include <QListWidget>
#include <QComboBox>
#include <QSpinBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChartView>
#include <QWidget>
#include <ql/math/matrix.hpp>

#include "yieldtermstructure.h"
#include "spreadsheet.h"
#include "mytree.h"
#include "mainwindow.h"

QT_CHARTS_USE_NAMESPACE
class MainWindow;
class Spreadsheet;

//class MyTree;

using namespace QuantLib;
class Sol2ShockWidget : public QWidget
{
    Q_OBJECT

public:
    Sol2ShockWidget(QWidget *parent = 0);
    Sol2ShockWidget(MainWindow* mainPtr_);
    ~Sol2ShockWidget();
    //layout
    QHBoxLayout* mainLayout = new QHBoxLayout;
    QVBoxLayout* inputLayout = new QVBoxLayout;
    QVBoxLayout* curveLayout = new QVBoxLayout;

    QVBoxLayout* curveOptionLayout = new QVBoxLayout;
    QVBoxLayout* chartViewLayout = new QVBoxLayout;
    QHBoxLayout* inputLabelLayout = new QHBoxLayout;

    QHBoxLayout* typeLayout = new QHBoxLayout;

    QWidget* curveOptionContainer = new QWidget;
    QLabel *inputLabel = new QLabel("Input Rate:");
    QLabel *sol2Label = new QLabel("Solvency2 Shock Table");

    QComboBox* tempComboBox = new QComboBox;

    Spreadsheet* sol2Table;
    Spreadsheet* tempRateTable;


    QLabel* typeLabel = new QLabel("Curve Type:");

    QComboBox* typeComboBox = new QComboBox;



    QChartView* chartView = new QChartView();
    boost::shared_ptr<MY::YieldTermStructure> currentYieldTermStructure;
    vector<boost::shared_ptr<MY::YieldTermStructure>> temporaryCurveVector;
    vector<QListWidgetItem*> temporaryCurveItemVector;


    void setData(Matrix* mPtr){dataMat.push_back(mPtr);}
    void setMainPtr(MainWindow* mainPtr){mainPtr_=mainPtr;}
    void init();
    void layoutSetting();
    void signalSetting();
    void updateTempComboBox();
    void update();
    void voidSetting();
    void drawCurve();

\
private:
    std::vector<Matrix*> dataMat;
    MainWindow* mainPtr_;

private slots:

        void on_tempComboBox_changed();


signals:
    void updated();
};

#endif // SOL2SHOCKWIDGET_H
