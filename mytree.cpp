#include "mytree.h"
#include <QHBoxLayout>
#include <QDebug>
#include <QtCore>
#include "delegate.h"

MyTree::MyTree(MainWindow* mainPtr) : QWidget(NULL){
    mainPtr_=mainPtr;

    init();
}



MyTree::~MyTree(){

}

void MyTree::init(){

    treeView = new QTreeView(this);
    this->setStyleSheet("QTreeView { font-size: " + QString::number(10) + "pt; }");
    standardModel = new QStandardItemModel;
    standardModel->setHorizontalHeaderLabels(QStringList("Project"));
    rootNode = standardModel->invisibleRootItem();


    //defining a couple of items
    input = new QStandardItem("Input Rate");
    yield =  new QStandardItem("Yield Curve");
    vol = new QStandardItem("Volatility");
    model = new QStandardItem("Models");
    randomnumber = new QStandardItem("RN");
    scenario = new QStandardItem("Scenarios");

    //QStandardItem *termItem =  new QStandardItem("Term Structure");
    //QStandardItem *interpolatedCurveItem =   new QStandardItem("Interpolated Curve");
    //QStandardItem *swaptionItem =     new QStandardItem("Swaption");
    //QStandardItem *HW = new QStandardItem("HW");
    //QStandardItem *rnd1 = new QStandardItem("rnd1");
    //QStandardItem *lat = new QStandardItem("LAT2017_500");

    //building up the hierarchy
    rootNode-> appendRow(input);
    rootNode-> appendRow(yield);
    rootNode-> appendRow(vol);
    rootNode-> appendRow(model);
    rootNode-> appendRow(randomnumber);
    rootNode-> appendRow(scenario);

    //inputItem-> appendRow(termItem);
    //inputItem-> appendRow(swaptionItem);
    //yieldItem->  appendRow(interpolatedCurveItem);
    //Models->appendRow(HW);
    //RN->appendRow(rnd1);
    //scenarios->appendRow(lat);

    //register the model
    treeView->setModel(standardModel);
    treeView->expandAll();
    QHBoxLayout *treeView_Layout = new QHBoxLayout;
    treeView_Layout->addWidget(treeView);
    treeView_Layout->setMargin(0);
    setLayout(treeView_Layout);

    treeView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    signalSetting();

}

void MyTree::signalSetting(){
    connect(treeView,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(on_treeView_doubleClicked(QModelIndex)));
    connect(this,SIGNAL(updated()),mainPtr_,SLOT(updateWidgets()));
}

void MyTree::on_treeView_doubleClicked(const QModelIndex& index) {
    qDebug() << index.parent().row()<<index.row()<<index.column();
    //pop up Spreadsheet
    //initialize Spreadsheet
    int tableHeight = 0;
    int tableWidth = 0;


    qDebug()<<"height"<<tableHeight<<"width"<<tableWidth;


    if(index.parent().row()!=-1){
            spreadsheet = new Spreadsheet(mainPtr_);
            spreadsheet->setItemDelegate(new ItemDelegate);
        if(index.parent().row()==0){
            Matrix* m=inputItemVector[index.row()]->getDataMatrixPtr();
            //qDebug()<<m;
            spreadsheet->readMatrix(m);
            for (int i = 0; i < spreadsheet->rowCount(); i++)
              tableHeight += spreadsheet->rowHeight(i);
            for (int j = 0; j < spreadsheet->columnCount(); j++)
              tableWidth += spreadsheet->columnWidth(j);
            spreadsheet->resize(tableWidth+30,tableHeight+30);
            spreadsheet->show();

        }else if(index.parent().row()==2){
            Matrix* m=volItemVector[index.row()]->getDataMatrixPtr();
            spreadsheet->readMatrix(m);
            for (int i = 0; i < spreadsheet->rowCount(); i++)
              tableHeight += spreadsheet->rowHeight(i);
            for (int j = 0; j < spreadsheet->columnCount(); j++)
              tableWidth += spreadsheet->columnWidth(j);
            spreadsheet->resize(tableWidth+30,tableHeight+30);
            spreadsheet->show();
        }else if(index.parent().row()==3){
            Matrix* m=modelItemVector[index.row()]->getDataMatrixPtr();
            spreadsheet->readMatrix(m);
            for (int i = 0; i < spreadsheet->rowCount(); i++)
              tableHeight += spreadsheet->rowHeight(i);
            for (int j = 0; j < spreadsheet->columnCount(); j++)
              tableWidth += spreadsheet->columnWidth(j);
            spreadsheet->resize(tableWidth+30,tableHeight+30);
            spreadsheet->show();
        }else if(index.parent().row()==4){
            Matrix* m=rnItemVector[index.row()]->getDataMatrixPtr();
            spreadsheet->readMatrix(m);
            spreadsheet->resize(1200,1000);
            spreadsheet->show();
        }

    }
}



void MyTree::addInputItem(QString varName,Matrix& m){
    qDebug()<<"MyTree addInputItemCalled";
    inputItemVector.push_back(new MyTreeItem(varName));
    //Matrix mtest(1,2);
    //mtest[0][0]=3;
    //mtest[0][1]=4;
    //mti->setDataMatrix(mtest);
    inputItemVector.back()->setDataMatrix(m);
    input->appendRow(inputItemVector.back());
    //Matrix mmm=mti->getDataMatrix();
    //qDebug()<<mmm.size1();
    //qDebug()<<mmm.size2();
    //spreadsheet->readMatrix(m);
    //spreadsheet->show();


}

void MyTree::addYieldItem(QString varName, boost::shared_ptr<MY::YieldTermStructure>& ts){

    qDebug()<<"MyTree addYieldItemCalled";
    yieldItemVector.push_back(new MyTreeItem(varName));
    yieldItemVector.back()->setYieldTermStructure(ts);
    yield->appendRow(yieldItemVector.back());
    emit(updated());

}

void MyTree::addVolItem(QString varName,Matrix& m){
    qDebug()<<"MyTree addInputItemCalled";
    volItemVector.push_back(new MyTreeItem(varName));
    volItemVector.back()->setDataMatrix(m);
    vol->appendRow(volItemVector.back());
    //Matrix mmm=mti->getDataMatrix();
    //qDebug()<<mmm.size1();
    //qDebug()<<mmm.size2();
    //spreadsheet->readMatrix(m);
    //spreadsheet->show();
    emit(updated());

}
void MyTree::addModelItem(QString varName,Matrix& m){

    qDebug()<<"MyTree addRnItemCalled";
    modelItemVector.push_back(new MyTreeItem(varName));
    modelItemVector.back()->setDataMatrix(m);
    model->appendRow(modelItemVector.back());
    emit(updated());

}
void MyTree::addRnItem(QString varName,Matrix& m){

    qDebug()<<"MyTree addRnItemCalled";
    rnItemVector.push_back(new MyTreeItem(varName));
    rnItemVector.back()->setDataMatrix(m);
    randomnumber->appendRow(rnItemVector.back());
    emit(updated());

}
void MyTree::addScenItem(QString varName,Matrix& m){

    qDebug()<<"MyTree addScenItemCalled";
    scenarioItemVector.push_back(new MyTreeItem(varName));
    scenarioItemVector.back()->setDataMatrix(m);
    scenario->appendRow(scenarioItemVector.back());
    emit(updated());

}


void MyTree::delItem(){
    qDebug()<<"MyTree delItem called";
    qDebug()<<"current Index"<<treeView->currentIndex();
    qDebug()<<"parent()"<<treeView->currentIndex().parent().row();
    qDebug()<<"row()"<<treeView->currentIndex().row();
    qDebug()<<"column"<<treeView->currentIndex().column();
    int parent=treeView->currentIndex().parent().row();
    int child=treeView->currentIndex().row();

//    std::vector<int> ii{10,20,30,40,50};
//    qDebug()<<ii[0]<<ii[1]<<ii[2]<<ii[3]<<ii[4];//10 20 30 40 50
//    ii.erase(ii.begin()+1);
//    qDebug()<<ii[0]<<ii[1]<<ii[2]<<ii[3];//10 30 40 50

    switch(parent){
        case -1:
            qDebug()<<"parent case -1 called";
            break;
        case 0:
            qDebug()<<"parent case 0 called";
            inputItemVector.erase(inputItemVector.begin()+child);
            input->removeRow(child);
            break;
        case 1:
            qDebug()<<"parent case 1 called";
            yieldItemVector.erase(yieldItemVector.begin()+child);
            yield->removeRow(child);
            break;
        case 2:
            qDebug()<<"parent case 2 called";
            volItemVector.erase(volItemVector.begin()+child);
            vol->removeRow(child);
            break;
        case 3:
            qDebug()<<"parent case 3 called";
            modelItemVector.erase(modelItemVector.begin()+child);
            model->removeRow(child);
            break;
        case 4:
            qDebug()<<"parent case 4 called";
            rnItemVector.erase(rnItemVector.begin()+child);
            randomnumber->removeRow(child);
            break;
        case 5:
            qDebug()<<"parent case 5 called";
            scenarioItemVector.erase(scenarioItemVector.begin()+child);
            scenario->removeRow(child);
            break;
    }
    emit(updated());
}
