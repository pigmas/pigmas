#include "rwmodel.h"

namespace MY {
    class Vasicek : public RwModel{

    public:

        Vasicek();

        virtual std::vector<double> calibrate(QuantLib::Matrix indexData);
        virtual std::vector<double> calibrate(){ return this->calibrate(this->data_); }
        virtual QuantLib::Matrix scenario(QuantLib::Matrix& rnd);

    private:

    };

}
