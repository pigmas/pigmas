#ifndef BASE_H
#define BASE_H

#include <QObject>
#include <QWidget>
#include <QTextEdit>
#include <QHBoxLayout>
#include <QSplitter>
#include <QLabel>
#include <QTabWidget>
#include <QStackedWidget>

class Base : public QWidget
{
public:
    Base();
};

#endif // MAINWINDOW_H
