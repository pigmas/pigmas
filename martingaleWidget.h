#ifndef MARTINGALEWIDGET_H
#define MARTINGALEWIDGET_H

#include <QWidget>
#include "mainwindow.h"
#include "spreadsheet.h"
#include "surfacegraph.h"
#include "bargraph.h"
#include "ql/math/matrix.hpp"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QComboBox>

#include <QtCharts/QLineSeries>
#include <QtCharts/QChartView>

#include<QtDataVisualization/Q3DSurface>
#include<QtDataVisualization/QSurface3DSeries>
#include<QtDataVisualization/Q3DBars>

class MainWindow;
class Spreadsheet;
class QtCharts::QChartView;


using namespace QuantLib;
using namespace QtDataVisualization;

class MartingaleWidget : public QWidget
{
    Q_OBJECT

public:
    MartingaleWidget(QWidget *parent = 0);
    MartingaleWidget(MainWindow* mainPtr = 0);

    QtCharts::QChartView* martingaleView = new QtCharts::QChartView();
    Spreadsheet* martingaleTable;
    QWidget* martingaleTableContainer=new QWidget;
    QHBoxLayout* tableLayout = new QHBoxLayout;
    QHBoxLayout* mainLayout = new QHBoxLayout;

    void init();
    void layoutSetting();
    void signalSetting();
    void generate();
    void chartUpdate();


private:
    MainWindow* mainPtr_;
    //vector<Matrix> tempRnVector_;

private slots:

signals:
    void updated();
};

#endif // CALIBRATIONWIDGET_H
