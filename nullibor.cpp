#include "nullibor.h"
#include "ql/currencies/asia.hpp"
#include "ql/time/calendars/nullcalendar.hpp"
#include "ql/time/businessdayconvention.hpp"
#include "ql/time/daycounters/simpledaycounter.hpp"

NullIbor::NullIbor(const Handle<YieldTermStructure>& h)
: IborIndex("NullIbor", //family Name
            Period(3,Months), //tenor
            0, // settlement days
            KRWCurrency(), //currency
            NullCalendar(), //fixing calendar
            BusinessDayConvention::Unadjusted, //business day convention
            false, //endOfMonth
            SimpleDayCounter(), //daycounter
            h) //termstructure
{
    QL_REQUIRE(this->tenor().units()!=Days,
               "for daily tenors (" << this->tenor() <<
               ") dedicated DailyTenor constructor must be used");
}
