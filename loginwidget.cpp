#include "loginwidget.h"
#include "ui_login.h"

#include <QMessageBox>

LoginWidget::LoginWidget(QWidget *parent) : QWidget(parent)
{
    ui= new Ui::LoginWidget;
    ui->setupUi(this);

}

void LoginWidget::on_loginButton_clicked()
{
    QString username = ui->lineEdit->text();
    QString password = ui->lineEdit_2->text();

    if(username == "kidi"&& password=="kidi"){
        this->hide();
        selectWorld();
    }else{
        QMessageBox::warning(this,"Login","Username and password is not correct");
    }
}

void LoginWidget::selectWorld(){
    qDebug()<<" LoginWidget::selectWorld() Called";
    QDialog* worldSelectDialog = new QDialog(this);
    QVBoxLayout* mainLayout = new QVBoxLayout;
    QWidget* listContainer = new QWidget;
    QVBoxLayout* listLayout = new QVBoxLayout;
    QLabel* worldListLabel = new QLabel(tr("Select Item:"));
    QListWidget* worldList = new QListWidget;
    worldList->addItem(tr("Risk Neutral"));
    worldList->addItem(tr("Real World"));
    worldList->setStyleSheet("QListWidget { font-size: " + QString::number(13) + "pt; }");
    worldList->setCurrentRow(0);
    QWidget* buttonBox = new QWidget;
    QHBoxLayout* buttonBoxLayout = new QHBoxLayout;
    QPushButton* okButton = new QPushButton(tr("Ok"));
    QPushButton* cancleButton = new QPushButton(tr("Cancle"));
    QSpacerItem* hSpacer = new QSpacerItem(1,1,QSizePolicy::Expanding,QSizePolicy::Minimum);
    buttonBox->setLayout(buttonBoxLayout);
    buttonBoxLayout->addSpacerItem(hSpacer);
    buttonBoxLayout->addWidget(okButton);
    buttonBoxLayout->addWidget(cancleButton);
    connect(okButton, SIGNAL(clicked(bool)), worldSelectDialog, SLOT(accept()));
    connect(cancleButton, SIGNAL(clicked(bool)), worldSelectDialog, SLOT(reject()));

    worldSelectDialog->setLayout(mainLayout);
    listContainer->setLayout(listLayout);
    listLayout->addWidget(worldListLabel);
    listLayout->addWidget(worldList);

    mainLayout->addWidget(listContainer);
    mainLayout->addWidget(buttonBox);

    int result = worldSelectDialog->exec();
    if(result==1){
        if(worldList->currentRow()==0){
            mainWindow = new MainWindow(this);
            mainWindow->showMaximized();
        }else if(worldList->currentRow()==1){
            realMain = new RealMainWindow;
            realMain->showMaximized();
            connect(realMain,SIGNAL(realMainSignal()),this,SLOT(selectWorld()));
        }
    }else{
        mainWindow=0;
        realMain=0;
    }
    qDebug()<<" LoginWidget::selectWorld() End";
    delete worldSelectDialog;
}

LoginWidget::~LoginWidget(){
    qDebug()<<"# LoginWidget Destructor Called";
    qDebug()<<" RealMainWindow Ptr : "<<realMain;
    delete mainWindow;
    delete realMain;
    delete ui;
}
