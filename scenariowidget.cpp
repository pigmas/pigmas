#include "ScenarioWidget.h"
#include "nullibor.h"
#include "delegate.h"

#include "ql/quotes/simplequote.hpp"
#include "ql/termstructures/yield/flatforward.hpp"
#include "ql/models/shortrate/calibrationhelpers/swaptionhelper.hpp"
#include "ql/pricingengines/swaption/jamshidianswaptionengine.hpp"
#include "ql/math/optimization/levenbergmarquardt.hpp"
#include "ql/math/array.hpp"

#include "delegate.h"
#include <QSizePolicy>
#include <QInputDialog>
#include <QSplitter>


using namespace QuantLib;
class GeneralHullWhite;


ScenarioWidget::ScenarioWidget(QWidget *parent)
    : QWidget(parent)
{
    init();
    signalSetting();
}

ScenarioWidget::ScenarioWidget(MainWindow* mainPtr)
{
    mainPtr_=mainPtr;
    init();


}

void ScenarioWidget::init(){

    table_a=Matrix(1,3);
    table_sigma=Matrix(1,7);
    buttonCheck();
    layoutSetting();
    signalSetting();

}

void ScenarioWidget::signalSetting(){
    connect(modelComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(on_modelComboBox_changed()));
    connect(generatePush,SIGNAL(clicked(bool)),this,SLOT(on_generatePush_clicked()));
    connect(savePush,SIGNAL(clicked(bool)),this,SLOT(on_savePush_clicked()));
}

void ScenarioWidget::update(){
    qDebug()<<"Scenario Widget update() called";
    if(mainPtr_->getMyTree()->getModelItemVector().size()!=0){
        paramsUpdate();
    }

    int nModel=mainPtr_->getMyTree()->getModelItemVector().size();
    int nTerm=mainPtr_->getMyTree()->getYieldItemVector().size();
    int nRn=mainPtr_->getMyTree()->getRNItemVector().size();

        modelComboBox->clear();
        termComboBox->clear();
        rnComboBox->clear();

        for(int i=0;i<nModel;i++){
            modelComboBox->addItem(mainPtr_->getMyTree()->getModelItemVector()[i]->text());
        }
        modelComboBox->setCurrentIndex(mainPtr_->getMyTree()->getModelItemVector().size()-1);

        for(int i=0;i<nTerm;i++){
            termComboBox->addItem(mainPtr_->getMyTree()->getYieldItemVector()[i]->text());
        }
        termComboBox->setCurrentIndex(mainPtr_->getMyTree()->getYieldItemVector().size()-1);

        for(int i=0;i<nRn;i++){
            rnComboBox->addItem(mainPtr_->getMyTree()->getRNItemVector()[i]->text());
        }
        rnComboBox->setCurrentIndex(mainPtr_->getMyTree()->getRNItemVector().size()-1);

     buttonCheck();
}


void ScenarioWidget::layoutSetting(){

    aTable = new Spreadsheet(mainPtr_);
    aTable->setScenParamsTable();
    aTable->setItemDelegate(new ItemDelegate);

    sigmaTable = new Spreadsheet(mainPtr_);
    sigmaTable->setScenParamsTable();
    sigmaTable->setItemDelegate(new ItemDelegate);
    sigmaTable->setStyleSheet("font-size: " + QString::number(8.5) + "pt;");

    optionContainer->setLayout(optionLayout);
    optionContainer->setFixedWidth(350);

    QWidget* chartContainer = new QWidget;
    QHBoxLayout* chartContainerLayout = new QHBoxLayout;
    chartContainerLayout->addWidget(chart1Container);
    chartContainerLayout->addWidget(chart2Container);
    chartContainer->setLayout(chartContainerLayout);

    //mainLayout->addWidget(optionContainer);
    //mainLayout->addWidget(chart1Container);
    //mainLayout->addWidget(chart2Container);
    QSplitter* mainSplitter = new QSplitter;
    mainSplitter->addWidget(optionContainer);
    mainSplitter->addWidget(chartContainer);
    mainLayout->addWidget(mainSplitter);


    chart1Container->setLayout(scenChartLayout);
    chart2Container->setLayout(martingaleChartLayout);
    scenChartLayout->addWidget(scenChartView);
    martingaleChartLayout->addWidget(martingaleChartView);
    scenChartView->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    optionLayout->addWidget(modelLabel);
    optionLayout->addWidget(modelComboBox);
    optionLayout->addWidget(aLabel);
    optionLayout->addWidget(aTable);
    optionLayout->addWidget(sigmaLabel);
    optionLayout->addWidget(sigmaTable);
    optionLayout->addWidget(termLabel);
    optionLayout->addWidget(termComboBox);
    optionLayout->addWidget(rnLabel);
    optionLayout->addWidget(rnComboBox);
    optionLayout->addWidget(generatePush);
    optionLayout->addWidget(savePush);
    optionLayout->addWidget(tempScenarioLabel);
    optionLayout->addWidget(tempScenarioList);

    this->setLayout(mainLayout);


}

void ScenarioWidget::on_modelComboBox_changed(){
    qDebug()<<"ScenarioWidget modelComboBox changed called";
    qDebug()<<modelComboBox->currentIndex()<<"current index";

    int i=modelComboBox->currentIndex();
    if(i==-1){

    }
    if(i!=-1){
        tableUpdate();
    }

}
void ScenarioWidget::tableUpdate(){
    qDebug()<<"Scenario Widget table update called";
    Matrix* tempParam=mainPtr_->getMyTree()->getModelItemVector().at(modelComboBox->currentIndex())->getDataMatrixPtr();
    table_a[0][0]=(*tempParam)[0][0];
    table_a[0][1]=(*tempParam)[0][1];
    table_a[0][2]=(*tempParam)[0][2];

    table_sigma[0][0]=(*tempParam)[0][3];
    table_sigma[0][1]=(*tempParam)[0][4];
    table_sigma[0][2]=(*tempParam)[0][5];
    table_sigma[0][3]=(*tempParam)[0][6];
    table_sigma[0][4]=(*tempParam)[0][7];
    table_sigma[0][5]=(*tempParam)[0][8];
    table_sigma[0][6]=(*tempParam)[0][9];

    aTable->readMatrix(&table_a);
    sigmaTable->readMatrix(&table_sigma);

    QStringList aHeaderLabels;
    aHeaderLabels<<"0~10"<<"10~20"<<"20~";
    aTable->setHorizontalHeaderLabels(aHeaderLabels);

    QStringList sigmaHeaderLabels;
    sigmaHeaderLabels<<"0~1"<<"1~2"<<"2~3"<<"3~5"<<"5~7"<<"7~10"<<"10~";
    sigmaTable->setHorizontalHeaderLabels(sigmaHeaderLabels);


}
void ScenarioWidget::paramsUpdate(){
    qDebug()<<"Scenario Widget params update called";
    int i=modelComboBox->currentIndex();

    if(i!=-1){
        Matrix tempParam(1,10);
        tempParam[0][0]=table_a[0][0];
        tempParam[0][1]=table_a[0][1];
        tempParam[0][2]=table_a[0][2];

        tempParam[0][3]=table_sigma[0][0];
        tempParam[0][4]=table_sigma[0][1];
        tempParam[0][5]=table_sigma[0][2];
        tempParam[0][6]=table_sigma[0][3];
        tempParam[0][7]=table_sigma[0][4];
        tempParam[0][8]=table_sigma[0][5];
        tempParam[0][9]=table_sigma[0][6];

        mainPtr_->getMyTree()->getModelItemVector().at(i)->setDataMatrix(tempParam);

    }
}

void ScenarioWidget::on_generatePush_clicked(){
    qDebug()<<"Scenario Widget generatePush clicked";
    generate();
}

void ScenarioWidget::generate(){

    bool ok;
    QString varName = QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                  tr("Scenario Name:"), QLineEdit::Normal,
                                  "", &ok);
    if(varName==""){
        varName="null";
    }


    int termIndex=termComboBox->currentIndex();
    int modelIndex=modelComboBox->currentIndex();
    int rnIndex=rnComboBox->currentIndex();

    ts=mainPtr_->getMyTree()->getYieldItemVector().at(termIndex)->getTermStructure();
    Matrix* tempParam=mainPtr_->getMyTree()->getModelItemVector().at(modelIndex)->getDataMatrixPtr();
    Matrix* rnd=mainPtr_->getMyTree()->getRNItemVector().at(rnIndex)->getDataMatrixPtr();

    Array param(10);

    param[0]=(*tempParam)[0][0];
    param[1]=(*tempParam)[0][1];
    param[2]=(*tempParam)[0][2];
    param[3]=(*tempParam)[0][3];
    param[4]=(*tempParam)[0][4];
    param[5]=(*tempParam)[0][5];
    param[6]=(*tempParam)[0][6];
    param[7]=(*tempParam)[0][7];
    param[8]=(*tempParam)[0][8];
    param[9]=(*tempParam)[0][9];

    Matrix dm_=ts.getDiscountMonth();
    boost::shared_ptr<MY::YieldTermStructure> rth(new MY::YieldTermStructure);
    rth->setDiscountMonth(dm_);
    Handle<QuantLib::YieldTermStructure> currentTermHandle(rth);

    boost::shared_ptr<GeneralHullWhite> modelGHWscenario(new GeneralHullWhite(currentTermHandle));
    modelGHWscenario->setParams(param);
    Matrix scen=modelGHWscenario->scenario(*rnd);
    qDebug()<<scen[0][0]<<scen[0][1]<<scen[0][2]<<scen[0][3]<<scen[0][4];
    qDebug()<<scen[1][0]<<scen[1][1]<<scen[1][2]<<scen[1][3]<<scen[1][4];

    addTemp(varName,scen);
    scenChartShow(scen);

    QString msg;
    msg+="Task No.";
    msg+=QString::number(mainPtr_->taskNum);
    msg+=" ";
    msg+=QDateTime::currentDateTime().toString();
    msg+=" ";
    msg+=" Scenario Genearate";
    mainPtr_->logText->append(msg);
    mainPtr_->taskNum++;

}

void ScenarioWidget::addTemp(QString varName,Matrix scen){

    tempScenarioList->addItem(varName);
    tempScenVector_.push_back(scen);

}
void ScenarioWidget::delTemp(){
    if(tempScenarioList->currentIndex().row()!=-1){
        tempScenVector_.erase(tempScenVector_.begin()+tempScenarioList->currentIndex().row());
        tempScenarioList->takeItem(tempScenarioList->currentIndex().row());
    }
}

void ScenarioWidget::addToProject(){

    int i=tempScenarioList->currentIndex().row();
    if(i!=-1){
        mainPtr_->getMyTree()->addScenItem(tempScenarioList->item(i)->text(),
                                               tempScenVector_.at(i));
        QString msg;
        msg+="Task No.";
        msg+=QString::number(mainPtr_->taskNum);
        msg+=" ";
        msg+=QDateTime::currentDateTime().toString();
        msg+=" ";
        msg+=" Scenario added to Project";
        mainPtr_->logText->append(msg);
        mainPtr_->taskNum++;
    }
}
void ScenarioWidget::buttonCheck(){
    int termIndex=termComboBox->currentIndex();
    int modelIndex=modelComboBox->currentIndex();
    int rnIndex=rnComboBox->currentIndex();

    if(termIndex==-1 || modelIndex==-1 || rnIndex==-1){
        generatePush->setEnabled(false);
        savePush->setEnabled(false);
    }else{
        generatePush->setEnabled(true);
        savePush->setEnabled(true);
    }
}
void ScenarioWidget::scenChartShow(Matrix& scen){

    int numRow=scen.size1();
    int numCol=scen.size2();
    int lowerIndex=int(numRow*0.05)-1;
    int upperIndex=int(numRow*0.95);

    disc=martingale(scen);

    vector<double> lowerScen;
    vector<double> upperScen;


    for(int j=0;j<numCol;j++){
        vector<double> temp;

        for(int i=0;i<numRow;i++){
            temp.push_back(scen[i][j]);
        }
        std::sort(temp.begin(),temp.end());
        lowerScen.push_back(temp[upperIndex]);
        upperScen.push_back(temp[lowerIndex]);
    }

    ts=mainPtr_->getMyTree()->getYieldItemVector().at(termComboBox->currentIndex())->getTermStructure();

    QChart *chart1 = new QChart();

    QLineSeries* lowerSeries = new QLineSeries();
    for(int j=0;j<numCol;j++){
        lowerSeries->append(j,lowerScen[j]);
    }
    chart1->addSeries(lowerSeries);

    QLineSeries* upperSeries = new QLineSeries();
    for(int j=0;j<numCol;j++){
        upperSeries->append(j,upperScen[j]);
    }
    chart1->addSeries(upperSeries);

//    vector<QLineSeries*> serieses;
//        for(int i=0;i<numRow;i++){
//            QLineSeries* series = new QLineSeries();
//            for(int j=0;j<numCol;j++){
//                series->append(j,scen[i][j]);
//            }
//            serieses.push_back(series);
//            chart1->addSeries(serieses.back());
//        }

    QLineSeries* baseCurve = new QLineSeries();
        for(int i=0;i<numCol;i++){
            double t=double(i)/12.0;

                baseCurve->append(i,ts.forwardRate(t,t+1.0/12.0, Continuous, NoFrequency));

        }
    chart1->addSeries(baseCurve);
    scenChartView->setChart(chart1);
    scenChartView->setRenderHint(QPainter::Antialiasing);
    chart1->legend()->hide();
    chart1->createDefaultAxes();
    chart1->setTitle("Forward Rate Scenario 95, 5 Percentile");

    QChart *chart2 = new QChart();

    QLineSeries* scenDisc = new QLineSeries();
    for(int i=0;i<numCol;i++){
        scenDisc->append(i,disc[0][i]);
        //qDebug()<<disc[0][i]<<i<<"scendisc";
    }
    chart2->addSeries(scenDisc);

    QLineSeries* baseDisc = new QLineSeries();
        for(int i=0;i<numCol;i++){
            double t=double(i)/12.0;
                baseDisc->append(i,ts.discount(t));
                //qDebug()<<ts.discount(t)<<i<<"discount";
        }
    chart2->addSeries(baseDisc);
    martingaleChartView->setChart(chart2);
    martingaleChartView->setRenderHint(QPainter::Antialiasing);
    chart2->legend()->hide();
    chart2->createDefaultAxes();
    chart2->setTitle("Martingale Test");

    mainPtr_->martingaleWidget->chartUpdate();
}

void ScenarioWidget::on_savePush_clicked(){
    qDebug()<<"Scenario Widget savePush clicked";
    //Save


    // scenario
    int i=tempScenarioList->currentIndex().row();
    if(i!=-1){

        QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                            "/home",
                                                            QFileDialog::ShowDirsOnly
                                                            | QFileDialog::DontResolveSymlinks);

        QString fileName=tempScenarioList->item(i)->text()+"_Scen.csv";

        std::ofstream file(QString(dir+"/"+fileName).toStdString());
        Matrix matrix=tempScenVector_.at(i);
        for(unsigned int i = 0; i != matrix.rows(); i++)
        {
         std::string stream;
         for(unsigned int j = 0; j != matrix.columns(); j++)
         {
            stream += (std::to_string(matrix[i][j]) + ",");
         }
            file << stream << std::endl;
        }
        // close text file
        file.close();
        QMessageBox msgBox;
        msgBox.setText(QString("File Saved : ")+QString(dir+"/"+fileName));
        msgBox.exec();

        QString msg;
        msg+="Task No.";
        msg+=QString::number(mainPtr_->taskNum);
        msg+=" ";
        msg+=QDateTime::currentDateTime().toString();
        msg+=" ";
        msg+=" Scenario Saved";
        mainPtr_->logText->append(msg);
        mainPtr_->taskNum++;


    }




}
Matrix ScenarioWidget::martingale(Matrix& scen){
    int numRow=scen.size1();
    int numCol=scen.size2();


    Matrix tempAcc(numRow,numCol);
    tempDiscount=Matrix(numRow,numCol);

    for(int i=0;i<numRow;i++){
        tempDiscount[i][0]=1.0;
        tempAcc[i][0]=scen[i][0];
        for(int j=0;j<numCol-1;j++){
            int front=j+1;
            tempAcc[i][front]=tempAcc[i][j]+scen[i][front];
            tempDiscount[i][front]=std::exp(-tempAcc[i][j]/12.0);
        }
    }
    Matrix disc(1,numCol);
    for(int j=0;j<numCol;j++){
        double temp=0.0;
        for(int i=0;i<numRow;i++){
            temp=temp+tempDiscount[i][j];
        }
        disc[0][j]=temp/double(numRow);
        //qDebug()<<j<<disc[0][j]<<"disc test";
    }
    qDebug()<<"martingale debug";
    qDebug()<<tempAcc[0][0]<<tempAcc[0][1]<<tempAcc[0][2]<<tempAcc[0][3];
    qDebug()<<disc[0][0]<<disc[0][1]<<disc[0][2]<<disc[0][3];
    return disc;
}


void ScenarioWidget::generate_oneClick(){

    QString varName=mainPtr_->oneClickWidget->currentTempScenName;

    int termIndex=termComboBox->currentIndex();
    int modelIndex=modelComboBox->currentIndex();
    int rnIndex=rnComboBox->currentIndex();

    ts=mainPtr_->getMyTree()->getYieldItemVector().at(termIndex)->getTermStructure();
    Matrix* tempParam=mainPtr_->getMyTree()->getModelItemVector().at(modelIndex)->getDataMatrixPtr();
    Matrix* rnd=mainPtr_->getMyTree()->getRNItemVector().at(rnIndex)->getDataMatrixPtr();

    Array param(10);

    param[0]=(*tempParam)[0][0];
    param[1]=(*tempParam)[0][1];
    param[2]=(*tempParam)[0][2];
    param[3]=(*tempParam)[0][3];
    param[4]=(*tempParam)[0][4];
    param[5]=(*tempParam)[0][5];
    param[6]=(*tempParam)[0][6];
    param[7]=(*tempParam)[0][7];
    param[8]=(*tempParam)[0][8];
    param[9]=(*tempParam)[0][9];

    Matrix dm_=ts.getDiscountMonth();
    boost::shared_ptr<MY::YieldTermStructure> rth(new MY::YieldTermStructure);
    rth->setDiscountMonth(dm_);
    Handle<QuantLib::YieldTermStructure> currentTermHandle(rth);

    boost::shared_ptr<GeneralHullWhite> modelGHWscenario(new GeneralHullWhite(currentTermHandle));
    modelGHWscenario->setParams(param);
    Matrix scen=modelGHWscenario->scenario(*rnd);
    qDebug()<<scen[0][0]<<scen[0][1]<<scen[0][2]<<scen[0][3]<<scen[0][4];
    qDebug()<<scen[1][0]<<scen[1][1]<<scen[1][2]<<scen[1][3]<<scen[1][4];

    addTemp(varName,scen);
    scenChartShow(scen);

    QString msg;
    msg+="Task No.";
    msg+=QString::number(mainPtr_->taskNum);
    msg+=" ";
    msg+=QDateTime::currentDateTime().toString();
    msg+=" ";
    msg+=" Scenario Genearate";
    mainPtr_->logText->append(msg);
    mainPtr_->taskNum++;
}
