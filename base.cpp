#include "base.h"
#include "fancytabbar.h"
#include "mytree.h"

Base::Base()
{
    FancyTabBar *tabbar = new FancyTabBar;
    tabbar->addFancyTab(QIcon(":/new/prefix1/img/information.svg"), "Information");
    tabbar->addFancyTab(QIcon(":/new/prefix1/img/network.svg"), "Network");
    tabbar->addFancyTab(QIcon(":/new/prefix1/img/settings.svg"), "Settings");
    tabbar->setActiveIndex(0);

    MyTree *mytree = new MyTree;

    QTabWidget *tabslot1 = new QTabWidget;
    QTabWidget *tabslot2 = new QTabWidget;
    QTabWidget *tabslot3 = new QTabWidget;
    QLabel *tab1_1 = new QLabel;
    QLabel *tab1_2 = new QLabel;
    QLabel *tab2_1 = new QLabel;
    QLabel *tab3_1 = new QLabel;

    tabslot1->addTab(tab1_1,tr("Tab1_1"));
    tabslot1->addTab(tab1_2,tr("Tab1_2"));
    tabslot2->addTab(tab2_1,tr("Tab2_1"));
    tabslot3->addTab(tab3_1,tr("Tab3_1"));

    QStackedWidget *stack = new QStackedWidget;
    stack->addWidget(tabslot1);
    stack->addWidget(tabslot2);
    stack->addWidget(tabslot3);

    QTextEdit *editor4 = new QTextEdit;


    QSplitter *split2_34 = new QSplitter;
    QSplitter *split3_4 = new QSplitter;

    connect(tabbar,SIGNAL(activeIndexChanged(qint32)),
            stack,SLOT(setCurrentIndex(int)));

    QHBoxLayout *layout = new QHBoxLayout;


    split2_34->addWidget(mytree);


    split3_4->setOrientation(Qt::Vertical);
    split3_4->addWidget(stack);
    split3_4->addWidget(editor4);
    split3_4->setSizes({700,100});
    split3_4->setStretchFactor(0,1);

    split2_34->addWidget(split3_4);
    split2_34->setSizes({150,700});
    split2_34->setStretchFactor(1,1); // 앞에 1이 두번째 창을 의미, 뒤에 1이 스트레치 한다는것을 의미


    layout->addWidget(tabbar);
    layout->addWidget(split2_34);

    setLayout(layout);
}
