#include "rwmodel.h"
#include <optimization.h>
#include <iostream>
#include <boost/math/distributions/normal.hpp>
#include <ql/math/randomnumbers/mt19937uniformrng.hpp>
#include <QDebug>

using namespace alglib;

class RSLN2 : public RwModel{

public:

    RSLN2();

    virtual std::vector<double> calibrate(QuantLib::Matrix indexData);
    virtual std::vector<double> calibrate(){ return this->calibrate(this->data_); }
    virtual QuantLib::Matrix scenario(QuantLib::Matrix& rnd);
	double MLE(QuantLib::Matrix indexData);
    double MLE(){ return this->MLE(this->data_); }


private:



};
