#include "realcalibrationwidget.h"
#include <QTemporaryDir>
#include <algorithm>

RealCalibrationWidget::RealCalibrationWidget(QWidget *parent)
    :QWidget(parent)
{
    qDebug()<<"# RealCalibrationWidget Constructor called";

    // 0. Declare
    dm = boost::make_shared<DataManager>();
    setMainPtr();

    // 1. Layout
    this->setLayout(mainLayout);
    mainLayout->addWidget(calibrationContainer);
    mainLayout->addWidget(dataTable);
    mainLayout->addWidget(customPlot);
    myDelegate = new ItemDelegate(this);
    noDelegate = new NoEditDelegate(this);

    calibrationContainer->setLayout(calibrationLayout);
    calibrationLayout->addWidget(newModelPush);
    calibrationLayout->addWidget(modelsLabel);
    calibrationLayout->addWidget(modelsCombo);
    calibrationLayout->addWidget(paramsLabel);
    calibrationLayout->addWidget(paramsTable);
    calibrationLayout->addWidget(targetLabel);
    calibrationLayout->addWidget(targetCombo);
    calibrationLayout->addWidget(lastDateLabel);
    calibrationLayout->addWidget(lastDateCombo);
    calibrationLayout->addWidget(nDataLabel);
    calibrationLayout->addWidget(nDataCombo);
    calibrationLayout->addWidget(calibratePush);
    calibrationLayout->addWidget(debugPush);
    calibrationLayout->addSpacerItem(vSpacer);

    // 2. Signals
    connect(newModelPush,SIGNAL(clicked(bool)),this,SLOT(on_newModelPush_clicked()));
    connect(modelsCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(on_modelsCombo_Changed()));
    connect(targetCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(on_targetCombo_Changed()));
    connect(lastDateCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(on_lastDateCombo_Changed()));
    connect(nDataCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(on_nDataCombo_Changed()));
    connect(calibratePush,SIGNAL(clicked(bool)),this,SLOT(on_calibratePush_clicked()));
    connect(debugPush,SIGNAL(clicked(bool)),this,SLOT(on_debugPush_clicked()));

    // 3. Implementation
        //Params Table
    paramsTable->setItemDelegateForColumn(0,noDelegate);
    paramsTable->setItemDelegateForColumn(1,myDelegate);
    paramsTable->setModel(paramsTableModel);
    myDelegate->setMainPtr(mainPtr_);
        //Target Combo

        //Last Date Combo

        //N Data Combo
    nDataCombo->blockSignals(1);
    nDataCombo->addItem(tr("All"));
    nDataCombo->addItem(tr("120 : 10 Year"));
    nDataCombo->addItem(tr("60 : 5 Year"));
    nDataCombo->addItem(tr("36 : 3 Year"));
    nDataCombo->addItem(tr("Can't Calibrate"));

    nDataCombo->setCurrentIndex(4);
    QStandardItemModel* comboModel = qobject_cast<QStandardItemModel*>(nDataCombo->model());
    comboModel->item(0)->setFlags(comboModel->item(0)->flags() & ~Qt::ItemIsEnabled);
    comboModel->item(1)->setFlags(comboModel->item(1)->flags() & ~Qt::ItemIsEnabled);
    comboModel->item(2)->setFlags(comboModel->item(2)->flags() & ~Qt::ItemIsEnabled);
    comboModel->item(3)->setFlags(comboModel->item(3)->flags() & ~Qt::ItemIsEnabled);
    nDataCombo->blockSignals(0);

        //Data Table
    QStringList headerLabels;
    headerLabels.append("Time");
    headerLabels.append("Data");
    dataTableModel->setHorizontalHeaderLabels(headerLabels);
    dataTable->setModel(dataTableModel);
    dataTable->setEditTriggers(QAbstractItemView::NoEditTriggers);

    // 4. Size Policy
        //CalibrationContainer
    calibrationContainer->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Expanding);

        //ParamsTable
    paramsTable->horizontalHeader()->hide();
    paramsTable->verticalHeader()->hide();
    paramsTable->horizontalHeader()->setStretchLastSection(1);
    paramsTable->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Expanding);
    paramsTable->setFixedHeight(2);

        //DataTable
    dataTable->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Expanding);
    dataTable->verticalHeader()->hide();
    dataTable->horizontalHeader()->setStretchLastSection(1);
    dataTable->setMinimumWidth(300);

        //CustomPlot
    customPlot->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    customPlot->setMinimumWidth(400);
    customPlot->plotLayout()->insertRow(0);
    QCPTextElement *noTitle = new QCPTextElement(customPlot,QString(""));
    customPlot->plotLayout()->addElement(0, 0, noTitle);

    // 5. Test
    debugPush->setVisible(false);
}

void RealCalibrationWidget::on_newModelPush_clicked(){
    qDebug()<<"# RealCalibrationWidget::on_newModelPush_clicked() Called";
    modelsCombo->blockSignals(1);
    //Dialog Main
    QDialog* dialog = new QDialog(this);
    QVBoxLayout* layout = new QVBoxLayout;
    QLabel* modelTypeLabel = new QLabel(tr("Model Type:"));
    QComboBox* modelTypeCombo = new QComboBox;
    QLabel* modelNameLabel = new QLabel(tr("Name:"));
    QLineEdit* modelNameEdit = new QLineEdit("model");

    //Button Box
    QWidget* buttonBox = new QWidget;
    QHBoxLayout* buttonBoxLayout = new QHBoxLayout;
    QPushButton* okButton = new QPushButton(tr("Ok"));
    QPushButton* cancleButton = new QPushButton(tr("Cancle"));
    buttonBox->setLayout(buttonBoxLayout);
    buttonBoxLayout->addWidget(okButton);
    buttonBoxLayout->addWidget(cancleButton);
    connect(okButton, SIGNAL(clicked(bool)), dialog, SLOT(accept()));
    connect(cancleButton, SIGNAL(clicked(bool)), dialog, SLOT(reject()));

    //Combo Box
    RwModel realModel;
    int size = realModel.modelType.size();
    for(int i=0;i<size;i++){
        modelTypeCombo->addItem(QString::fromStdString(realModel.modelType[i]));
    }

    //Layout
    layout->addWidget(modelTypeLabel);
    layout->addWidget(modelTypeCombo);
    layout->addWidget(modelNameLabel);
    layout->addWidget(modelNameEdit);
    layout->addWidget(buttonBox);
    dialog->setLayout(layout);

    int result = dialog->exec(); // Ok = 1 , Cancle = 0

    if(result==1){
        qDebug()<<"OK";
        int id = modelTypeCombo->currentIndex();
        switch(id){
        case 0:
            mainPtr_->modelDB_.push_back(boost::make_shared<MY::Vasicek>());
            break;
        case 1:
            mainPtr_->modelDB_.push_back(boost::make_shared<CIR>());
            break;
        case 2:
            mainPtr_->modelDB_.push_back(boost::make_shared<LN>());
            break;
        case 3:
            mainPtr_->modelDB_.push_back(boost::make_shared<RSLN2>());
            break;
        }
        QString modelCode = modelTypeCombo->currentText() +" : "+ modelNameEdit->text();
        mainPtr_->modelDB_.back()->modelName = modelCode.toStdString();
        updateModelsCombo();
        updateParamsTable();
        mainPtr_->updateTree();
        mainPtr_->scenWidget->updateModelComboBox();
        mainPtr_->scenWidget->updateParamsTable();
    }else{
        qDebug()<<"Cancled";
    }
    delete dialog;
    modelsCombo->blockSignals(0);
}

void RealCalibrationWidget::on_modelsCombo_Changed(){
    qDebug()<<"# RealCalibrationWidget::on_modelsCombo_Changed() Called";
    //qDebug()<<" modelsCombo Current Index : "<<modelsCombo->currentIndex();
    updateParamsTable();
}

void RealCalibrationWidget::on_targetCombo_Changed(){
    qDebug()<<"# RealCalibrationWidget::on_targetCombo_Changed() Called";
    //qDebug()<<" targetCombo Current Index : "<<targetCombo->currentIndex();

    updateLastDateCombo();
    updateNDataCombo();
    updateDataTable();
    updateChart();
    updateSelected();

}

void RealCalibrationWidget::on_lastDateCombo_Changed(){
    qDebug()<<"# RealCalibrationWidget::on_lastDateCombo_Changed() Called";
    //qDebug()<<" lastDateCombo Current Index : "<<lastDateCombo->currentIndex();
    updateNDataCombo();
    updateSelected();

}

void RealCalibrationWidget::on_nDataCombo_Changed(){
    qDebug()<<"# RealCalibrationWidget::on_nDataCombo_Changed() Called";
    //qDebug()<<" nDataCombo Current Index : "<<nDataCombo->currentIndex();
    updateSelected();
}

void RealCalibrationWidget::on_calibratePush_clicked(){
    qDebug()<<"# RealCalibrationWidget::on_calibratePush_clicked() Called";
    //qDebug()<<" modelCombo Current Index : "<<modelsCombo->currentIndex();

    modelCalibration();
    updateParamsTable();
    mainPtr_->scenWidget->updateParamsTable();
}

void RealCalibrationWidget::on_debugPush_clicked(){
    qDebug()<<"# RealCalibrationWidget::on_debugPush_clicked() Called";

}

void RealCalibrationWidget::updateModelsCombo(){
    qDebug()<<"# RealCalibrationWidget::updateModelCombo() Called";
    modelsCombo->blockSignals(1);
    modelsCombo->clear();
    for(int i=0;i<mainPtr_->modelDB().size();i++){
        modelsCombo->addItem(QString::fromStdString(mainPtr_->modelDB().at(i)->modelName));
    }
    modelsCombo->blockSignals(0);

}

void RealCalibrationWidget::updateTargetCombo(){
    qDebug()<<"# RealCalibrationWidget::updateTargetCombo() Called";
    //qDebug()<<" targetCombo Current Index : "<<targetCombo->currentIndex();
    targetCombo->blockSignals(1);
    targetCombo->clear();
    for(int i=0;i<mainPtr_->marketDataDB().size();i++){
        targetCombo->addItem(mainPtr_->marketDataDB().at(i)->name);
    }
    targetCombo->blockSignals(0);

}
void RealCalibrationWidget::updateLastDateCombo(){
    qDebug()<<"# RealCalibrationWidget::updateLastDateCombo() Called";
    //qDebug()<<" targetCombo Current Index : "<<targetCombo->currentIndex();

    lastDateCombo->blockSignals(1);
    lastDateCombo->clear();
    int currentIndex = targetCombo->currentIndex();
    if(currentIndex!=-1){
        int numRow = mainPtr_->marketDataDB().at(currentIndex)->timeset.size();
        for(int i=0;i<numRow;i++){
            lastDateCombo->addItem(mainPtr_->marketDataDB().at(currentIndex)->timeset.at(numRow-i-1));
        }
    }
    lastDateCombo->blockSignals(0);
}

void RealCalibrationWidget::updateNDataCombo(){
    qDebug()<<"# RealCalibrationWidget::updateNDataCombo() Called";
    //qDebug()<<" targetCombo Current Index : "<<targetCombo->currentIndex();
    nDataCombo->blockSignals(1);
    int currentIndex = targetCombo->currentIndex();
    int lastDateIndex = lastDateCombo->currentIndex();

    QStandardItemModel* comboModel = qobject_cast<QStandardItemModel*>(nDataCombo->model());
    comboModel->item(0)->setFlags(comboModel->item(0)->flags() & ~Qt::ItemIsEnabled);
    comboModel->item(1)->setFlags(comboModel->item(1)->flags() & ~Qt::ItemIsEnabled);
    comboModel->item(2)->setFlags(comboModel->item(2)->flags() & ~Qt::ItemIsEnabled);
    comboModel->item(3)->setFlags(comboModel->item(3)->flags() & ~Qt::ItemIsEnabled);
    comboModel->item(4)->setFlags(comboModel->item(4)->flags() | Qt::ItemIsEnabled);
    nDataCombo->setCurrentIndex(4);

    if(currentIndex!=-1 && lastDateIndex!=-1){
        int numRow = mainPtr_->marketDataDB().at(currentIndex)->timeset.size()-lastDateIndex;
        if(numRow<36){
            nDataCombo->setCurrentIndex(4);
        }else if(numRow>=36 && numRow<60){
            comboModel->item(0)->setFlags(comboModel->item(0)->flags() | Qt::ItemIsEnabled);
            comboModel->item(3)->setFlags(comboModel->item(3)->flags() | Qt::ItemIsEnabled);
            comboModel->item(4)->setFlags(comboModel->item(4)->flags() & ~Qt::ItemIsEnabled);
            nDataCombo->setCurrentIndex(0);
        }else if(numRow>=60 && numRow<120){
            comboModel->item(0)->setFlags(comboModel->item(0)->flags() | Qt::ItemIsEnabled);
            comboModel->item(2)->setFlags(comboModel->item(2)->flags() | Qt::ItemIsEnabled);
            comboModel->item(3)->setFlags(comboModel->item(3)->flags() | Qt::ItemIsEnabled);
            comboModel->item(4)->setFlags(comboModel->item(4)->flags() & ~Qt::ItemIsEnabled);
            nDataCombo->setCurrentIndex(0);
        }else if(numRow>=120){
            comboModel->item(0)->setFlags(comboModel->item(0)->flags() | Qt::ItemIsEnabled);
            comboModel->item(1)->setFlags(comboModel->item(1)->flags() | Qt::ItemIsEnabled);
            comboModel->item(2)->setFlags(comboModel->item(2)->flags() | Qt::ItemIsEnabled);
            comboModel->item(3)->setFlags(comboModel->item(3)->flags() | Qt::ItemIsEnabled);
            comboModel->item(4)->setFlags(comboModel->item(4)->flags() & ~Qt::ItemIsEnabled);
            nDataCombo->setCurrentIndex(0);
        }
    }
    nDataCombo->blockSignals(0);

}

void RealCalibrationWidget::updateDataTable(){
    qDebug()<<"# RealCalibrationWidget::updateDataTable() Called";
    //qDebug()<<" targetCombo Current Index : "<<targetCombo->currentIndex();

    dataTableModel->clear();
    QStringList headerLabels;
    headerLabels.append("Time");
    headerLabels.append("Data");
    dataTableModel->setHorizontalHeaderLabels(headerLabels);

    int currentIndex = targetCombo->currentIndex();
    if(currentIndex!=-1){
        int numRow = mainPtr_->marketDataDB().at(currentIndex)->timeset.size();
        dataTableModel->setColumnCount(2);
        dataTableModel->setRowCount(numRow);

        for (int i=0;i<numRow;i++){
            QStandardItem* timeItem = new QStandardItem;
            QStandardItem* dataItem = new QStandardItem;
            timeItem->setBackground(QBrush(QColor(Qt::lightGray)));
            dataItem->setBackground(QBrush(QColor(Qt::lightGray)));
            timeItem->setTextAlignment(Qt::AlignCenter);
            dataItem->setTextAlignment(Qt::AlignCenter);
            timeItem->setData(mainPtr_->marketDataDB().at(currentIndex)->timeset.at(numRow-1-i),Qt::DisplayRole);
            dataItem->setData(mainPtr_->marketDataDB().at(currentIndex)->dataset.at(numRow-1-i),Qt::DisplayRole);
            dataTableModel->setItem(i,0,timeItem);
            dataTableModel->setItem(i,1,dataItem);
        }
    }
}



void RealCalibrationWidget::updateChart(){
    qDebug()<<"# RealCalibrationWidget::updateChart() Called";
    //qDebug()<<" targetCombo Current Index : "<<targetCombo->currentIndex();
    customPlot->clearGraphs();
    customPlot->plotLayout()->removeAt(0);
    QCPTextElement *noTitle = new QCPTextElement(customPlot,QString(""));
    customPlot->plotLayout()->addElement(0, 0, noTitle);
    customPlot->legend->setVisible(false);

    int targetIndex =targetCombo->currentIndex();
    if(targetIndex!=-1){
        int numRow = mainPtr_->marketDataDB().at(targetIndex)->dataset.size();

        //Main Graph
        QVector<double> x(numRow), y(numRow);
        for(int i=0;i<numRow;i++){
            //x[i] = i;
            QDate qd = QDate::fromString(mainPtr_->marketDataDB().at(targetIndex)->timeset.at(i),Qt::ISODate);
            QDateTime qdt(qd);
            x[i] = qdt.toTime_t();
            y[i] = mainPtr_->marketDataDB().at(targetIndex)->dataset.at(i);
        }
        QCPGraph* mainGraph = customPlot->addGraph();
        QCPGraph* initSelectedGraph = customPlot->addGraph();
        mainGraph->setData(x,y);
        QString graphTitle = mainPtr_->marketDataDB().at(targetIndex)->name+" Data";
        mainGraph->setName(graphTitle);

        //Selected Graph Initilize
        initSelectedGraph->setData(x,y);
        QColor color(Qt::blue);
        initSelectedGraph->setLineStyle(QCPGraph::lsLine);
        initSelectedGraph->setPen(QPen(color.lighter()));
        initSelectedGraph->setBrush(QBrush(color.lighter()));
        initSelectedGraph->setName(tr("Calibration Target"));

        //Custom Plot Setting
            //x Axis
        QSharedPointer<QCPAxisTickerDateTime> dateTimeTicker(new QCPAxisTickerDateTime);
        dateTimeTicker->setDateTimeSpec(Qt::UTC);
        dateTimeTicker->setDateTimeFormat("d. MMM\nyyyy");
        customPlot->xAxis->setTicker(dateTimeTicker);
        customPlot->xAxis->setRange(x[0],x[numRow-1]);
            //y Axis
        double yMin = *std::min_element(y.begin(),y.end());
        double yMax = *std::max_element(y.begin(),y.end());
        double yRange = yMax-yMin;
        customPlot->yAxis->setRange(0,yMax+yRange*0.15);
            //Legend
        customPlot->legend->setVisible(true);
        customPlot->plotLayout()->removeAt(0);
        QCPTextElement *title = new QCPTextElement(customPlot,mainPtr_->marketDataDB().at(targetIndex)->name , QFont("sans", 17, QFont::Bold));
        customPlot->plotLayout()->addElement(0, 0, title);
    }
    customPlot->replot();
}

void RealCalibrationWidget::updateSelected(){
    qDebug()<<"# RealCalibrationWidget::updateSelected() Called";

    int targetIndex =targetCombo->currentIndex();
    int lastDateIndex = lastDateCombo->currentIndex();
    int nDataIndex = nDataCombo->currentIndex();
    if(targetIndex!=-1 && lastDateIndex!=-1 && nDataIndex!=-1){
        int numRow = mainPtr_->marketDataDB().at(targetIndex)->dataset.size();
        QVector<int> nData = {numRow-lastDateIndex,120,60,36,0};

        int beginIndex = numRow-lastDateIndex-nData[nDataIndex];
        int endIndex = numRow-lastDateIndex-1;
        int selectedNumRow = endIndex-beginIndex+1;
        //qDebug()<<" Begin : "<<beginIndex<<" End : "<<endIndex;

        //Update Selected Data
        for(int i=0;i<numRow;i++){
            dataTableModel->item(i,0)->setBackground(QBrush(QColor(Qt::lightGray)));
            dataTableModel->item(i,1)->setBackground(QBrush(QColor(Qt::lightGray)));
        }
        for(int i=0;i<selectedNumRow;i++){
            dataTableModel->item(lastDateIndex+i,0)->setBackground(QBrush(QColor(Qt::white)));
            dataTableModel->item(lastDateIndex+i,1)->setBackground(QBrush(QColor(Qt::white)));
        }

        //Update Selected Chart
        QVector<double> sx(selectedNumRow), sy(selectedNumRow);
        for(int i=0;i<selectedNumRow;i++){
            QDate qd = QDate::fromString(mainPtr_->marketDataDB().at(targetIndex)->timeset.at(beginIndex+i),Qt::ISODate);
            QDateTime qdt(qd);
            sx[i] = qdt.toTime_t();
            sy[i]=mainPtr_->marketDataDB().at(targetIndex)->dataset.at(beginIndex+i);
        }
        customPlot->removeGraph(1);
        QCPGraph* selectedGraph = customPlot->addGraph();
        selectedGraph->setData(sx,sy);
        QColor color(Qt::blue);
        selectedGraph->setLineStyle(QCPGraph::lsLine);
        selectedGraph->setPen(QPen(color.lighter()));
        selectedGraph->setBrush(QBrush(color.lighter()));
        selectedGraph->setName(tr("Calibration Target"));
        customPlot->replot();
    }
}

void RealCalibrationWidget::updateAll(){
    updateModelsCombo();
    updateParamsTable();
    updateTargetCombo();
    updateLastDateCombo();
    updateNDataCombo();
    updateDataTable();
    updateChart();
    updateSelected();
}

void RealCalibrationWidget::updateParamsTable(){
    qDebug()<<"# RealCalibrationWidget::updateParamsTable() Called";

    int modelIndex = modelsCombo->currentIndex();
    if(modelIndex!=-1){

    std::vector<double> params = mainPtr_->modelDB_.at(modelIndex)->getParams();
    std::vector<QString> paramsName = mainPtr_->modelDB_.at(modelIndex)->getParamsName();
    int numParams = params.size();

    paramsTableModel->clear();
    paramsTableModel->setColumnCount(2);
    paramsTableModel->setRowCount(numParams);


    for(int i=0;i<numParams;i++){

        QStandardItem* nameItem = new QStandardItem;
        nameItem->setBackground(QBrush(QColor(Qt::lightGray)));
        nameItem->setTextAlignment(Qt::AlignCenter);
        QStandardItem* paramItem = new QStandardItem;
        paramItem->setTextAlignment(Qt::AlignCenter);

        nameItem->setData(paramsName[i],Qt::DisplayRole);
        paramItem->setData(params[i],Qt::DisplayRole);

        paramsTableModel->setItem(i,0,nameItem);
        paramsTableModel->setItem(i,1,paramItem);
    }

    int rowHeight = paramsTable->rowHeight(0);
    int rows = paramsTableModel->rowCount();
    paramsTable->setFixedHeight(rowHeight*rows+2);
    }else{
    paramsTable->setFixedHeight(2);
    }

}

void RealCalibrationWidget::readTestData(){

    // 0. Data Input
    QTemporaryDir temporaryDir;

    //KOSPI
    QString fileName1=":/csv/kospi_180228.csv";

    QFile::copy(fileName1, temporaryDir.path() + "/kospi.txt");
    QString newFilePath1= temporaryDir.path() + "/kospi.txt";

    ifstream kospi(newFilePath1.toStdString());
    mainPtr_->marketDataDB().push_back(boost::make_shared<DataClass>());
    mainPtr_->marketDataDB().back()->timeset = dm->csv_TS_Time(kospi);
    mainPtr_->marketDataDB().back()->dataset = dm->csv_TS_Data(kospi);
    mainPtr_->marketDataDB().back()->name = QString(tr("KOSPI"));

    //S&P 500 SOA
    QString fileName2=":/csv/snp.csv";
    QFile::copy(fileName2, temporaryDir.path() + "/snp.txt");
    QString newFilePath2= temporaryDir.path() + "/snp.txt";

    ifstream snp(newFilePath2.toStdString());
    mainPtr_->marketDataDB().push_back(boost::make_shared<DataClass>());
    mainPtr_->marketDataDB().back()->timeset = dm->csv_TS_Time(snp);
    mainPtr_->marketDataDB().back()->dataset = dm->csv_TS_Data(snp);
    mainPtr_->marketDataDB().back()->name = QString(tr("S&P 500"));

    //TSE 500 SOA
    QString fileName3=":/csv/tse.csv";
    QFile::copy(fileName3, temporaryDir.path() + "/tse.txt");
    QString newFilePath3= temporaryDir.path() + "/tse.txt";

    ifstream tse(newFilePath3.toStdString());
    mainPtr_->marketDataDB().push_back(boost::make_shared<DataClass>());
    mainPtr_->marketDataDB().back()->timeset = dm->csv_TS_Time(tse);
    mainPtr_->marketDataDB().back()->dataset = dm->csv_TS_Data(tse);
    mainPtr_->marketDataDB().back()->name = QString(tr("TSE 300"));

    //KWCRT1 (OIS)
    QString fileName4=":/csv/kwcr1t.csv";
    QFile::copy(fileName4, temporaryDir.path() + "/ois.txt");
    QString newFilePath4= temporaryDir.path() + "/ois.txt";

    ifstream ois(newFilePath4.toStdString());
    mainPtr_->marketDataDB().push_back(boost::make_shared<DataClass>());
    mainPtr_->marketDataDB().back()->timeset = dm->csv_TS_Time(ois);
    mainPtr_->marketDataDB().back()->dataset = dm->csv_TS_Data(ois);
    mainPtr_->marketDataDB().back()->name = QString(tr("KRW OIS"));

}

void RealCalibrationWidget::modelCalibration(){
    qDebug()<<"# RealCalibrationWidget::modelCalibration() Called";

    int modelIndex = modelsCombo->currentIndex();
    int targetIndex = targetCombo->currentIndex();

    if(modelIndex!=-1 && targetIndex!=-1){
        int lastDateIndex = lastDateCombo->currentIndex();
        int nDataIndex = nDataCombo->currentIndex();
        qDebug()<<" (Before) Initial Params : "<<mainPtr_->modelDB().at(modelIndex)->getParams();
        std::vector<double>& targetData=mainPtr_->marketDataDB().at(targetIndex)->dataset;
        std::vector<double> calibrationData;
        if(nDataIndex==0){
            calibrationData = std::vector<double>(targetData.begin(),targetData.end()-lastDateIndex);
        }else if(nDataIndex==1){
            calibrationData = std::vector<double>(targetData.end()-lastDateIndex-120,targetData.end()-lastDateIndex);
        }else if(nDataIndex==2){
            calibrationData = std::vector<double>(targetData.end()-lastDateIndex-60,targetData.end()-lastDateIndex);
        }else if(nDataIndex==3){
            calibrationData = std::vector<double>(targetData.end()-lastDateIndex-36,targetData.end()-lastDateIndex);
        }
        //Time Series Check : right order : 0 = current, 1 = past,  d0>d1
        // if d0<d1 -> reverse
        QuantLib::Date d0 = DateParser::parseFormatted(mainPtr_->marketDataDB().at(targetIndex)->timeset[0].toStdString(),"%Y-%m-%d");
        QuantLib::Date d1 = DateParser::parseFormatted(mainPtr_->marketDataDB().at(targetIndex)->timeset[1].toStdString(),"%Y-%m-%d");
        if(d0<d1){
            std::reverse(calibrationData.begin(),calibrationData.end());
        }
        Matrix m = dm->vToMatrix(calibrationData);
        mainPtr_->modelDB().at(modelIndex)->setData(m);
        mainPtr_->modelDB().at(modelIndex)->calibrate();
        qDebug()<<" (After) Calibrated Params : "<<mainPtr_->modelDB().at(modelIndex)->getParams();
    }else{
        qDebug()<<" Model or Target Is Empty";
    }

}

RealCalibrationWidget::~RealCalibrationWidget(){
    qDebug()<<"# RealCalibrationWidget Destructor Called";

}
