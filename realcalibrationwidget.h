#ifndef REALCALIBRATIONWIDGET_H
#define REALCALIBRATIONWIDGET_H

#include <ql/math/matrix.hpp>
#include <ql/utilities/dataparsers.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <QMainWindow>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSplitter>
#include <QPushButton>
#include <QLabel>
#include <QTableView>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QDebug>
#include <QHeaderView>
#include <QSpacerItem>
#include <QRect>
#include <QComboBox>
#include <QDialog>
#include <QLineEdit>

#include "delegate.h"
#include "datamanager.h"
#include "rwmodel.h"
#include "Vasicek.h"
#include "CIR.h"
#include "LN.h"
#include "RSLN2.h"
#include "dataclass.h"
#include "qcustomplot.h"

#include "realmainwindow.h"
class RealMainWindow;
class ItemDelegate;
class NoEditDelegate;

class RealCalibrationWidget : public QWidget
{
    Q_OBJECT

public:
    RealCalibrationWidget(QWidget *parent = 0);
    ~RealCalibrationWidget();

    // 1. Declare

    //Main Stage
    QHBoxLayout* mainLayout = new QHBoxLayout;

    //mainLayout Item Declare
    QWidget* calibrationContainer = new QWidget(this);
    QVBoxLayout* calibrationLayout = new QVBoxLayout;

    QTableView* dataTable = new QTableView(this);
    QCustomPlot* customPlot = new QCustomPlot(this);

    //mainLayout_optionContainer Item Declare
    QPushButton* newModelPush = new QPushButton(tr("New Model"),this);
    QLabel* modelsLabel = new QLabel(tr("Models:"),this);
    QComboBox* modelsCombo = new QComboBox(this);

    //mainLayout_calibrationContainer Item Declare
    QLabel* paramsLabel = new QLabel(tr("Params:"),this);
    QTableView* paramsTable = new QTableView(this);
    QLabel* targetLabel = new QLabel(tr("Target Data:"),this);
    QComboBox* targetCombo = new QComboBox(this);
    QLabel* lastDateLabel = new QLabel(tr("Last Date:"),this);
    QComboBox* lastDateCombo = new QComboBox(this);
    QLabel* nDataLabel = new QLabel(tr("N Data:"),this);
    QComboBox* nDataCombo = new QComboBox(this);
    QPushButton* calibratePush = new QPushButton(tr("Calibrate"),this);
    QPushButton* debugPush = new QPushButton(tr("Debug"),this);
    QSpacerItem* vSpacer = new QSpacerItem(1,1,QSizePolicy::Minimum,QSizePolicy::Expanding);

    //paramsTable Declare
    QStandardItemModel* paramsTableModel = new QStandardItemModel(this);
    ItemDelegate* myDelegate;
    NoEditDelegate* noDelegate;

    //dataTable Declare
    QStandardItemModel* dataTableModel = new QStandardItemModel(this);

    //Other Class
    boost::shared_ptr<DataManager> dm;

    //Data
    std::vector<boost::shared_ptr<DataClass>> dataBase;
    std::vector<boost::shared_ptr<RwModel>> modelVector;

    void updateModelsCombo();
    void updateTargetCombo();
    void updateLastDateCombo();
    void updateNDataCombo();
    void updateDataTable();
    void updateChart();
    void updateSelected();
    void updateParamsTable();
    void updateAll();
    void setMainPtr(){ mainPtr_ = qobject_cast<RealMainWindow*>(this->parent()); }

private slots:
    void on_newModelPush_clicked();
    void on_modelsCombo_Changed();
    void on_targetCombo_Changed();
    void on_lastDateCombo_Changed();
    void on_nDataCombo_Changed();
    void on_calibratePush_clicked();
    void on_debugPush_clicked();

    void modelCalibration();
    void readTestData();

private:
    RealMainWindow* mainPtr_;
};

#endif // REALCALIBRATIONWIDGET_H
