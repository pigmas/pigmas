
#include "SliceWidget.h"

#include "ql/math/randomnumbers/randomsequencegenerator.hpp"
#include "ql/math/randomnumbers/mt19937uniformrng.hpp"
#include "ql/math/distributions/normaldistribution.hpp"


#include "delegate.h"
#include <QSizePolicy>
#include <QInputDialog>
#include <QBarSeries>
#include <QBarSet>
#include <QChart>


using namespace QuantLib;
class GeneralHullWhite;


SliceWidget::SliceWidget(QWidget *parent)
    : QWidget(parent)
{
    init();
    signalSetting();

}

SliceWidget::SliceWidget(MainWindow* mainPtr)
{
    mainPtr_=mainPtr;
    init();
    layoutSetting();
    signalSetting();

    absoluteTable = new Spreadsheet(mainPtr_);
    relativeTable = new Spreadsheet(mainPtr_);

    absoluteTable->setVolTable();
    relativeTable->setVolTable();

    mainLayout->addLayout(selectLayout);
    selectLayout->addWidget(optionLabel);
    selectLayout->addWidget(optionCombo);
    optionCombo->addItem("1");
    optionCombo->addItem("2");
    optionCombo->addItem("3");
    optionCombo->addItem("5");
    optionCombo->addItem("7");
    optionCombo->addItem("10");
    selectLayout->addSpacerItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
    selectLayout->addWidget(swapLabel);
    selectLayout->addWidget(swapCombo);
    swapCombo->addItem("1");
    swapCombo->addItem("2");
    swapCombo->addItem("3");
    swapCombo->addItem("5");
    swapCombo->addItem("7");
    swapCombo->addItem("10");
    selectLayout->addSpacerItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));

    mainLayout->addLayout(chartLayout);
    chartLayout->addWidget(optionView);
    chartLayout->addWidget(swapView);




    this->setLayout(mainLayout);

}

void SliceWidget::init(){

}

void SliceWidget::signalSetting(){
    connect(this->optionCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(on_optionCombo_changed()));
    connect(this->swapCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(on_swapCombo_changed()));
}


void SliceWidget::layoutSetting(){



}

void SliceWidget::chartUpdate(){
    qDebug()<<"Slice Widget chartUpdate() called"<<optionCombo->currentIndex()<<swapCombo->currentIndex();
    QLineSeries* marketOptionSeries = new QLineSeries();
    QLineSeries* modelOptionSeries = new QLineSeries();
    QLineSeries* marketSwapSeries = new QLineSeries();
    QLineSeries* modelSwapSeries = new QLineSeries();

    optionCombo->currentIndex();
    Matrix* marketPrice = mainPtr_->calibrationWidget->currentMarketPricePtr_;
    Matrix* modelPrice = mainPtr_->calibrationWidget->currentModelPricePtr_;

    Matrix optionMaturity = Matrix(1,6);

    optionMaturity[0][0]=1; optionMaturity[0][1]=2; optionMaturity[0][2]=3; optionMaturity[0][3]=5; optionMaturity[0][4]=7; optionMaturity[0][5]=10;

    for(int j=0;j<6;j++){
        marketOptionSeries->append(optionMaturity[0][j],(*marketPrice)[optionCombo->currentIndex()][j]);
        modelOptionSeries->append(optionMaturity[0][j],(*modelPrice)[optionCombo->currentIndex()][j]);
        marketSwapSeries->append(optionMaturity[0][j],(*marketPrice)[j][swapCombo->currentIndex()]);
        modelSwapSeries->append(optionMaturity[0][j],(*modelPrice)[j][swapCombo->currentIndex()]);
    }
    marketOptionSeries->setName("Market Swaption Price");
    modelOptionSeries->setName("Model Swaption Price");
    marketSwapSeries->setName("Market Swaption Price");
    modelSwapSeries->setName("Model Swaption Price");

    QChart *optionChart = new QChart();
    QChart *swapChart = new QChart();

    optionChart->addSeries(marketOptionSeries);
    optionChart->addSeries(modelOptionSeries);
    swapChart->addSeries(marketSwapSeries);
    swapChart->addSeries(modelSwapSeries);

    optionView->setChart(optionChart);
    swapView->setChart(swapChart);

    optionChart->createDefaultAxes();
    optionChart->setTitle("Option Maturity Market vs Model");

    swapChart->createDefaultAxes();
    swapChart->setTitle("Swap Maturity Market vs Model");

    optionView->setRenderHint(QPainter::Antialiasing);
    swapView->setRenderHint(QPainter::Antialiasing);

}

void SliceWidget::on_optionCombo_changed(){
    qDebug()<<"Slice Widget on_optionCombo_changed() called";
    chartUpdate();

}
void SliceWidget::on_swapCombo_changed(){
    qDebug()<<"Slice Widget on_swapCombo_changed() called";
    chartUpdate();

}
