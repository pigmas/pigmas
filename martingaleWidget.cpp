
#include "MartingaleWidget.h"

#include "ql/math/randomnumbers/randomsequencegenerator.hpp"
#include "ql/math/randomnumbers/mt19937uniformrng.hpp"
#include "ql/math/distributions/normaldistribution.hpp"


#include "delegate.h"
#include <QSizePolicy>
#include <QInputDialog>
#include <QBarSeries>
#include <QBarSet>
#include <QChart>


using namespace QuantLib;
class GeneralHullWhite;


MartingaleWidget::MartingaleWidget(QWidget *parent)
    : QWidget(parent)
{
    init();
    signalSetting();

}

MartingaleWidget::MartingaleWidget(MainWindow* mainPtr)
{
    mainPtr_=mainPtr;
    init();
    layoutSetting();
    signalSetting();




}

void MartingaleWidget::init(){

}

void MartingaleWidget::signalSetting(){

}


void MartingaleWidget::layoutSetting(){

    martingaleTable= new Spreadsheet(mainPtr_);
    mainLayout->addWidget(martingaleView);
    mainLayout->addWidget(martingaleTableContainer);
    martingaleTableContainer->setLayout(tableLayout);
    tableLayout->addWidget(martingaleTable);

    this->setLayout(mainLayout);


}

void MartingaleWidget::chartUpdate(){
    qDebug()<<"Martingale Widget chartUpdate() called";
    MY::YieldTermStructure ts;
    ts=mainPtr_->getMyTree()->getYieldItemVector().at(mainPtr_->scenarioWidget->termComboBox->currentIndex())->getTermStructure();
    Matrix tempDisc = mainPtr_->scenarioWidget->tempDiscount;
    vector<double> sdVector;
    vector<double> upperVector;
    vector<double> lowerVector;


    int numRow=tempDisc.size1();
    int numCol=tempDisc.size2();

    double sum = 0.0, mean, standardDeviation = 0.0;

    for(int j = 0; j < numCol; j++){

        for(int i = 0; i < numRow; i++)
        {
            //sum += ts.discount(t)/tempDisc[i][j];
            sum += tempDisc[i][j];
        }

        mean = sum/double(numRow);
        for(int i = 0; i < numRow; ++i)
            standardDeviation += pow(tempDisc[i][j] - mean, 2);

        sdVector.push_back(sqrt(standardDeviation / double(numRow)));
        upperVector.push_back(1.0+sdVector.at(j)*1.96/sqrt(double(numRow))/numRow/5);
        lowerVector.push_back(1.0-sdVector.at(j)*1.96/sqrt(double(numRow))/numRow/5);

    }

    Matrix disc(1,numCol);
    for(int j=0;j<numCol;j++){
        double temp=0.0;
        for(int i=0;i<numRow;i++){
            temp=temp+tempDisc[i][j];
        }
        disc[0][j]=temp/double(numRow);
    }

    qDebug()<<disc[0][0]<<disc[0][1]<<disc[0][2]<<disc[0][3]<<"middle vector";

    QChart *chart1 = new QChart();

    QLineSeries* lowerSeries = new QLineSeries();
    for(int j=0;j<numCol;j++){
        lowerSeries->append(j,lowerVector[j]);
    }
    chart1->addSeries(lowerSeries);

    QLineSeries* upperSeries = new QLineSeries();
    for(int j=0;j<numCol;j++){
        upperSeries->append(j,upperVector[j]);
    }
    chart1->addSeries(upperSeries);

    QLineSeries* middleSeries = new QLineSeries();
    for(int j=0;j<numCol;j++){
        double t=double(j)/12.0;
        middleSeries->append(j,ts.discount(t)/disc[0][j]);
    }
    chart1->addSeries(middleSeries);

    QLineSeries* baseSeries = new QLineSeries();
    for(int j=0;j<numCol;j++){
        baseSeries->append(j,1.0);
    }
    chart1->addSeries(baseSeries);

    martingaleView->setChart(chart1);
    martingaleView->setRenderHint(QPainter::Antialiasing);
    chart1->legend()->hide();
    chart1->createDefaultAxes();
    chart1->setTitle("1 to 1 Test");

    Matrix* m=new Matrix(numRow,3);
    for(int i=0;i<numRow;i++){
        (*m)[i][0]=ts.discount(double(i)/12);
        (*m)[i][1]=disc[0][i];
        (*m)[i][2]=ts.discount(double(i)/12)/disc[0][i];
    }
    martingaleTable->readMatrix(m);
    martingaleTableContainer->setFixedWidth(400);

}

