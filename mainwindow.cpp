
#include <QSizePolicy>
#include <QDebug>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QString>

#include "mainwindow.h"
#include "fancytabbar.h"
#include "mytree.h"
#include "smithwilsonengine.h"
#include "smithwilsonwidget.h"
#include "yieldtermstructure.h"
#include "generalhullwhite.h"

#include "ql/models/shortrate/onefactormodels/hullwhite.hpp"
#include "ql/termstructures/yieldtermstructure.hpp"
#include "ql/quotes/simplequote.hpp"
#include "ql/termstructures/yield/flatforward.hpp"

#include "nullibor.h"
#include <ql/models/shortrate/calibrationhelpers/swaptionhelper.hpp>
#include "ql/pricingengines/swaption/jamshidianswaptionengine.hpp"
#include <ql/math/optimization/levenbergmarquardt.hpp>

#include "ql/processes/hullwhiteprocess.hpp"
#include "ql/methods/montecarlo/pathgenerator.hpp"

#include "ql/math/randomnumbers/randomsequencegenerator.hpp"
#include "ql/math/randomnumbers/centrallimitgaussianrng.hpp"
#include "ql/math/randomnumbers/mt19937uniformrng.hpp"

#include "ql/math/distributions/normaldistribution.hpp"
#include "datamanager.h"
#include "dataclass.h"

#include <optimization.h>
#include <gsl/gsl_sf_bessel.h>

#define myqDebug() qDebug()<<qSetRealNumberPrecision(20)


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    init();

    createActions();
    createMenus();
    signalSetting();

    setWindowTitle(tr("Menus"));
    setMinimumSize(800, 600);
    resize(1028, 768);

//    QString fileName = QFileDialog::getOpenFileName(this,tr("Open csv"),".",tr("Csv files (*.csv)")); //debugger encounter an exception
//    qDebug()<<fileName<<"fileName";
    QString fileName1="c:/yieldDB.csv";
    ifstream data1(fileName1.toStdString());
    dataManager->readYieldDB(data1);
    databaseWidget->setYieldTable();
    oneClickWidget->dateComboUpdate();

    QString fileName2="c:/volDB.csv";
    ifstream data2(fileName2.toStdString());

    dataManager->readVolDB(data2);
    databaseWidget->setVolTable();
    oneClickWidget->generatePush->setEnabled(true);

    databaseWidget->yieldTable->setColumnWidth(0,80);
    for(int i=1;i<databaseWidget->yieldTable->columnCount();i++){
        databaseWidget->yieldTable->setColumnWidth(i,50);
    };
    databaseWidget->volTable->setColumnWidth(0,80);
    for(int i=1;i<databaseWidget->volTable->columnCount();i++){
       databaseWidget->volTable->setColumnWidth(i,50);
    };

    //test();
    test2();



}

void MainWindow::init(){
    //
    //updateButton = new QPushButton("update");
    //
    QFont f;
    f.setPointSize(10);
    this->setFont(f);
    QWidget *Base = new QWidget;

    myTree = new MyTree(this);
    myTree->installEventFilter(this);
    myTree->setMinimumWidth(250);
    myTreeMenu=new QMenu(this);
    myTreeMenu->addAction("Delete",this,SLOT(deleteItem()));

    dataManager = new DataManager;

    mainStacked = new QStackedWidget;

    QTabWidget *tabslot0 = new QTabWidget;
    QTabWidget *tabslot1 = new QTabWidget;
    QTabWidget *tabslot2 = new QTabWidget;
    QTabWidget *tabslot3 = new QTabWidget;
    QTabWidget *tabslot4 = new QTabWidget;
    QTabWidget *tabslot5 = new QTabWidget;

    databaseWidget = new DatabaseWidget(this);
    oneClickWidget = new OneClickWidget(this);

    smithWilsonWidget = new SmithWilsonWidget(this);
    smithWilsonWidget->curveList->installEventFilter(this);
    sol2ShockWidget = new Sol2ShockWidget(this);

    swTermMenu=new QMenu(this);
    swTermMenu->addAction("Add to project",this,SLOT(smithAddToProject()));
    swTermMenu->addAction("Delete",this,SLOT(smithDeleteTempCurve()));


    calibrationWidget = new CalibrationWidget(this);
    compareWidget = new CompareWidget(this);
    sliceWidget = new SliceWidget(this);

    rnWidget = new RnWidget(this);
    rnWidget->tempList->installEventFilter(this);
    rnListMenu=new QMenu(this);
    rnListMenu->addAction("Add to project",this,SLOT(rnAddToProject()));
    rnListMenu->addAction("Delete",this,SLOT(rnDelTemp()));
    rnListMenu->addAction("Rename");

    scenarioWidget = new ScenarioWidget(this);
    scenarioWidget->tempScenarioList->installEventFilter(this);
    martingaleWidget = new MartingaleWidget(this);

    scenListMenu = new QMenu(this);
    scenListMenu->addAction("Add to project",this,SLOT(scenAddToProject()));
    scenListMenu->addAction("Delete",this,SLOT(scenDelTemp()));
    scenListMenu->addAction("Rename");

    mainStacked->addWidget(tabslot0);
    mainStacked->addWidget(tabslot1);
    mainStacked->addWidget(tabslot2);
    mainStacked->addWidget(tabslot3);
    mainStacked->addWidget(tabslot4);
    mainStacked->addWidget(tabslot5);


    tabbar = new FancyTabBar;
    tabbar->addFancyTab(QIcon(":/new/prefix1/img/database.svg"),trUtf8("Database"));
    tabbar->addFancyTab(QIcon(":/new/prefix1/img/oneclick.svg"), tr("One Click"));
    tabbar->addFancyTab(QIcon(":/new/prefix1/img/curve.svg"), tr("Yield Curve"));
    tabbar->addFancyTab(QIcon(":/new/prefix1/img/theta.svg"), tr("Calibration"));
    tabbar->addFancyTab(QIcon(":/new/prefix1/img/dice.svg"), tr("Random Number"));
    tabbar->addFancyTab(QIcon(":/new/prefix1/img/scenario.svg"), tr("Scenario"));
    tabbar->setActiveIndex(0);

    tabslot0->addTab(databaseWidget,tr("Database"));
    tabslot1->addTab(oneClickWidget,tr("One Click"));
    tabslot2->addTab(smithWilsonWidget,tr("Smith-Wilson"));
    tabslot2->addTab(sol2ShockWidget,tr("Sol2 Shock"));
    tabslot3->addTab(calibrationWidget,tr("Calibration"));
    tabslot3->addTab(compareWidget,tr("Compare"));
    tabslot3->addTab(sliceWidget,tr("Slice"));
    tabslot4->addTab(rnWidget,tr("Random Number"));
    tabslot5->addTab(scenarioWidget,tr("Scenario Generator"));
    tabslot5->addTab(martingaleWidget,tr("Martingale Widget"));

    logText = new QTextEdit;
    logText->setFontPointSize(10);
    QSplitter *split2_34 = new QSplitter;
    QSplitter *split3_4 = new QSplitter;

    QHBoxLayout *layout = new QHBoxLayout;

    split3_4->setOrientation(Qt::Vertical);
    split3_4->addWidget(mainStacked);
    split3_4->addWidget(logText);
    split3_4->setSizes({700,100});
    split3_4->setStretchFactor(0,1);

    split2_34->addWidget(split3_4);
    split2_34->addWidget(myTree);
    split2_34->setSizes({700,200});
    split2_34->setStretchFactor(0,1);


    layout->addWidget(tabbar);
    layout->addWidget(split2_34);

    Base->setLayout(layout);

    QWidget *widget = new QWidget;
    setCentralWidget(widget);

    QWidget *topFiller = new QWidget;
    topFiller->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
   // Base *base= new Base;

    infoLabel = new QLabel(tr("<i>Choose a menu option, or right-click to "
                              "invoke a context menu</i>"));
    infoLabel->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    infoLabel->setAlignment(Qt::AlignBottom);
    infoLabel->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
    //QWidget *bottomFiller = new QWidget;
    //bottomFiller->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QVBoxLayout *layout2 = new QVBoxLayout;
    layout2->setMargin(5);
    layout2->addWidget(Base);
    layout2->addWidget(infoLabel);

    //layout->addWidget(bottomFiller);
    widget->setLayout(layout2);
    QString message = tr("A context menu is available by right-clicking");
    statusBar()->showMessage(message);


}
void MainWindow::signalSetting(){

    connect(tabbar,SIGNAL(activeIndexChanged(qint32)),mainStacked,SLOT(setCurrentIndex(int)));
    //connect(updateButton,SIGNAL(clicked(bool)),this,SLOT(updateButtonClicked()));
    connect(this,SIGNAL(updated()),this,SLOT(updateWidgets()));

}

#ifndef QT_NO_CONTEXTMENU
void MainWindow::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);
    menu.exec(event->globalPos());
}
#endif // QT_NO_CONTEXTMENU

void MainWindow::newFile()
{
    infoLabel->setText(tr("Invoked <b>File|New</b>"));

    //string1=textbox1input
    //string2=textbox2input
    //myTree->additem(string1,string2)
}

void MainWindow::open()
{
    infoLabel->setText(tr("Invoked <b>File|Open</b>"));
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open csv"),".",tr("Csv files (*.csv)")); //debugger encounter an exception

    //nameDialog->setLabelText("Variable Name");
    //nameDialog->show();

    ifstream data(fileName.toStdString());
    //dataManager->fnc();
    //myTree->addItem();
    if(!data.fail()){
        Matrix m=dataManager->csv_Matrix(data);
        if(m.size2()==2){
            bool ok;

            QString varName = QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                          tr("Variable Name:"), QLineEdit::Normal,
                                          "", &ok);
            if(varName==""){
                varName="null";
            }
            myTree->addInputItem(varName,m);
        }else if(m.size2()==6){
            bool ok;

            QString varName = QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                          tr("Variable Name:"), QLineEdit::Normal,
                                          "", &ok);
            if(varName==""){
                varName="null";
            }
            myTree->addVolItem(varName,m);
        }else if(m.size2()==13){
            dataManager->readYieldDB(data);
            databaseWidget->setYieldTable();
            oneClickWidget->dateComboUpdate();
        }else if(m.size2()==37){
            if(dataManager->dateVector.size()!=0){
                dataManager->readVolDB(data);
                databaseWidget->setVolTable();
                oneClickWidget->generatePush->setEnabled(true);
            }
        }
    emit(updated());
    }


}

void MainWindow::save()
{
    infoLabel->setText(tr("Invoked <b>File|Save</b>"));

}

void MainWindow::print()
{
    infoLabel->setText(tr("Invoked <b>File|Print</b>"));
}

void MainWindow::closeProject(){
    qDebug()<<" MainWindow::close() Called";
    infoLabel->setText(tr("Invoked <b>File|Close</b>"));
    LoginWidget* parent = qobject_cast<LoginWidget*>(this->parent());
    this->close();
    this->deleteLater();
    parent->selectWorld();
}

void MainWindow::createActions()
{
    newAct = new QAction(tr("&New"), this);
    newAct->setShortcuts(QKeySequence::New);
    newAct->setStatusTip(tr("Create a new file"));
    connect(newAct, &QAction::triggered, this, &MainWindow::newFile);

    openAct = new QAction(tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, &QAction::triggered, this, &MainWindow::open);

    saveAct = new QAction(tr("&Save"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save the document to disk"));
    connect(saveAct, &QAction::triggered, this, &MainWindow::save);

    printAct = new QAction(tr("&Print..."), this);
    printAct->setShortcuts(QKeySequence::Print);
    printAct->setStatusTip(tr("Print the document"));
    connect(printAct, &QAction::triggered, this, &MainWindow::print);

    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, &QAction::triggered, this, &MainWindow::closeProject);


}

void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newAct);
    fileMenu->addAction(openAct);
    fileMenu->addAction(saveAct);
    fileMenu->addAction(printAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

}

void MainWindow::updateButtonClicked(){
    qDebug()<<"main_update_clicked";
    emit(updated());
}

void MainWindow::updateWidgets(){
    qDebug()<<"Main Window updateWidgets() called";
    smithWilsonWidget->update();
    calibrationWidget->update();
    scenarioWidget->update();
}

void MainWindow::updateScenParam(){
    scenarioWidget->paramsUpdate();
}

bool MainWindow::eventFilter(QObject *target, QEvent *event){
    //qDebug()<<target;
    QContextMenuEvent* e = dynamic_cast<QContextMenuEvent*>(event);
    if (event->type() == QEvent::ContextMenu && e!=0)
    {
        if(target==myTree){
                qDebug()<<"MainWindow MyTree Context Menu";
                myTreeMenu->exec(QCursor::pos());
                return true;

        }else if(target==smithWilsonWidget->curveList){
                qDebug()<<"MainWindow Temporary Curve List Context Menu";
                swTermMenu->exec(QCursor::pos());
                return true;

        }else if(target==rnWidget->tempList){

                qDebug()<<"MainWindow Temporary RN List Context Menu";
                rnListMenu->exec(QCursor::pos());
                return true;

        }else if(target==scenarioWidget->tempScenarioList){

                qDebug()<<"MainWindow Temporary Scenario List Context Menu";
                scenListMenu->exec(QCursor::pos());
                return true;

         }
    }
    return false;

}
void MainWindow::deleteItem(){
    myTree->delItem();
}
void MainWindow::smithAddToProject(){
    smithWilsonWidget->addToProject();
}
void MainWindow::smithDeleteTempCurve(){
    smithWilsonWidget->delTempCurve();
}
void MainWindow::rnAddToProject(){
    rnWidget->addToProject();
}
void MainWindow::rnDelTemp(){
    rnWidget->delTemp();
}
void MainWindow::scenAddToProject(){
    scenarioWidget->addToProject();
}
void MainWindow::scenDelTemp(){
    scenarioWidget->delTemp();
}


void MainWindow::test(){
    ////////////////////////////////////////////curve////////////////////////////////////
        SmithWilsonEngine* sw = new SmithWilsonEngine;

        //QuantLib::Handle<MY::YieldTermStructure> yt(boost::shared_ptr<MY::YieldTermStructure>(new MY::YieldTermStructure));
        //yt->setDiscountMonth(sw->generate());

        //qDebug()<<yt->discount(3);
        //boost::shared_ptr<QuantLib::HullWhite> hw(new QuantLib::HullWhite(yt));

        //QuantLib::YieldTermStructure tt;
        //QuantLib::Date todaysDate(15, February, 2002);
        //Calendar calendar = TARGET();
        //Date settlementDate(19, February, 2002);
        //Settings::instance().evaluationDate() = todaysDate;

        //boost::shared_ptr<Quote> flatRate(new QuantLib::SimpleQuote(0.04875825));
        //Handle<YieldTermStructure> rhTermStructure(boost::shared_ptr<FlatForward>(new FlatForward(settlementDate,Handle<Quote>(flatRate),Actual365Fixed())));

        //qDebug()<<typeid(boost::shared_ptr<FlatForward>(new FlatForward(settlementDate,Handle<Quote>(flatRate),Actual365Fixed()))).name();
        //qDebug()<<typeid(new FlatForward(settlementDate,Handle<Quote>(flatRate),Actual365Fixed())).name();

        boost::shared_ptr<MY::YieldTermStructure> rth(new MY::YieldTermStructure);
        rth->setDiscountMonth(sw->generate());
        Handle<QuantLib::YieldTermStructure> rth_term(rth);

        ///////////////////////////////////Hull White Swaption////////////////////////////

        boost::shared_ptr<HullWhite> modelHW(new HullWhite(rth_term));
        boost::shared_ptr<GeneralHullWhite> modelGHW(new GeneralHullWhite(rth_term));
        //boost::shared_ptr<Vasicek> modelV(new Vasicek);

        //myqDebug()<<modelGHW->sigma_p(10,10.5);
        //myqDebug()<<modelGHW->A(3,20);
        //myqDebug()<<modelGHW->discountBond(3,20,0.03);
        //myqDebug()<<modelGHW->discountBondOption(QuantLib::Option::Put,0.95,3,20);

        boost::shared_ptr<IborIndex> nullIndex(new NullIbor(rth_term));
        std::vector<Period> optionMaturities;

        optionMaturities.push_back(Period(1, Years));
        optionMaturities.push_back(Period(2, Years));
        optionMaturities.push_back(Period(3, Years));
        optionMaturities.push_back(Period(5, Years));
        optionMaturities.push_back(Period(7, Years));
        optionMaturities.push_back(Period(10, Years));

        std::vector<Period> swapMaturities;

        swapMaturities.push_back(Period(1, Years));
        swapMaturities.push_back(Period(2, Years));
        swapMaturities.push_back(Period(3, Years));
        swapMaturities.push_back(Period(5, Years));
        swapMaturities.push_back(Period(7, Years));
        swapMaturities.push_back(Period(10, Years));

        std::vector<boost::shared_ptr<CalibrationHelper>> swaptions;

        Volatility swaptionVols[] = {
            0.363,  0.348,  0.335,  0.315,  0.31,   0.303,
            0.335,  0.302,  0.292,  0.273,  0.266,  0.264,
            0.295,  0.271,  0.265,  0.25,   0.241,  0.242,
            0.257,  0.248,  0.239,  0.229,  0.225,  0.228,
            0.2442, 0.2396, 0.2346, 0.2294, 0.2314, 0.242,
            0.225,  0.227,  0.228,  0.23,   0.241,  0.263};

        std::list<Time> times;
        boost::shared_ptr<PricingEngine> jams_GHW(new JamshidianSwaptionEngine(modelGHW));
        boost::shared_ptr<Quote> vol(new SimpleQuote(swaptionVols[2]));
        boost::shared_ptr<CalibrationHelper> finder(new
                                                    SwaptionHelper(optionMaturities[1],
                                                                   swapMaturities[1],
                                                                   Handle<Quote>(vol),
                                                                   nullIndex,
                                                                   nullIndex->tenor(),
                                                                   nullIndex->dayCounter(),
                                                                   nullIndex->dayCounter(),
                                                                   rth_term));
        finder->setPricingEngine(jams_GHW);
        finder->addTimesTo(times);
        finder->marketValue();
        for (int i=0; i<6; i++) {
            for (int j=0; j<6; j++){
                Size k = i*6 + j;
                boost::shared_ptr<Quote> vol(new SimpleQuote(swaptionVols[k]));
                swaptions.push_back(boost::shared_ptr<CalibrationHelper>(new
                    SwaptionHelper(optionMaturities[i],
                                   swapMaturities[j],
                                   Handle<Quote>(vol),
                                   nullIndex,
                                   nullIndex->tenor(),
                                   nullIndex->dayCounter(),
                                   nullIndex->dayCounter(),
                                   rth_term)));
                swaptions.back()->addTimesTo(times);
                swaptions[k]->setPricingEngine(jams_GHW);
            }
        }

        //myqDebug()<<swaptions[0]->modelValue()<<"1x1 model value";
        //myqDebug()<<swaptions[5]->modelValue()<<"1x10 model value";
        //myqDebug()<<swaptions[35]->modelValue()<<"10x10 model value";
        LevenbergMarquardt om;
        //modelGHW->calibrate(swaptions, om, EndCriteria(400, 100, 1.0e-8, 1.0e-8, 1.0e-8));

    //    myqDebug()<<swaptions[0]->marketValue()<<"1x1 market value";
    //    myqDebug()<<swaptions[5]->marketValue()<<"1x10 market value";
    //    myqDebug()<<swaptions[35]->marketValue()<<"10x10 market value";

    //    myqDebug()<<modelGHW->params()[0]<<"param0";
    //    myqDebug()<<modelGHW->params()[1]<<"param1";
    //    myqDebug()<<modelGHW->params()[2]<<"param2";
    //    myqDebug()<<modelGHW->params()[3]<<"param3";
    //    myqDebug()<<modelGHW->params()[4]<<"param4";
    //    myqDebug()<<modelGHW->params()[5]<<"param5";
    //    myqDebug()<<modelGHW->params()[6]<<"param6";
    //    myqDebug()<<modelGHW->params()[7]<<"param7";
    //    myqDebug()<<modelGHW->params()[8]<<"param8";
    //    myqDebug()<<modelGHW->params()[9]<<"param9";

        //myqDebug()<<swaptions[0]->marketValue()<<"market value";
        myqDebug()<<swaptions[0]->modelValue()<<"1x1 model value";
        myqDebug()<<swaptions[5]->modelValue()<<"1x10 model value";
        myqDebug()<<swaptions[35]->modelValue()<<"10x10 model value";
    ///////////////////HullWhitePath//////////////////////////////////////////////////////////////

        Real reversionSpeed = 0.75;
        Real rateVolatility = 0.015;
        Time maturity = 5.0;
        Size nSteps = 10;
        Date settlementDate(19, February, 2002);

        typedef RandomSequenceGenerator<CLGaussianRng<MersenneTwisterUniformRng>> GSG;
        unsigned long seed = 28749;
        MersenneTwisterUniformRng generator(seed);
        CLGaussianRng<MersenneTwisterUniformRng> gaussianGenerator(generator);
        GSG gaussianSequenceGenerator(nSteps, gaussianGenerator);



        boost::shared_ptr<Quote> flatRate(new QuantLib::SimpleQuote(0.04875825));
        Handle<QuantLib::YieldTermStructure> rhTermStructure(boost::shared_ptr<FlatForward>(new FlatForward(settlementDate,Handle<Quote>(flatRate),Actual365Fixed())));
        boost::shared_ptr<StochasticProcess1D> HW1F(new HullWhiteProcess(rhTermStructure, reversionSpeed, rateVolatility));



        PathGenerator<GSG> pathGenerator(HW1F, maturity, nSteps, gaussianSequenceGenerator, false);

        Size nColumns = 2;
        Matrix paths(nSteps + 1, nColumns);
        for(unsigned int i = 0; i != paths.columns(); i++)
        {
         // request a new stochastic path from path generator
             QuantLib::Sample<Path> path = pathGenerator.next();
         //
         // save generated path into container
            for(unsigned int j = 0; j != path.value.length(); j++)
            {
                paths[j][i] = path.value.at(j);
                myqDebug()<<paths[j][i];
            }
        }
        ///////////////////////////////////////////////////////////////////////////////////
        boost::shared_ptr<GeneralHullWhite> modelGHWinitial(new GeneralHullWhite(rth_term));
        //qDebug()<<modelGHWinitial->theta(1.0/12.0);
        MersenneTwisterUniformRng uGenerator(28749);

        InverseCumulativeNormal normInv;

        Matrix rnd(1,1200);
        for (int i=0; i<rnd.size1();i++){

            for (int j=0; j<rnd.size2(); ++j) {

              rnd[i][j] = normInv(uGenerator.next().value);

              //qDebug()<<normInv(rnd[i][j]);
            }

        }

        Matrix scen=modelGHWinitial->scenario(rnd);
        qDebug()<<scen[0][0]<<scen[0][1]<<scen[0][2]<<scen[0][3]<<scen[0][4];
        std::ofstream out("c:/testscen.csv");

        for (int i=0;i<scen.size1();i++){
            for (int j=0;j<scen.size2();j++){
                out<<scen[i][j]<<",";
            }
            out<<"\n";
        }
        std::ofstream out2("c:/testrnd.csv");
        for (int i=0;i<scen.size1();i++){
            for (int j=0;j<scen.size2();j++){
                out2<<rnd[i][j]<<",";
            }
            out2<<"\n";
        }
        std::ofstream out3("c:/testtheta.csv");
        for (int i=0;i<scen.size1();i++){
            for (int j=0;j<scen.size2();j++){
                out3<<modelGHWinitial->theta(j/12.0)<<",";
            }
            out3<<"\n";
        }

        //DataClass dc;
        //Matrix* mat = new Matrix(3,9);
        //Matrix* scenPtr=&scen;
        //dc.matrixVector.push_back(scenPtr);
        //dc.matrixVector.push_back(mat);
        //qDebug()<<scenPtr;
        matrixVector.push_back(scen);
        //qDebug()<<scen[10][10];
        smithWilsonWidget->setData(&matrixVector.back());
}

void MainWindow::test2(){

    qDebug()<<"Hello std::cout";
    qDebug()<<"Bessel Test : "<<gsl_sf_bessel_Inu_scaled(3, 200);
    std::cout<<"Optimization test : "<<std::endl;


    alglib::real_1d_array x2="[0.01,0.02,0.03]";
    alglib::real_1d_array bndl2 = "[-0.5,-0.5,0.001]";
    alglib::real_1d_array bndu2 = "[0.5,0.5,1]";
    alglib::minbcstate state2;
    //alglib::minbcreport rep2;

    double epsg2 = 1.0e-16;
    double epsf2 = 0;
    double epsx2 = 0;
    alglib::ae_int_t maxits2 = 0;

    double diffstep2 = 1.0e-6;

    alglib::minbccreatef(x2, diffstep2, state2);
    alglib::minbcsetbc(state2, bndl2, bndu2);
    alglib::minbcsetcond(state2, epsg2, epsf2, epsx2, maxits2);

    std::cout<<"Optimization test OK "<<std::endl;


}
