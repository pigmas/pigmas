#include "datamanager.h"
#include <QFileDialog>
#include <QString>
#include <QTranslator>
#include <QDate>

DataManager::DataManager()
{
    qDebug()<<"# Data Manager Initializer called";
    Date d = DateParser::parseFormatted("2016-12-30","%Y-%m-%d");
    d = Date(38016);
    //QString dString = QString::fromStdString(string(d.year()));//+"-"+QString(d.month())+"-"+QString(d.dayOfMonth());

    //qDebug()<<dString<<"dString";
    std::ostringstream out;
    out<<QuantLib::io::iso_date(d);
//    qDebug()<<QString::fromStdString(out.str());
//    qDebug()<<d.year()<<d.month()<<d.dayOfMonth();


}
void DataManager::qDebug_Matrix(Matrix m){
    for (int i=0;i<m.size1();i++){

        QString str;

        for (int j=0;j<m.size2();j++){

            str+=QString("(")+=QString::fromStdString(to_string(i))+=QString(",")+=QString::fromStdString(to_string(j))+=QString(")")+=QString::fromStdString(to_string(m[i][j]))+=" ";
        }

        qDebug()<<str;

    }

}

Matrix DataManager::csv_Matrix(ifstream& data){
    string line;
    data.clear();
    data.seekg(0,ios::beg);

    int rows=0;
    int cols=0;
       while(std::getline(data,line))
       {
           stringstream lineStream(line);
           string cell;
           cols=0;
           while(std::getline(lineStream,cell,','))
           {
               cols++;
           }

           rows++;
        }
    data.clear();
    data.seekg(0,ios::beg);

    //boost::shared_ptr<Matrix> m(new Matrix(rows,cols));
    boost::shared_ptr<Matrix> m(new Matrix(rows,cols));
    int i=0;

       while(std::getline(data,line))
       {
           stringstream lineStream(line);
           string cell;
           int j=0;
           while(getline(lineStream,cell,','))
           {
               (*m)[i][j]=stod(cell);
               //qDebug()<<(*m)[i][j];
               j++;
           }

           i++;

        }
    return *m;
}

Matrix DataManager::csv_Matrix(ifstream& data,int rownum,int colnum){

    boost::shared_ptr<Matrix> m(new Matrix(rownum,colnum));

    string line;

    int i=0;

       while(std::getline(data,line))
       {
           stringstream lineStream(line);
           string cell;
           int j=0;
           while(getline(lineStream,cell,','))
           {

               (*m)[i][j]=stod(cell);
               //qDebug()<<(*m)[i][j];
               j++;
           }

           i++;

        }
    return *m;
}

Matrix DataManager::csv_Matrix(ifstream& data,int rownum,int colnum,int start, int end){

    boost::shared_ptr<Matrix> m(new Matrix(rownum,colnum));

    string line;

    int line_no=1;
    int i=0;

       while(std::getline(data,line))
       {
           stringstream lineStream(line);
           string cell;

           int j=0;

           while(getline(lineStream,cell,','))
           {

               if(line_no>=start&&line_no<=end){
                   (*m)[i][j]=stod(cell);
               }
               //qDebug()<<(*m)[i][j];
               j++;
           }

           if(line_no>=start&&line_no<=end){
               i++;
           }
           line_no++;

        }
    return *m;
}

void DataManager::fnc(){
    qDebug()<<"ok";
}

Matrix DataManager::row(Matrix m,int i){

    Matrix ret = Matrix(1,m.size2());

    for(int j=0;j<m.size2();j++){
        ret[0][j]=m[i][j];
    };
    return ret;
}
Matrix DataManager::col(Matrix m,int j){

    Matrix ret = Matrix(m.size1(),1);

    for(int i=0;i<m.size1();i++){
        ret[i][0]=m[i][j];
    };
    return ret;

}

void DataManager::readYieldDB(ifstream& data){
    qDebug()<<"DataManager readYieldDB() called";

    Matrix rawMatrix = csv_Matrix(data);

    int numRow=rawMatrix.size1();
    int numCol=rawMatrix.size2();
    yieldDbTenor=Matrix(1,numCol-1);


    for (int j=1;j<numCol;j++){
        yieldDbTenor[0][j-1]=rawMatrix[0][j];
    }

    for (int i=1;i<numRow;i++){
        dateVector.push_back(Date(rawMatrix[i][0]));
    }



    for (int i=1;i<numRow;i++){
        Matrix tempRate = Matrix(1,numCol-1);
        for (int j=1;j<numCol;j++){
            tempRate[0][j-1]=rawMatrix[i][j];
        }
        yieldDB.push_back(tempRate);
    }

    //qDebug()<<yieldDbTenor[0][0]<<yieldDbTenor[0][1]<<yieldDbTenor[0][2]<<yieldDbTenor[0][3]<<"yieldDbTenor";
    //qDebug()<<dateVector[0].dayOfMonth()<<dateVector[1].dayOfMonth()<<dateVector[2].dayOfMonth()<<dateVector[3].dayOfMonth()<<"date vector month";
    //qDebug()<<yieldDB.at(0)[0][0]<<yieldDB.at(0)[0][1]<<yieldDB.at(0)[0][2]<<"yield DB 0";
    //qDebug()<<yieldDB.at(1)[0][0]<<yieldDB.at(1)[0][1]<<yieldDB.at(1)[0][2]<<"yield DB 1";


}

void DataManager::readVolDB(ifstream& data){

    Matrix rawMatrix = csv_Matrix(data);

    int numRow=rawMatrix.size1();

    for (int i=0;i<numRow;i++){
        Matrix tempVol = Matrix(6,6);
        for (int m=0;m<6;m++){
            for (int n=0;n<6;n++){
                tempVol[m][n]=rawMatrix[i][m*6+n+1];
            }
        }
        volDB.push_back(tempVol);
    }

    //qDebug()<<volDB.at(0)[0][0]<<volDB.at(0)[0][1]<<volDB.at(0)[0][2]<<volDB.at(0)[0][3]<<volDB.at(0)[0][4]<<volDB.at(0)[0][5]<<volDB.at(0)[1][0]<<volDB.at(0)[5][5]<<"volDB 0";
    //qDebug()<<volDB.at(1)[0][0]<<volDB.at(1)[0][1]<<volDB.at(1)[5][5]<<"volDB 1";
}

std::vector<QString> DataManager::csv_TS_Time(ifstream& data){
    Matrix raw = csv_Matrix(data);
    std::vector<QString> ret;

    int numRow=raw.size1();
    for(int i=0;i<numRow;i++){
        int serialNumber = int(raw[i][0]);
        if(serialNumber<=367 || serialNumber>=109574){
            serialNumber=367+i;
        }
        std::ostringstream out;
        out<<QuantLib::io::iso_date(Date(serialNumber));
        ret.push_back(QString::fromStdString(out.str()));
    }
    return ret;
}

std::vector<double> DataManager::csv_TS_Data(ifstream& data){
    Matrix raw = csv_Matrix(data);
    std::vector<double> ret;

    int numRow=raw.size1();
    for(int i=0;i<numRow;i++){
        ret.push_back(raw[i][1]);
    }

    return ret;

}

Matrix DataManager::vToMatrix(std::vector<double> v){
    int numRow=v.size();
    Matrix m(numRow,1);
    for(int i=0;i<numRow;i++){
        m[i][0]=v[i];
    }
    return m;
}

void DataManager::csvWrite(QString fileName, Matrix m){

    std::ofstream out(fileName.toStdString());

    for (int i=0;i<m.size1();i++){
        for (int j=0;j<m.size2()-1;j++){
            out<<m[i][j]<<",";
        }
        out<<m[i][m.size2()-1]<<"\n";
    }

}
